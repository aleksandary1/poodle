import sectionsData from '../data/sections.data.js';
import { v4 as uuid } from 'uuid';
import restrictionsData from '../data/restrictions.data.js';

export const getSections = async () => {
	return await sectionsData.getAll();
};

export const getSectionBy = async (column, value) => {
	return await sectionsData.getBy(column, value);
};

export const getAllSectionsBy = async (column, value) => {
	return await sectionsData.getAllBy(column, value);
};

export const getAllSectionsByWithRestrictions = async (column, value, user) => {
	const sections = await sectionsData.getAllBy(column, value);
	const result = Promise.all(sections.map(async (section) => {
		if (section.is_restricted && !user.isTeacher) {
			if (section.restricted_by === 'date') {
				if (new Date() > new Date(section.restriction_date)) {
					return section;
				}
			} else if (section.restricted_by === 'users') {
				const restrictedUsers = await restrictionsData.getUsers(section.id);
				const bool = restrictedUsers.some(ru => ru.user_id === user.id);
				if (!bool) {
					return section;
				}
			}
		} else return section;
	}));

	return result;
};

export const addSection = async (data, courseId) => {
	data.id = uuid();

	return await sectionsData.create(data, courseId);
};

export const editSection = async (id, updateData, courseId) => {
	const sectionToUpdate = await sectionsData.getBy('id', id);

	if (!sectionToUpdate) {
		return null;
	}

	const updatedSection = { ...sectionToUpdate, ...updateData };

	return await sectionsData.edit(id, updatedSection, courseId);
};

export const removeSection = async (id) => {
	const sectionToDelete = await sectionsData.getBy('id', id);

	if (!sectionToDelete) return null;

	await sectionsData.remove(id);

	return sectionToDelete;
};

export const moveSection = async (courseId, sectionId, way) => {
	const sectionToMove = await sectionsData.getBy('id', sectionId);
	const allSections = await getAllSectionsBy('course_id', courseId);

	if (!sectionToMove) return null;

	if (way === 'down') {
		const sectionToReplace = allSections.filter(
			(section) => section.section_order === sectionToMove.section_order + 1
		);

		await sectionsData.moveDown(sectionId);
		await sectionsData.moveUp(sectionToReplace[0].id);
	} else if (way === 'up') {
		const sectionToReplace = allSections.filter(
			(section) => section.section_order === sectionToMove.section_order - 1
		);

		await sectionsData.moveUp(sectionId);
		await sectionsData.moveDown(sectionToReplace[0].id);
	}

	return await getAllSectionsBy('course_id', courseId);
};

export const getOrderBy = async (column, value) => {
	return await sectionsData.getOrderBy(column, value);
};

export const getSectionsCountByCourseId = async (courseId) => {
	return await sectionsData.getCountByCourseId(courseId);
};

export const removeAllSectionsByCourseId = async (courseId) => {
	await sectionsData.removeAllByCourseId(courseId);
};
