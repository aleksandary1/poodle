import { createContext, useContext, useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import { SOCKET_URL } from '../common/constants';
import AuthContext from './auth.provider';

const SocketContext = createContext();

export const useSocket = () => useContext(SocketContext);

const SocketProvider = ({ children }) => {
  const [socket, setSocket] = useState();
  const { user } = useContext(AuthContext);

  useEffect(() => {
    if (!user) return;
    const newSocket = io(SOCKET_URL);
    
    newSocket.emit('addUser', user.id);

    setSocket(newSocket);

    return () => newSocket.close();
  }, [user])

  return (
    <SocketContext.Provider value={socket}>
      {children}
    </SocketContext.Provider>
  )
}

export default SocketProvider;