import { body, validationResult } from 'express-validator';

const sectionValidator = [
  body('title').isLength({min: 5, max: 128}),
	body('content').isLength({ min: 5 }),
  // body('order').custom(value => {
  //   const order = Number(value);
  //   if (order < 1 || order > 127) {
  //     return Promise.reject('Order must be between 1 and 127');
  //   }
  // }),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({errors: errors.array()});
    next();
  },
];

export default sectionValidator;