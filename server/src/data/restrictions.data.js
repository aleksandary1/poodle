import pool from './db-pool.js';

const get = async (sectionId, userId) => {
  const sql = `
  SELECT *
  FROM restricted_sections_users
  WHERE section_id = ? AND user_id = ?;
  `;

  const restrictions = await pool.query(sql, [sectionId, userId]);
  
  return restrictions[0] || null;
};

const getUsers = async (sectionId) => {
  const sql = `
  SELECT rsu.user_id, rsu.section_id, u.email, u.first_name, u.last_name, u.is_teacher
  FROM restricted_sections_users AS rsu
  JOIN users AS u
  ON rsu.user_id = u.id
  AND rsu.section_id = ?
  AND u.is_teacher = 0;
  `;

  return await pool.query(sql, sectionId);
};

const restrict = async (sectionId, userId) => {
  const sql = `
  INSERT INTO restricted_sections_users (section_id, user_id)
  VALUES (?, ?);
  `;

  return await pool.query(sql, [sectionId, userId]);
};

const remove = async (sectionId, userId) => {
  const sql = `
  DELETE
  FROM restricted_sections_users
  WHERE section_id = ? AND user_id = ?;
  `;

  return await pool.query(sql, [sectionId, userId]);
};

const clear = async (sectionId) => {
  const sql = `
  DELETE
  FROM restricted_sections_users
  WHERE section_id = ?;
  `;

  return await pool.query(sql, sectionId);
};

const change = async (sectionId, isRestricted) => {
  const sql = `
  UPDATE sections
  SET is_restricted = ?
  WHERE id = ?
  `;

  return await pool.query(sql, [isRestricted, sectionId]);
};

const changeBy = async (sectionId, restriction) => {
  const sql = `
  UPDATE sections
  SET restricted_by = ?
  WHERE id = ?
  `;

  return await pool.query(sql , [restriction, sectionId]);
};

export default {
  get,
  getUsers,
  restrict,
  remove,
  clear,
  change,
  changeBy,
};