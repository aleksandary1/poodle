import { makeStyles } from '@material-ui/core';
import { deepPurple } from '@material-ui/core/colors';

export const useStyles = makeStyles(() => ({
	outlinedTextField: {
		width: '100%',
		marginBottom: '12px',
		[`& fieldset`]: {
			borderRadius: '15px',
			borderColor: deepPurple[500],
		},
		'& label.Mui-focused': {
			color: deepPurple[500],
			borderColor: 'solid 2px',
		},
		'& .MuiOutlinedInput-root': {
			'& fieldset': {
				borderWidth: '1.5px',
				borderColor: deepPurple[500],
			},
			'&.Mui-focused fieldset': {
				borderColor: deepPurple[500],
			},
		},
	},
	button: {
		marginLeft: '20px',
		float: 'right',
		background: '#fff',
		color: '#7e57c2',
		borderRadius: '8px',
		boxShadow: '0px 8px 12px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#9575cd',
			color: '#fff',
		},
	},
	media: {
		width: '65%',
		paddingTop: '38%', // 16:9
		borderRadius: '20px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.99)',
	},
	imageMessage: {
		color: '#1e6055',
		fontWeight: 'bold',
		marginBottom: '15px',
		marginTop: '0px',
		fontStyle: 'italic',
	},
}));
