import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
  titleWithButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '900px'
  },
  name: {
    fontSize: '25pt',
    marginBottom: 0
  },
  buttons: {
    marginTop: '25px',
  },
  description: {
    marginBottom: '30px',
    width: 700
  },
  cardButtons: {
    display: 'flex'
  },
  addModal: {
    position: 'absolute',
    width: 900,
    height: 700,
    backgroundColor: 'white',
    borderRadius: '20px',
    boxShadow: '5px',
    outline: 0,
    left: '30%',
    top: '13%'
  },
  addButton: {
    backgroundColor: 'green',
    '&:hover': {
      backgroundColor: 'green'
    }
  },
  cancelButton: {
    backgroundColor: 'red',
    '&:hover': {
      backgroundColor: 'red'
    },
    marginRight: '5px'
  },
  enrollButton: {
    height: 40,
    width: 120,
    marginTop: 40,
    fontSize: '22px',
		letterSpacing: 1,
		textTransform: 'none',
		background: '#9575cd',
		color: '#fff',
		borderRadius: '10px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#7e57c2',
			color: '#fff',
		},
  },
}));