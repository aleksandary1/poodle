import { List, ListItem, ListItemText, makeStyles } from '@material-ui/core'
import React from 'react'
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../../providers/auth.provider';

const useStyles = makeStyles(() => ({
  item: {
    padding: '2% 0 0 40%',
    fontSize: '10pt',
  },
  list: {
    paddingTop: 0,
  },
  itemText: {
    fontSize: '10pt'
  }
}));

const ProfileOptions = () => {
  const classes = useStyles();

  const { setUser } = useContext(AuthContext)

  const history = useHistory();

  const logout = () => {
    localStorage.removeItem('token');
    setUser(null);
    history.push('/')
  }

  return (
    <List className={classes.list}>
      {/* <ListItem button className={classes.item}>
        <ListItemText className={classes.itemText}>View Profile</ListItemText>
      </ListItem> */}
      <ListItem button className={classes.item} onClick={logout}>
        <ListItemText className={classes.itemText}>Logout</ListItemText>
      </ListItem>
    </List>
  )
}

export default ProfileOptions;
