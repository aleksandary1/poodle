import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import { changeRestriction, changeRestrictionBy, clearRestrictions, getRestrictedUsers, getRestriction, removeRestriction, restrictUser } from '../services/restrictions.service.js';

const router = express.Router();

router.use(authMiddleware);

router.get('/:sectionId', async (req, res) => {
  const { sectionId } = req.params;

  try {
    const restrictedUsers = await getRestrictedUsers(sectionId);
    res.status(200).send({status: 'success', result: restrictedUsers});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.put('/:sectionId/restrictBy', async (req, res) => {
  const { sectionId } = req.params;
  const { restrictBy } = req.body;

  try {
    await changeRestrictionBy(sectionId, restrictBy);
    res.status(200).send({status: 'success'});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.put('/:sectionId', async (req, res) => {
  const { sectionId } = req.params;
  const { isRestricted } = req.body;

  try {
    await changeRestriction(sectionId, isRestricted);
    res.status(200).send({status: 'success'});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.delete('/:sectionId', async (req, res) => {
  const { sectionId } = req.params;

  try {
    await clearRestrictions(sectionId);
    res.status(200).send({status: 'success'});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/:sectionId/:userId', async (req, res) => {
  const { sectionId, userId } = req.params;

  try {
    const restriction = getRestriction(sectionId, userId);
    res.status(200).send({status: 'success', result: restriction});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post('/:sectionId/:userId', async (req, res) => {
  const { sectionId, userId } = req.params;

  try {
    await restrictUser(sectionId, userId);
    res.status(200).send({status: 'success'});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.delete('/:sectionId/:userId', async (req, res) => {
  const { sectionId, userId } = req.params;

  try {
    await removeRestriction(sectionId, userId);
    res.status(200).send({status: 'success'});
  } catch (err) {
    res.status(500).send(err);
  }
});

export default router;