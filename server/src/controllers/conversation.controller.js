import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import { checkConversation, createConversation, getConversationsBy, getOtherUser } from '../services/conversations.service.js';

const router = express.Router();

router.use(authMiddleware);

router.post('/', async (req, res) => {
  const {creatorId, recieverId} = req.body;

  try {
    const newConversation = await createConversation(creatorId, recieverId);
    res.status(200).send({ status: 'success', result: newConversation });
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/', async (req, res) => {
  const { userId } = req.query;
  
  try {
    const conversations = await getConversationsBy('user_id', userId);
    res.status(200).send({ status: 'success', result: conversations});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post('/check', async (req, res) => {
  const { userOne, userTwo } = req.body;
  try {
    const check = await checkConversation(userOne, userTwo);
    res.status(200).send({ status: 'success', result: check});
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/:conversationId', async (req, res) => {
  const { conversationId } = req.params;

  try {
    const members = await getOtherUser(conversationId, req.user.id);
    res.status(200).send({ status: 'success', result: members });
  } catch (err) {
    res.status(500).send(err);
  }
});


export default router;