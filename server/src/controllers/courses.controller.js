import express from 'express';
import {
	addCourse,
	getCourseBy,
	updateCourse,
	deleteCourse,
	getCoursesByPage,
	getCoursesWithLimit,
	getCoursesBySearch,
	enrollUserInCourse,
	unenrollUserInCourse,
	getPublicCoursesByPage,
	getPublicCoursesBySearch,
	getCoursesByCreatorId,
	getEnrolledCoursesByUserId,
	getEnrolledStudentsByCourseId,
	getOverview,
	getAllCoursesTitles,
	createBullets,
	updateBullets,
	getCourses,
} from '../services/courses.service.js';
import {
	addSection,
	editSection,
	getAllSectionsByWithRestrictions,
	getOrderBy,
	getSectionBy,
	moveSection,
	removeSection,
} from '../services/sections.service.js';
import sectionValidator from '../validators/section.validator.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import usersService from '../services/users.service.js';
import multer from 'multer';
import quizService from '../services/quiz.service.js';

const storage = multer.diskStorage({
	destination: function (req, file, callback) {
		callback(null, '../client/src/uploads/');
	},
	filename: function (req, file, callback) {
		if (file) {
			const fileName = `${Date.now()}_${file.originalname}`;
			callback(null, fileName);
		}
	},
});

const upload = multer({ storage: storage });

const router = express.Router();

let isPrivateMode = null;

router.get('/getAllCourses', async (req, res) => {
	const result = await getCourses();
	return res.status(200).json({ status: 'success', result });
});

router.get('/overview', async (req, res) => {
	try {
		const result = await getOverview();

		return res.status(200).json({ status: 'success', result });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/', authMiddleware, async (req, res) => {
	const userId = req.user.id;
	const limit = Number(req.query.limit);

	const { page, elementsPerPage } = req.query;
	isPrivateMode = 0;

	const searchWord = req.query.title;
	const courseFilter = req.query.courseFilter;

	let searchInAll = null;

	switch (courseFilter) {
		case 'All':
			searchInAll = true;
			break;
		case 'Private':
			searchInAll = false;
			isPrivateMode = 1;
			break;
		case 'Public':
			searchInAll = false;
			isPrivateMode = 0;
			break;
	}

	if (limit) {
		try {
			const limitedResult = await getCoursesWithLimit(
				searchInAll,
				isPrivateMode,
				1,
				limit,
				userId
			);

			return res.status(200).send({ status: 'success', result: limitedResult });
		} catch (err) {
			res.status(500).send(err);
		}
	} else if (searchWord) {
		try {
			const courses = await getCoursesBySearch(
				searchInAll,
				isPrivateMode,
				+page,
				+elementsPerPage,
				searchWord,
				'title',
				userId
			);

			return res.status(200).send({ status: 'success', result: courses });
		} catch (err) {
			res.status(500).send(err);
		}
	} else if ((page, elementsPerPage)) {
		try {
			const result = await getCoursesByPage(
				searchInAll,
				isPrivateMode,
				+page,
				+elementsPerPage,
				userId
			);

			return res.status(200).send({ status: 'success', result });
		} catch (err) {
			res.status(500).send(err);
		}
	}
});

router.get('/titles', async (req, res) => {
	const result = await getAllCoursesTitles();

	return res.status(200).send({ status: 'success', result });
});

router.get('/myCourses', authMiddleware, async (req, res) => {
	const { page, elementsPerPage, userId } = req.query;

	const searchWord = req.query.title;

	const user = await usersService.getById(userId);
	const isUserTeacher = user.isTeacher;

	let result = null;

	try {
		if (isUserTeacher) {
			const courseFilter = req.query.courseFilter;

			let searchInAll = null;

			switch (courseFilter) {
				case 'All':
					searchInAll = true;
					break;
				case 'Private':
					searchInAll = false;
					isPrivateMode = 1;
					break;
				case 'Public':
					searchInAll = false;
					isPrivateMode = 0;
					break;
			}

			result = await getCoursesByCreatorId(
				searchInAll,
				isPrivateMode,
				userId,
				+page,
				+elementsPerPage,
				searchWord
			);
		} else {
			result = await getEnrolledCoursesByUserId(
				userId,
				+page,
				+elementsPerPage,
				searchWord
			);
		}

		return res.status(200).send({ status: 'success', result });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/public', async (req, res) => {
	const { page, elementsPerPage, userId } = req.query;
	isPrivateMode = 1;

	const searchWord = req.query.title;

	try {
		if (searchWord) {
			const result = await getPublicCoursesBySearch(
				userId,
				isPrivateMode,
				+page,
				+elementsPerPage,
				searchWord,
				'title'
			);

			return res.status(200).send({ status: 'success', result });
		} else {
			const result = await getPublicCoursesByPage(
				userId,
				isPrivateMode,
				+page,
				+elementsPerPage
			);

			return res.status(200).send({ status: 'success', result });
		}
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/:courseId', authMiddleware, async (req, res) => {
	const { courseId } = req.params;

	try {
		const result = await getCourseBy('id', courseId);

		return res.status(200).send({ status: 'success', result: result });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/:courseId/enroll', authMiddleware, async (req, res) => {
	const { courseId } = req.params;
	const user = req.user;

	try {
		const result = await enrollUserInCourse(courseId, user);

		return res.status(200).send({ status: 'success', result });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/:courseId/unenroll', authMiddleware, async (req, res) => {
	const { courseId } = req.params;
	const { userId } = req.query;

	try {
		await unenrollUserInCourse(courseId, userId);

		return res.status(200).send({ status: 'success' });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/:courseId/enrolled', authMiddleware, async (req, res) => {
	const { courseId } = req.params;

	try {
		const enrolledUsers = await getEnrolledStudentsByCourseId(courseId);
		res.status(200).send({ status: 'success', result: enrolledUsers });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.post(
	'/',
	authMiddleware,
	upload.single('courseImage'),
	async (req, res) => {
		try {
			const course = JSON.parse(req.body.course);

			const file = req.file;
			let fileName = null;

			if (file) {
				fileName = file.filename;
			}

			const add = await addCourse({
				...course,
				imageName: fileName,
			});

			if (file) {
				const timeout = Math.min(5000, 1000 * (file.size / 20000));

				setTimeout(() => {
					return res.status(200).send({ status: 'success', result: add });
				}, timeout);
			} else {
				return res.status(200).send({ status: 'success', result: add });
			}
		} catch (err) {
			res.status(500).send(err);
		}
	}
);

router.post('/:courseId/bullets', authMiddleware, async (req, res) => {
	const { courseId } = req.params;
	const bullets = req.body;

	const result = await createBullets(courseId, bullets);

	return res.status(200).send(result);
});

router.put('/:courseId/bullets', authMiddleware, async (req, res) => {
	const bullets = req.body;

	try {
		const result = await updateBullets(bullets);
		res.status(200).send({ status: 'success', result });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.put(
	'/:courseId',
	authMiddleware,
	upload.single('courseImage'),
	async (req, res) => {
		const { courseId } = req.params;
		const course = JSON.parse(req.body.course);

		const file = req.file;
		let fileName = null;

		if (file) {
			fileName = file.filename;
		}

		const updatedCourse = await updateCourse(courseId, {
			id: courseId,
			...course,
			imageName: fileName,
		});

		if (file) {
			const timeout = Math.min(5000, 1000 * (file.size / 20000));

			setTimeout(() => {
				return res
					.status(200)
					.send({ status: 'success', result: updatedCourse });
			}, timeout);
		} else {
			return res.status(200).send({ status: 'success', result: updatedCourse });
		}
	}
);

router.delete('/:courseId', authMiddleware, async (req, res) => {
	const { courseId } = req.params;

	try {
		const deletedCourse = await deleteCourse(courseId);

		res.status(200).send({ status: 'success', result: deletedCourse });
	} catch (err) {
		res.status(500).send(err);
	}
});

// Sections Part

router.get('/:courseId/sections', authMiddleware, async (req, res) => {
	const { courseId } = req.params;
	const user = req.user;

	try {
		let result = await getAllSectionsByWithRestrictions(
			'course_id',
			courseId,
			user
		);
		if (!result[0]) result = [];
		const order = await getOrderBy('course_id', courseId);

		res.status(200).send({ status: 'success', result, order: order.count });
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get(
	'/:courseId/sections/:sectionId',
	authMiddleware,
	async (req, res) => {
		const { sectionId } = req.params;

		try {
			const result = await getSectionBy('id', sectionId);

			res.status(200).send({ status: 'success', result });
		} catch (err) {
			res.status(500).send(err);
		}
	}
);

router.post(
	'/:courseId/sections',
	authMiddleware,
	sectionValidator,
	async (req, res) => {
		const { courseId } = req.params;

		try {
			const add = await addSection(req.body, courseId);

			return res.status(200).send({ status: 'success', result: add });
		} catch (err) {
			res.status(500).send(err);
		}
	}
);

router.put(
	'/:courseId/sections/:sectionId',
	authMiddleware,
	async (req, res) => {
		const { order } = req.query;
		const { courseId, sectionId } = req.params;

		try {
			if (order) {
				const sections = await moveSection(courseId, sectionId, order);

				res.status(200).send({ status: 'success', result: sections });
			} else {
				const updateData = req.body;

				const updatedSection = await editSection(
					sectionId,
					updateData,
					courseId
				);

				res.status(200).send({ status: 'success', result: updatedSection });
			}
		} catch (err) {
			res.status(500).send(err);
		}
	}
);

router.delete(
	'/:courseId/sections/:sectionId',
	authMiddleware,
	async (req, res) => {
		const { sectionId } = req.params;

		try {
			const deletedSection = await removeSection(sectionId);

			res.status(200).send({ status: 'success', result: deletedSection });
		} catch (err) {
			res.status(500).send(err);
		}
	}
);

router.post('/:courseId/createQuiz', authMiddleware, async (req, res) => {
	const { courseId } = req.params;
	const quiz = req.body;

	await quizService.create(courseId, quiz);

	return res.status(200);
});

router.post('/:courseId/takeQuiz', authMiddleware, async (req, res) => {
	const quizResult = req.body;

	const result = await quizService.userQuizResult(quizResult);

	return res.status(200).send({ status: 'success', result: result });
});

export default router;
