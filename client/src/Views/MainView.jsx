import { Box, makeStyles } from '@material-ui/core';
import React from 'react';
import Sidebar from '../Components/Sidebar/Sidebar';

const useStyles = makeStyles(() => ({
	root: {
		fontFamily: 'Lato',
		backgroundColor: '#edeff6',
		height: '100vh',
		overflow: 'scroll',
	},
	content: {
		width: 'calc(100% - 400px)',
		position: 'relative',
		left: '300px',
		padding: '0 50px',
	},
}));

const MainView = ({ children, page }) => {
	const classes = useStyles();

	return (
		<Box className={classes.root}>
			<Sidebar page={page} />
			<Box className={classes.content}>{children}</Box>
		</Box>
	);
};

export default MainView;
