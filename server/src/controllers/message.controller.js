import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import { getMessagesBy, saveMessage } from '../services/message.service.js';

const router = express.Router();

router.use(authMiddleware);

router.post('/', async (req, res) => {
	const messageData = req.body;
	// conversationId, sender, text

	try {
		const messages = await saveMessage(messageData);
		res.status(200).send(messages);
	} catch (err) {
		res.status(500).send(err);
	}
});

router.get('/:conversationId', async (req, res) => {
	const { conversationId } = req.params;

	try {
		const messages = await getMessagesBy('conversation_id', conversationId);
		res.status(200).send(messages);
	} catch (err) {
		res.status(500).send(err);
	}
});

export default router;
