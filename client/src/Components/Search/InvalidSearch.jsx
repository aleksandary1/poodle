import React from 'react';
import { Box, Typography } from '@material-ui/core';

const suggestions = [
	'Make sure all words are spelled correctly',
	'Try different search terms',
	'Try more general search terms',
];

const InvalidSearch = ({ searchWord }) => {
	return (
		<Box>
			<Typography variant="h4" style={{ margin: '20px 0' }}>
				Sorry, we couldn't find any results for "{searchWord}"
			</Typography>
			<Typography variant="h6" style={{ margin: '10px 0' }}>
				Try adjusting your search. Here are some ideas:
			</Typography>
			{suggestions.map((suggestion, index) => (
				<Typography variant="subtitle1" style={{ marginLeft: '20px' }}>
					{index + 1}. {suggestion}
				</Typography>
			))}
		</Box>
	);
};

export default InvalidSearch;
