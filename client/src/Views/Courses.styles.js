import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
	header: {
		height: '100px',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		margin: '40px 0',
		marginRight: '50px',
	},
	filterOptions: { display: 'flex', alignItems: 'center' },
	pagination: { display: 'flex', justifyContent: 'center', margin: '20px' },
}));
