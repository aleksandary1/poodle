import { v4 as uuid } from 'uuid';
import messagesData from '../data/messages.data.js';
import { normalizeEntityProperties } from './entity-mapper.service.js';

export const getMessagesBy = async (column, value) => {
  const messages = await messagesData.getBy(column, value);

  return messages.map(m => normalizeEntityProperties(m));
};

export const saveMessage = async (data) => {
  data.id = uuid();

  const message = await messagesData.saveMessage(data);

  return message.map(m => normalizeEntityProperties(m));
};