import { ChatBubble, Home, MenuBook, Grade } from '@material-ui/icons';

const options = [
	{ icon: <Home />, name: 'Home', link: '/', page: 'home' },
	{ icon: <MenuBook />, name: 'Courses', link: '/courses', page: 'courses' },
	{
		icon: <Grade />,
		name: 'My Courses',
		link: '/myCourses',
		page: 'myCourses',
	},
	{ icon: <ChatBubble />, name: 'Chat', link: '/chat', page: 'chat' },
];

export default options;
