import pool from './db-pool.js';

const getAllUsers = async () => {
	const sql = `
  SELECT id, email, first_name, last_name, created_on, is_teacher
  FROM users
  WHERE is_deleted = 0;
  `;

	return await pool.query(sql);
};

const getAllByRole = async (role) => {
	const sql = `
	SELECT id, email, first_name, last_name, created_on, is_teacher
  FROM users
  WHERE is_teacher = ? AND is_deleted = 0
	ORDER BY first_name ASC
	;`;

	return await pool.query(sql, [role]);
};

const getBy = async (column, value) => {
	const sql = `
  SELECT id, email, first_name, last_name, created_on, is_teacher
  FROM users
  WHERE ${column} = ? AND is_deleted = 0;
  `;

	const result = await pool.query(sql, [value]);
	return result[0];
};

const getAllBy = async (column, value) => {
	const sql = `
  SELECT *
  FROM users
  WHERE ${column} = ? AND is_deleted = 0;
  `;

	const result = await pool.query(sql, [value]);
	return result[0];
};

const getUserPasswordByEmail = async (email) => {
	const sql = `
  SELECT id, email, password, is_teacher
  FROM users
  WHERE email = ?;
  `;

	const result = await pool.query(sql, [email]);
	return result[0];
};

const create = async (user) => {
	const { id, email, password, firstName, lastName } = user;

	const sql = `
  INSERT INTO users (id, email, password, first_name, last_name, created_on)
  VALUES (?, ?, ?, ?, ?, NOW());
  `;

	await pool.query(sql, [id, email, password, firstName, lastName]);

	const createdUser = await getBy('id', id);
	return createdUser;
};
const insertTeacher = async (teacher) => {
	const { id, email, password, firstName, lastName } = teacher;

	const sql = `
  INSERT INTO users (id, email, password, first_name, last_name, created_on, is_teacher)
  VALUES (?, ?, ?, ?, ?, NOW(), 1);
  `;

	await pool.query(sql, [id, email, password, firstName, lastName]);
};

const update = async (updatedData) => {
	const { id, email, firstName, lastName } = updatedData;

	const sql = `
  UPDATE users
  SET email = ?, first_name = ?, last_name = ?
  WHERE id = ?;
  `;

	await pool.query(sql, [email, firstName, lastName, id]);

	return await getBy('id', id);
};

const remove = async (id) => {
	const sql = `
  UPDATE users
  SET is_deleted = 1
  WHERE id = ?;
  `;

	await pool.query(sql, [id]);

	const getDeletedUserSql = `
  SELECT id, email, first_name, last_name, created_on, is_teacher
  FROM users
  WHERE id = ?;
  `;

	const deletedUser = await pool.query(getDeletedUserSql, [id]);
	return deletedUser[0];
};

export default {
	getAll: getAllUsers,
	getAllByRole,
	getBy,
	getAllBy,
	getUserPasswordByEmail,
	create,
	insertTeacher,
	update,
	remove,
};
