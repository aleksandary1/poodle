

<h1 align="center">
  <img src="client/src/Media/SidebarLogo.png">
</h1>

<h2 align="center">E-learn single page application built on React</h4>


## Key Features
* User-friendly and intuitive learner interface
* Authoring tools
* Chat
* The content of the sections supports HTML
* Quizzes and Leaderboard

## How to setup the project
  #### Server
  1. Open the `server` folder and create an `.env` file and insert the following information in it: 
      ```
      PORT=(the port for express)
      DB_HOST=(IP of the database)
      DB_PORT=(port of the database)
      DB_USER=(username to connect to the database)
      DB_PASS=(password of the user)
      DATABASE=(the name of the database)
      SECRET_KEY=(the key to encrypt the password with)
      TOKEN_LIFETIME=(the time in milliseconds that the key will be active)
      ```
  2. Open a new terminal in the `server` folder and type in <b>npm install</b>. After it's done type in type in <b>npm seed</b>. This command will create the entire database and will import all the users and data that is needed to see the project in it's full potential.
  #### Socket
  1. Open the `chatSocket` folder and there open a new terminal, then you type in <b>npm install</b> to install all the dependencies.
  2. Run in the terminal <b>npm start</b> to start the socket.
  #### Client
  1. Open the `client` folder and there open a new terminal, then you type in <b>npm install</b> to install all the dependencies.
  2. Head into `src/common/constants.js` and change the port of the `BASE_URL` constant to the proper one.
  3. Run in the terminal <b>npm start</b> to start the client.

  ## How to run the project?
  * Open a terminals in `client`, `server`, `chatSocket` folder and type in each of them <b>npm start</b>.

  ## Project Structure
  #### Server:
  - `src/app.js` - the entry point of the project, here are all the routers
  - `src/auth` - contains the files for the authentication. There are three files in the folder - `auth.middleware.js`, `create_token.js`, `strategy.js`
  - `src/common` - shared resources in the project such as constants. There are two files in the folder - `config.js`, `courses-mapping-config.js`
  - `src/controllers` - contains the controllers for each endpoint. There are five files in the folder - `conversation.controller.js`, `courses.controller.js`, `message.controller.js`, `restriction.controller.js`, `users.controller.js`
  - `src/data` - contains all the operations for the database. There are one folder and eight files in the folder - `mock_data`, `conversations.data.js`, `courses.data.js`, `db-pool.js`, `messages.data.js`, `restrictions.data.js`, `sections.data.js`, `seed.js`, `users.data.js`
  - `src/services` - contains all the operations between controllers and data. There are seven files in the folder - `conversations.service.js`, `courses.service.js`, `entity-mapper.service.js`, `message.service.js`, `restrictions.service.js`, `sections.service.js`, `users.service.js`
  - `src/validators` - contains all the validators for the request body. There are 4 files in the folder - `course.validator.js`, `login.validator.js`, `register.validator.js`, `section.validator.js`
  - `Database.sql` - contains the SQL dump
  
  #### Socket
  - `index.js` - contains all the endpoints for the socket
  
  #### Client
  - `src/App.js` - the entry point of the project
  - `src/common` - contains shared resources in the project
  - `src/Components` - contains all components in the project in subfolders: <br> - 
  Conversation - component that displays each user that you had conversation with.<br> -
  CourseCard - card that displays the info about the course <br> - 
  CourseOverview - more detailed course info that is displayed when hovering the course <br> -
  Dropdown - reusable dropdown component <br> -
  FileUpload - component for uploading photos to courses <br> - 
  LinearStepper - stepper to show on which step you are <br> - 
  Login - login page component <br> -
  Register - register page component <br> -
  Message - message component that gets the message and displays it like a blob <br> - 
  Modals - reusable modal component <br> - 
  ScrollToTopArrow <br> -
  Search - reusable search component <br> - 
  Sections <br> - 
  Sidebar <br> - 
  SquareProgress - reusable component to show some progress <br> - 
  Switch - switch made like the site theme <br> - 
  `ProtectedRoute.jsx` - extends the react-router to check if user is logged
  - `src/Media` - all the photos for the app
  - `src/providers` - authentication and socket providers
  - `src/Requests` - all the request to the server
  - `src/style` - some styling for components
  - `src/utils` - utility functions
  - `src/Views` - All the views in the app
  
  ## Designing the database
  1. The users table <br>
     - `id` - the ID of the user
     - `email` - the email of the user
     - `password` - hash of the password
     - `first_name` - first name of the user
     - `last_name` - last name of the user
     - `is_teacher` - boolean to check if the user is teacher
     - `created_on` - the date on which the user is created
     - `is_deleted` - boolean to check if the user is deleted
  2. The conversations table <br>
     - `id` - the ID of the conversation
     - `admin_id` - the ID of the creator of the conversation
     - `is_deleted` - boolean to check if the user is deleted
  3. The conversation_users table <br>
     - `conversation_id` - the ID of the conversation
     - `user_id` - the ID of the user that participates in the conversation
     - `is_deleted` - boolean to check if the user is removed
  4. The courses table <br>
     - `id` - the ID of the course
     - `title` - the title of the course
     - `description` - the description of the course
     - `is_private` - boolean to check if the course is private
     - `created_on` - the date on which the course is created
     - `is_deleted` - boolean to check if the course is deleted
     - `creator_id` - the ID of the user that created the course
     - `last_modified` - timestamp on which the course was last modified
  5. The enrolled_users table <br>
     - `user_id` - the ID of the user that is enrolled in the course
     - `course_id` - the ID of the course that the user is enrolled in
  6. The messages table <br>
     - `id` - the ID of the message
     - `conversation_id` - the ID of the conversation that the message is in
     - `user_id` - the ID of the sender
     - `message` - the message
     - `created_on` - the date on which the message was created
     - `is_deleted` - boolean to check if the course is deleted
  7. The sections table <br>
     - `id` - the ID of the section
     - `title` - the title of the section
     - `content` - the content of the section
     - `section_order` - number on which is the section
     - `restriction_date` - the date that the course is restricted to
     - `is_deleted` - boolean to check if the course is deleted
     - `is_restricted` - boolean to check if the course is restricted
     - `course_id` - the ID of the course that the section is in
     - `restricted_by` - checks by what the section is restricted
  8. The restricted_sections_users table <br>
     - `user_id` - the ID of the user that is restricted
     - `section_id` - the ID of the restricted section
  
  ## Endpoints
   1. Users
      | request | name | description |
      |---|---|---|
      | GET | / | get all users |
      | GET | /:id | get all users by ID |
      | POST | /register | create user  |
      | POST | /login | create JWT token for user |
      | POST | /:id | update user |
      | DELETE | /:id | delete user |
   2. Courses & Sections
      #### Courses
      | request | name | description |
      |---|---|---|
      | GET | /  | get all courses  |
      | GET | /overview | gets overview details of the course |
      | GET | /myCourses | gets all courses the user is enrolled in |
      | GET | /public | gets all public courses |
      | GET | /:courseId | get course by its ID |
      | GET | /:courseId/enroll | enrolls user in course |
      | GET | /:courseId/unenroll | unenrolls user in course |
      | GET | /:courseId/enrolled | gets all enrolled users in the course|
      | POST | / | add new course |
      | POST | /uploadPicture | upload a picture |
      | PUT | /:courseId | update course |
      | DELETE | /:courseId | delete course |
      #### Sections
      | request | name | description |
      |---|---|---|
      | GET | /:courseId/sections | get all sections for a course |
      | GET | /:courseId/sections/:sectionId | get section by ID |
      | POST | /:courseId/sections | add new section |
      | PUT | /:courseId/sections/:sectionId | update or move a section |
      | DELETE | /:courseId/sections/:sectionId | delete section |
   3. Restrictions
      | request | name | description |
      |---|---|---|
      | GET | /:sectionId | get all restricted users |
      | PUT | /:sectionId/restrictBy | restrict section by users or date |
      | PUT | /:sectionId | change section restriction |
      | DELETE | /:sectionId | remove restrictions |
      | GET | /:sectionId/:userId | get section restriction for user |
      | POST | /:sectionId/:userId | restrict section for user |
      | DELETE | /:sectionId/:userId | remove restriction for user |
   4. Conversations
      | request | name | description |
      |---|---|---|
      | POST | / | create a new conversation |
      | GET | / | get all conversations |
      | POST | /check | check if two users have conversation |
      | GET | /:conversationId | get other user in the conversation |
   5. Messages
      | request | name | description |
      |---|---|---|
      | POST | / | create a message |
      | GET | /:conversationId | get all messages for conversation |

  ## Authors
  * Kalina Georgieva
  * Aleksandar Yanakiev
  

  Enjoy using our e-learn platform!

  <b>Thank you!</b>