import React from 'react';
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AuthContext from '../../providers/auth.provider';
import { Box, Button, IconButton, Input, InputAdornment, makeStyles } from '@material-ui/core';
import { Notifications, Search } from '@material-ui/icons';

const useStyles = makeStyles(() => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
	},

	buttons: {
		display: 'flex',
		marginTop: '10px',
		marginRight: '2%',
	},
	coinButton: {
		backgroundColor: 'lightgreen',
		'&:hover': {
			backgroundColor: 'lightgreen',
		},
		marginRight: '10px',
	},
	notificationButton: {
		borderRadius: '50px',
	},
	loginButtons: {
		backgroundColor: 'green',
		marginRight: '10px',
		'&:hover': {
			backgroundColor: 'green',
		},
	},
}));

const Header = ({ hideSearch }) => {
	const classes = useStyles();

	const { user } = useContext(AuthContext);

	const history = useHistory();

	const loggedUser = (
		<Box className={classes.buttons}>
			<Button variant="contained" className={classes.coinButton}>
				Coins: 500
			</Button>
			<Box>
				<IconButton className={classes.notificationButton}>
					<Notifications />
				</IconButton>
			</Box>
		</Box>
	);

	const loggedOutUser = (
		<Box className={classes.buttons}>
			<Button
				className={classes.loginButtons}
				variant="contained"
				onClick={() => history.push('/login')}
			>
				Login
			</Button>
			<Button
				className={classes.loginButtons}
				variant="contained"
				onClick={() => history.push('/register')}
			>
				Register
			</Button>
		</Box>
	);

	return (
		<Box className={classes.root}>
			{hideSearch ? null : (
			<Box className={classes.search}>
				<Input
					startAdornment={
						<InputAdornment position="start">
							<Search />
						</InputAdornment>
					}
					placeholder="Search"
					disableUnderline={true}
				/>
			</Box>
			)}
			{user ? null : loggedOutUser}
		</Box>
	);
};

export default Header;
