import { useState } from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { Button } from '@material-ui/core';

const ScrollToTopArrow = () => {
	const [showScroll, setShowScroll] = useState(false);

	const checkScrollTop = () => {

		if (!showScroll && window.pageYOffset > 100) {
			setShowScroll(true);
		} else if (showScroll && window.pageYOffset <= 100) {
			setShowScroll(false);
		}
	};

	const scrollTop = () => {
		window.scrollTo({ top: 0, behavior: 'smooth' });
	};

	window.addEventListener('scroll', checkScrollTop);

	return (
		<Button onClick={scrollTop}>
			<ArrowBackIosIcon />
		</Button>
	);
};

export default ScrollToTopArrow;
