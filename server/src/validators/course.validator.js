import { body, validationResult } from 'express-validator';
import { getCourseBy } from '../services/courses.service.js';
import usersService from '../services/users.service.js';

const courseValidator = [
	body('title').custom(async (value) => {
		const exists = await getCourseBy('title', value);

		if (exists) {
			return Promise.reject('Title must be unique');
		}
	}),
	body('description').isLength({ min: 10 }),
	body('isPrivate').isBoolean(),
	body('creatorId').custom(async (value) => {
		const exists = await usersService.getBy('id', value);

		if (!exists) {
			return Promise.reject('Non-existing creator id');
		}
	}),
	body('enrolledStudents').isArray(),
	(req, res, next) => {
		const errors = validationResult(req);

		if (!errors.isEmpty())
			return res.status(422).json({ errors: errors.array() });
		next();
	},
];

export default courseValidator;
