import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
	root: {
		fontFamily: 'Lato',
		height: '100%',
		maxWidth: '350px',
		background: 'none',
	},
	media: {
		paddingTop: '56.25%', // 16:9
		borderRadius: '20px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.99)',
	},
	title: {
		fontSize: '100%',
		fontWeight: 'bold',
		marginTop: '8px',
	},
	subtitle: {
		fontSize: '85%',
		color: 'grey',
	},
	paper: {
		padding: '15px',
	},
	h6: {
		fontWeight: 'bold',
		fontSize: '25px',
	},
}));
