import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
	search: {
		position: 'relative',
		marginRight: '15px',
	},
	searchIcon: {
		color: 'grey',
		zIndex: '5',
		padding: theme.spacing(0, 1),
		position: 'absolute',
		top: '11px',
		left: '5px',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	inputRoot: {
		height: '50px',
		background: 'white',
		boxShadow: '0px 5px 8px -3px rgb(0 0 0 / 14%)',
		borderStyle: 'none',
		borderWidth: '2px',
		borderRadius: '12px',
	},
	inputInput: {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('md')]: {
			width: '20ch',
		},
	},
}));
