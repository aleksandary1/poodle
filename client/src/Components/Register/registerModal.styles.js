import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
	modal: {
		position: 'relative',
		display: 'flex',
		alignItems: 'center',
		marginBottom: '300px',
		justifyContent: 'center',
	},
	paper: {
		backgroundColor: theme.palette.background.paper,
		border: '2px solid blue',
		boxShadow: theme.shadows[10],
		padding: theme.spacing(2, 4, 3),
	},
	link: {
		textDecoration: 'none',
		color: 'blue',
	},
}));
