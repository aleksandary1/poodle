import { Box } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useStyles } from './conversation.styles'
import ProfilePic from '../../Media/ProfilePicture.jpg'
import usersRequests from '../../Requests/users.requests';
import conversationsRequests from '../../Requests/conversations.requests';

const Conversation = ({ conversationId, selected }) => {
  const [user, setUser] = useState(null);
  const classes = useStyles();

  useEffect(() => {

    conversationsRequests.getFriend(conversationId)
    .then(data => {
      usersRequests.getById(data.result[0].userId)
    .then(data => setUser(data.result));
    })
  }, [conversationId]);

  return (
    <>
    <Box className={selected ? `${classes.root} ${classes.selected}` : classes.root}>
      {/* <img className={classes.image} src={ProfilePic} alt='profile'/> */}
      <p style={{fontSize: 20}}>{user?.firstName}</p>
    </Box>
    </>
  )
}

export default Conversation
