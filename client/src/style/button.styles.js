import { makeStyles } from '@material-ui/core/styles';

export const btnStyles = makeStyles(() => ({
	btn: {
		width: '100px',
		height: '100%',
		marginRight: '15px',
		fontSize: '22px',
		letterSpacing: 1,
		textTransform: 'none',
		background: '#9575cd',
		color: '#fff',
		borderRadius: '10px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#7e57c2',
			color: '#fff',
		},
	},
}));
