import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
  editorButtons: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: '5px 5px 5px 0'
  },
  applyButton: {
    backgroundColor: 'green',
    '&:hover': {
      backgroundColor: 'green'
    }
  },
  cancelButton: {
    backgroundColor: 'red',
    '&:hover': {
      backgroundColor: 'red'
    },
    marginRight: '5px'
  },
  modal: {
    position: 'absolute',
    width: 400,
    backgroundColor: 'white',
    borderRadius: '20px',
    outline: 0,
    left: '40%',
    top: '40%'
  },
  modalButtons: {
    marginRight: '15px',
    marginBottom: '15px'
  }
}));