import { Box, Button, Modal } from '@material-ui/core'
import React from 'react'
import { useStyles } from './deleteSectionModal.styles'

const DeleteSectionModal = ({ openModal, setOpenModal, onClick: deleteSection }) => {
  const classes = useStyles();

  return (
    <Modal
        open={openModal}
        onClose={() => setOpenModal(false)}  
      >
        <Box className={classes.modal}>
          <h2 style={{marginLeft: '10px'}}>Are you sure?</h2>
          <Box className={`${classes.editorButtons} ${classes.modalButtons}`}>
            <Button
            className={classes.cancelButton}
            onClick={() => setOpenModal(false)}
            >
              No
            </Button>
            <Button
            className={classes.applyButton}
            onClick={() => deleteSection()}
            >
              Yes
            </Button>
          </Box>
        </Box>
      </Modal>
  )
}

export default DeleteSectionModal
