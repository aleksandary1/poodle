import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
	root: {
		position: 'fixed',
		width: '100%',
		left: '47.5%',
		bottom: '40px',
		height: '20px',
		fontSize: '3rem',
		zIndex: 1,
		cursor: 'pointer',
		transform: 'rotate(90deg)',
	},
}));
