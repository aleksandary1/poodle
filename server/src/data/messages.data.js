import pool from './db-pool.js';

const getBy = async (column, value) => {
  const sql = `
  SELECT *
  FROM messages
  WHERE ${column} = ? AND is_deleted = 0
  ORDER BY created_on asc;
  `;

  return await pool.query(sql, value);
};

const saveMessage = async (messageData) => {
  const sql = `
  INSERT INTO messages (id, conversation_id, user_id, message)
  VALUES (?, ?, ?, ?)
  `;

  await pool.query(sql,
    [messageData.id,
    messageData.conversationId,
    messageData.userId,
    messageData.text,
    ]);

  return await getBy('id', messageData.id);
};

export default {
  getBy,
  saveMessage,
};