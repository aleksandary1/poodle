/* eslint-disable import/no-anonymous-default-export */
import { BASE_URL } from '../common/constants';
import { createRequest } from './requests.helper';

const login = (data) => {
	const request = createRequest('POST', {
		body: JSON.stringify(data),
	});

	return fetch(`${BASE_URL}/users/login`, request).then((res) => res.json());
};

const register = (data) => {
	const request = createRequest('POST', {
		body: JSON.stringify(data),
	});

	return fetch(`${BASE_URL}/users/register`, request).then((res) => res.json());
};

const getById = (id) => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/users/${id}`, request).then((res) => res.json());
};

const getAllByRole = (role) => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/users?role=${role}`, request).then((res) =>
		res.json()
	);
};

const getAll = () => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/users`, request)
	.then((res) => res.json());
}

export default { 
	login,
	register,
	getById,
	getAllByRole,
	getAll,
};
