/* eslint-disable no-unused-vars */
import pool from './db-pool.js';
import { v4 as uuid } from 'uuid';

const getOverview = async () => {
	const sql = `
	SELECT COUNT(*) AS courses_count,
		(SELECT COUNT(*) FROM sections s WHERE s.is_deleted = 0) AS sections_count,
    (SELECT COUNT(*) FROM users st WHERE st.is_deleted = 0 AND st.is_teacher = 0 ) AS students_count,
    (SELECT COUNT(*) FROM users t WHERE t.is_deleted = 0 AND t.is_teacher = 1 ) AS teachers_count
	FROM courses c WHERE c.is_deleted = 0;`;

	const result = await pool.query(sql);
	return result[0];
};

const getAll = async () => {
	const sql = `
  SELECT *
  FROM courses
  WHERE is_deleted = 0
  `;

	return await pool.query(sql);
};

const getAllTitles = async () => {
	const sql = `
  SELECT title
  FROM courses
  WHERE is_deleted = 0
  `;

	return await pool.query(sql);
};

const getAllByCreatorId = async (
	searchInAll,
	isPrivate,
	id,
	skipElements,
	elementsPerPage,
	searchWord
) => {
	const sql = `
	SELECT *,
		(SELECT COUNT(s.course_id) FROM sections AS s WHERE s.course_id = c.id AND s.is_deleted = 0) AS sections_count
  FROM courses c
  WHERE  ${
		searchInAll ? '' : courseTypeWhereClause(isPrivate)
	}  c.creator_id = ? AND c.is_deleted = 0 ${
		searchWord ? addSearchWhereClause(searchWord) : ''
	}
	ORDER BY last_modified DESC
	LIMIT ?, ?;
  `;

	return await pool.query(sql, [id, skipElements, elementsPerPage]);
};

const getCountByCourseStatus = async (all, isPrivate) => {
	const sql = `
		SELECT COUNT(*)
		FROM courses AS c
		WHERE ${all ? '' : courseTypeWhereClause(isPrivate)} c.is_deleted = 0;
`;

	const _ = await pool.query(sql);
	const coursesCount = _[0]['COUNT(*)'];
	return coursesCount;
};

const getCountByCreatorId = async (id, searchInAll, isPrivate, searchWord) => {
	const sql = `
	SELECT COUNT(*)
	FROM courses c
	WHERE ${
		searchInAll ? '' : courseTypeWhereClause(isPrivate)
	}  c.creator_id = ? AND c.is_deleted = 0 ${
		searchWord ? addSearchWhereClause(searchWord) : ''
	} ;
	`;

	const _ = await pool.query(sql, [id]);
	const coursesCount = _[0]['COUNT(*)'];
	return coursesCount;
};

const getCountOfEnrolledCoursesByUserId = async (userId) => {
	const sql = `
	SELECT COUNT(*)
	FROM enrolled_users
	WHERE user_id = ?;
	`;

	const _ = await pool.query(sql, [userId]);
	const coursesCount = _[0]['COUNT(*)'];
	return coursesCount;
};

const getCountOfEnrolledCoursesByUserIdWithSearch = async (
	userId,
	column,
	searchWord
) => {
	const sql = `
	SELECT COUNT(*)
	FROM courses c
	JOIN enrolled_users eu ON c.id = eu.course_id
	WHERE eu.user_id = ? AND c.is_deleted = 0 AND ${column} LIKE '%${searchWord}%';
	`;

	const _ = await pool.query(sql, [userId]);
	const coursesCount = _[0]['COUNT(*)'];
	return coursesCount;
};

const getAllCount = async () => {
	const sql = `
	SELECT COUNT(*)
	FROM courses
	WHERE is_deleted = 0;
	`;

	const _ = await pool.query(sql);
	const coursesCount = _[0]['COUNT(*)'];
	return coursesCount;
};

const getWithLimit = async (
	all,
	isPrivate,
	skipElements,
	elementsPerPage,
	userId
) => {
	const sql = `
	SELECT c.id, c.title, c.description, c.last_modified, c.created_on, u.first_name, u.last_name,
		(SELECT COUNT(s.course_id) FROM sections AS s WHERE s.course_id = c.id AND s.is_deleted = 0) AS sections_count,
		(SELECT COUNT(*) FROM enrolled_users AS eu WHERE c.id = eu.course_id AND eu.user_id = ? ) AS is_student_enrolled
	FROM courses c
	JOIN users u ON u.id = c.creator_id
	WHERE ${all ? '' : courseTypeWhereClause(isPrivate)} c.is_deleted = 0
	ORDER BY last_modified DESC
	LIMIT ?, ?;
	`;

	return await pool.query(sql, [userId, skipElements, elementsPerPage]);
};

const getPublicWithLimit = async (
	userId,
	isPrivate,
	skipElements,
	elementsPerPage
) => {
	const sql = `
		SELECT c.id, c.title, c.description, c.last_modified, c.created_on, u.first_name, u.last_name,
			(SELECT COUNT(s.course_id) FROM sections AS s WHERE s.course_id = c.id AND s.is_deleted = 0) AS sections_count 
			${userId ? isStudentEnrolledWhereClause(userId) : ''}
		FROM courses c
		JOIN users u ON u.id = c.creator_id
		WHERE ${courseTypeWhereClause(isPrivate)} c.is_deleted = 0
		ORDER BY last_modified DESC
		LIMIT ?, ?;
	`;

	return await pool.query(sql, [skipElements, elementsPerPage]);
};

const getAllPublic = async () => {
	const sql = `
  SELECT *
  FROM courses
  WHERE is_private = 0 AND is_deleted = 0
  `;

	return await pool.query(sql);
};

const getBy = async (column, value) => {
	const sql = `
  SELECT id, title, description, is_private, created_on, last_modified, image, creator_id
  FROM courses
  WHERE ${column} = ? AND is_deleted = 0
  `;

	const result = await pool.query(sql, value);

	return result[0];
};

const getBySearchWordWithLimit = async (
	searchInAll,
	isPrivate,
	skipElements,
	elementsPerPage,
	searchWord,
	column,
	userId
) => {
	const sql = `
	SELECT c.id, c.title, c.description, c.last_modified, c.created_on, u.first_name, u.last_name,
		(SELECT COUNT(s.course_id) FROM sections AS s WHERE s.course_id = c.id AND s.is_deleted = 0) AS sections_count,
		(SELECT COUNT(*) FROM enrolled_users AS eu WHERE c.id = eu.course_id AND eu.user_id = ? ) AS is_user_enrolled
	FROM courses c
	JOIN users u ON u.id = c.creator_id
	WHERE ${
		searchInAll ? '' : courseTypeWhereClause(isPrivate)
	} c.is_deleted = 0 AND ${column} LIKE '%${searchWord}%'
	ORDER BY last_modified DESC
	LIMIT ?, ?;
`;

	return await pool.query(sql, [userId, skipElements, elementsPerPage]);
};

const getPublicBySearchWordWithLimit = async (
	userId,
	isPrivate,
	skipElements,
	elementsPerPage,
	searchWord,
	column
) => {
	const sql = `
	SELECT c.id, c.title, c.description, c.last_modified, c.created_on, u.first_name, u.last_name,
		(SELECT COUNT(s.course_id) FROM sections AS s WHERE s.course_id = c.id AND s.is_deleted = 0) AS sections_count
		${userId ? isStudentEnrolledWhereClause(userId) : ''}
	FROM courses c
	JOIN users u ON u.id = c.creator_id
	WHERE ${courseTypeWhereClause(
		isPrivate
	)} c.is_deleted = 0 AND ${column} LIKE '%${searchWord}%'
	ORDER BY last_modified DESC
	LIMIT ?, ?;
`;

	return await pool.query(sql, [skipElements, elementsPerPage]);
};

const getCountBySearchWord = async (
	searchInAll,
	isPrivate,
	column,
	searchWord
) => {
	const sql = `
		SELECT COUNT(*)
		FROM courses c
		WHERE ${
			searchInAll ? '' : courseTypeWhereClause(isPrivate)
		} c.is_deleted = 0 AND ${column} LIKE '%${searchWord}%'
	`;

	const _ = await pool.query(sql, [isPrivate]);
	const coursesCount = _[0]['COUNT(*)'];
	return coursesCount;
};

const getEnrolledByUserId = async (
	id,
	skipElements,
	elementsPerPage,
	searchWord
) => {
	const sql = `
	SELECT c.id, c.title, c.description, c.last_modified, c.created_on, u.first_name, u.last_name, u.email,
		(SELECT COUNT(s.course_id) FROM sections AS s WHERE s.course_id = c.id AND s.is_deleted = 0) AS sections_count
	FROM courses c
	JOIN users u ON u.id = c.creator_id
	JOIN enrolled_users eu ON c.id = eu.course_id
	WHERE eu.user_id = ? AND c.is_deleted = 0 ${
		searchWord ? addSearchWhereClause(searchWord) : ''
	}
	ORDER BY last_modified DESC
	LIMIT ?, ?;
	`;

	return await pool.query(sql, [id, skipElements, elementsPerPage]);
};

const add = async (body) => {
	const sql = `
  INSERT INTO courses (id, title, description, is_private, creator_id , created_on, image)
  VALUES (?, ?, ?, ?, ?, NOW(), ?)
  `;

	await pool.query(sql, [
		body.id,
		body.title,
		body.description,
		body.isPrivate,
		body.creatorId,
		body.imageName,
	]);

	return await getBy('id', body.id);
};

const updateCourse = async (id, data) => {
	const { title, description, isPrivate, imageName } = data;

	const sql = `
  UPDATE courses
  SET title = ?, description = ?, is_private = ?, last_modified = NOW(), image = ?
  WHERE id = ?
  `;

	await pool.query(sql, [title, description, isPrivate, imageName, id]);

	return await getBy('id', id);
};

const remove = async (id) => {
	const sql = `
  UPDATE courses
  SET is_deleted = 1
  WHERE id = ?
  `;

	return await pool.query(sql, id);
};

const enrollStudentInCourse = async (courseId, userId) => {
	const sql = `
	INSERT INTO enrolled_users (course_id, user_id)
	VALUES (? , ?);
	`;

	await pool.query(sql, [courseId, userId]);

	const sqlToReturn = `
	SELECT * 
	FROM enrolled_users
	WHERE course_id = ? AND user_id = ?;
	`;

	return await pool.query(sqlToReturn, [courseId, userId]);
};

const enrollMultipleStudentInCourse = async (courseId, newStudents) => {
	const sql = `
	INSERT IGNORE INTO enrolled_users (course_id, user_id)
	VALUES ${valuesForMultipleInsertions(courseId, newStudents)}`;

	await pool.query(sql);
};

const unenrollSingleStudentInCourse = async (courseId, userId) => {
	const sql = `
	DELETE
	FROM enrolled_users
	WHERE course_id = ? ${userId ? ' AND user_id = ?' : ''};
	`;

	await pool.query(sql, [courseId, userId]);
};

const unenrollAllStudentsByCourseId = async (courseId) => {
	const sql = `
	DELETE
	FROM enrolled_users
	WHERE course_id = ?;`;

	await pool.query(sql, [courseId]);
};

const getEnrolledUsers = async (courseId) => {
	const sql = `
	SELECT eu.user_id as id, eu.course_id, u.email, u.first_name, u.last_name
	FROM enrolled_users eu
	JOIN users AS u
	ON eu.user_id = u.id
	AND eu.course_id = ?
	AND u.is_teacher = 0;
	`;

	return await pool.query(sql, [courseId]);
};

const getUnenrolledByCourseId = async (courseId) => {
	const sql = `
	SELECT u.id, u.first_name, u.last_name, u.email
	FROM users u 
	WHERE u.id NOT IN 
		(SELECT user_id FROM enrolled_users WHERE course_id = ?);
	`;

	return await pool.query(sql, [courseId]);
};

const getBulletsByCourseId = async (courseId) => {
	const sql = `
	SELECT id, bullet 
	FROM course_summary_bullets
	WHERE course_id = ?
	`;

	return await pool.query(sql, [courseId]);
};

const getSingleBulletById = async (bulletId) => {
	const sql = `
	SELECT *
	FROM course_summary_bullets
	WHERE id = ?
	`;

	const result = await pool.query(sql, [bulletId]);
	return result[0];
};

const addBullets = async (courseId, bullets) => {
	const values = bullets.map(
		(bullet) => `('${uuid()}', '${bullet}', '${courseId}')`
	);

	const sql = `
	INSERT INTO course_summary_bullets (id, bullet, course_id)
	VALUES ${values}
	`;

	await pool.query(sql);

	return getBulletsByCourseId(courseId);
};

const updateBullet = async (bulletObj) => {
	const sql = `
	UPDATE course_summary_bullets
  SET bullet = ?
  WHERE id = ?`;

	await pool.query(sql, [bulletObj.bullet, bulletObj.id]);

	return await getSingleBulletById(bulletObj.id);
};

const removeBulletsByCourseId = async (courseId) => {
	const sql = `
	DELETE
	FROM course_summary_bullets
	WHERE course_id = ?;
	`;

	await pool.query(sql, [courseId]);
};

/* HELPER FUNCTIONS */
const courseTypeWhereClause = (isPrivate) => {
	const sql = `c.is_private = ${isPrivate} AND `;

	return sql;
};

const isStudentEnrolledWhereClause = (id) => {
	const sql = `
	,( SELECT COUNT(*) 
		FROM enrolled_users AS eu WHERE c.id = eu.course_id AND eu.user_id = '${id}' ) 
		AS is_student_enrolled `;

	return sql;
};

const addSearchWhereClause = (searchWord) => {
	const sql = `AND c.title LIKE '%${searchWord}%'`;

	return sql;
};

const valuesForMultipleInsertions = (value1, arrOfValues) => {
	const sql = arrOfValues.map((x) => `('${value1}', '${x.id}')`);

	return sql.join(',');
};

export default {
	getOverview,
	getAll,
	getAllTitles,
	getAllByCreatorId,
	getAllCount,
	getCountByCreatorId,
	getCountByCourseStatus,
	getCountOfEnrolledCoursesByUserId,
	getCountOfEnrolledCoursesByUserIdWithSearch,
	getWithLimit,
	getAllPublic,
	getPublicWithLimit,
	getBySearchWordWithLimit,
	getPublicBySearchWordWithLimit,
	getCountBySearchWord,
	getBy,
	getEnrolledByUserId,
	add,
	updateCourse,
	remove,
	enrollStudentInCourse,
	enrollMultipleStudentInCourse,
	unenrollSingleStudentInCourse,
	unenrollAllStudentsByCourseId,
	getEnrolledUsers,
	getUnenrolledByCourseId,
	getBulletsByCourseId,
	getSingleBulletById,
	addBullets,
	updateBullet,
	removeBulletsByCourseId,
};
