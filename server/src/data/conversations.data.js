import pool from './db-pool.js';

const insertMembers = async (members) => {
  const sql = `
    INSERT INTO conversation_users (conversation_id, user_id)
    VALUES ${members};
  `;

  return await pool.query(sql);
};

const create = async (id, creatorId) => {
  const sql = `
    INSERT INTO conversations (id, admin_id)
    VALUES (?, ?)
  `;

  await pool.query(sql, [id, creatorId]);
};

const getBy = async (column, value) => {
  const sql = `
    SELECT *
    FROM conversation_users
    WHERE ${column} = ? AND is_deleted = 0
  `;

  return await pool.query(sql, value);
};

const getOther = async (column, value, userId) => {
  const sql = `
    SELECT *
    FROM conversation_users
    WHERE ${column} = ? AND is_deleted = 0 AND user_id != ?
  `;

  return await pool.query(sql, [value, userId]);
};

const check = async (userOne, userTwo) => {
  const sql = `
  SELECT cu.conversation_id
  FROM conversation_users AS cu
  JOIN conversation_users AS cu1 ON cu1.conversation_id = cu.conversation_id
    AND cu1.user_id = '${userOne}'
  LEFT JOIN conversation_users cu2 ON cu2.conversation_id = cu.conversation_id
    AND cu2.user_id NOT IN ('${userOne}', '${userTwo}')
  WHERE cu.user_id = '${userTwo}'
  AND cu2.user_id is null;
  `;

  const query = await pool.query(sql);

  return query[0];
};

export default {
  insertMembers,
  create,
  getBy,
  getOther,
  check
};