import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
	addModal: {
		position: 'absolute',
		width: 900,
		height: 650,
		backgroundColor: 'white',
		borderRadius: '20px',
		boxShadow: '5px',
		outline: 0,
		left: '30%',
		top: '13%',
	},
	addButton: {
		backgroundColor: 'green',
		'&:hover': {
			backgroundColor: 'green',
		},
	},
	cancelButton: {
		backgroundColor: 'red',
		'&:hover': {
			backgroundColor: 'red',
		},
		marginRight: '5px',
	},
}));
