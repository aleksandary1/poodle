/* eslint-disable no-useless-catch */
import mariadb from 'mariadb';
import { DB_HOST, DB_PASS, DB_PORT, DB_USER } from '../common/config.js';
import usersService from '../services/users.service.js';
import { addCourse } from '../services/courses.service.js';
import coursesMockData from './mock_data/courses.mock.data.js';
import studentsMockData from './mock_data/students.mock.data.js';
import teachersMockData from './mock_data/teachers.mock.data.js';
import sectionsMockData from './mock_data/sections.mock.data.js';
import { getRandomValue } from './mock_data/selectRandom.sql.js';
import { addSection } from '../services/sections.service.js';
import databaseMockData from './mock_data/database.mock.data.js';
import coursesData from './courses.data.js';
import usersData from './users.data.js';

const pool = mariadb.createPool({
	host: DB_HOST,
	user: DB_USER,
	password: DB_PASS,
	port: DB_PORT,
	connectionLimit: 5,
});

const students = studentsMockData;
const teachers = teachersMockData;
const courses = coursesMockData;
const sections = sectionsMockData;

// insert mock students
const seedStudents = async () => {
	for (const student of students) {
		await usersService.create(student);
	}

	console.log('Default students created!');
};

// insert mock students
const seedTeachers = async () => {
	for (const teacher of teachers) {
		await usersService.insertTeacher(teacher);
	}

	console.log('Default teachers created!');
};

// insert mock courses
const seedCourses = async () => {
	for (const course of courses) {
		const creatorId = await getRandomValue(
			'id',
			'users',
			'WHERE is_teacher = 1'
		);
		course.creatorId = creatorId.id;

		await addCourse(course);
	}

	console.log('Default courses created!');
};

// insert mock sections
const seedSections = async () => {
	for (const section of sections) {
		const courseId = await getRandomValue('id', 'courses');

		await addSection(section, courseId.id);
	}

	console.log('Default sections created!');
};

// enroll students in course
const enrollStudentsInCourse = async () => {
	const allUsers = await usersData.getAll();

	allUsers.map(async (user) => {
		const courseId = await getRandomValue('id', 'courses');
		await coursesData.enrollStudentInCourse(courseId.id, user.id);
	});

	console.log('Students enrolled in courses!');
};

(async () => {
	try {
		await pool.query(databaseMockData.createPoodleDatabase);
		await pool.query(databaseMockData.createUsersTable);
		await pool.query(databaseMockData.createConversationsTable);
		await pool.query(databaseMockData.createConversationUsersTable);
		await pool.query(databaseMockData.createCoursesTable);
		await pool.query(databaseMockData.createCourseSummaryBullets);
		await pool.query(databaseMockData.createEnrolledUsersTable);
		await pool.query(databaseMockData.createMessagesTable);
		await pool.query(databaseMockData.createQuizesTable);
		await pool.query(databaseMockData.createQuizesQuestionsTable);
		await pool.query(databaseMockData.createQuizesAnswersTable);
		await pool.query(databaseMockData.createQuizesUsersTable);
		await pool.query(databaseMockData.createSectionsTable);
		await pool.query(databaseMockData.createRestrictedSectionsUsersTable);

		console.log('Database was created!');

		// await seedStudents();
		// await seedTeachers();
		// await seedCourses();
		// await seedSections();
		// await enrollStudentsInCourse();

		console.log('Done!');
	} catch (error) {
		console.log(`Ooops! There was an error: ${error}`);
	}
})();
