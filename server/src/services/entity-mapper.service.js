export const normalizeEntityProperties = (obj, renameObj = {}) => {
	if (!obj) {
		return null;
	}

	const normalizedObj = {};

	for (const key in obj) {
		let modifiedKey = '';

		if (renameObj[key]) {
			modifiedKey = renameObj[key];
		} else if (key.includes('_')) {
			modifiedKey = getCamelCaseString(key);
		} else {
			modifiedKey = key;
		}
		normalizedObj[modifiedKey] = obj[key];
	}

	return normalizedObj;
};

const getCamelCaseString = (str) => {
	const parts = str.split('_');
	const firstWord = parts[0];

	return parts
		.slice(1)
		.reduce(
			(acc, word) => acc + word[0].toUpperCase() + word.slice(1),
			firstWord
		);
};
