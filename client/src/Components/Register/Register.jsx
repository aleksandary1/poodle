import { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useStyles } from './register.styles';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import usersRequests from '../../Requests/users.requests';
import RegisterModal from './RegisterModal';

const Register = () => {
	const [newUser, setNewUser] = useState({
		email: '',
		password: '',
		firstName: '',
		lastName: '',
	});
	const [invalidEmail, setInvalidEmail] = useState('');
	const [invalidPassword, setInvalidPassword] = useState('');
	const [invalidNames, setInvalidNames] = useState('');
	const [successfulRegister, setSuccessfulRegister] = useState(false);

	const history = useHistory();

	const goToLogin = () => {
		history.push('/login');
	};

	const onUpdate = (prop, value) => {
		newUser[prop] = value;
		setNewUser({ ...newUser });
	};

	const onSubmit = (e) => {
		e.preventDefault();
		setInvalidEmail(false);
		setInvalidPassword(false);
		setInvalidNames(false);

		if (!newUser.email) {
			return setInvalidEmail('Provide email');
		}

		const emailRegex = new RegExp(
			'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$'
		);

		if (!emailRegex.test(newUser.email)) {
			return setInvalidEmail('Invalid email');
		}

		if (!newUser.firstName || !newUser.lastName) {
			return setInvalidNames(`Provide First and Last Name`);
		}

		if (newUser.firstName.length < 3) {
			return setInvalidNames('First name should be at least 3 characters long');
		}

		if (newUser.lastName.length < 3) {
			return setInvalidNames('Last name should be at least 3 characters long');
		}

		if (
			newUser.password.trim().length < 6 ||
			newUser.password.trim().length > 20 ||
			!newUser.password.match(/[A-Z]/) ||
			!newUser.password.match(/[0-9]/)
		) {
			return setInvalidPassword(
				'Password should be between 6 and 20 characters, should contain at least 1 uppercase character and 1 number'
			);
		}

		if (
			!invalidEmail &&
			!invalidPassword &&
			newUser.firstName &&
			newUser.lastName
		) {
			usersRequests.register(newUser).then((data) => {
				if (data.error) {
					return setInvalidEmail(data.error);
				}
			});

			setSuccessfulRegister(true);
		}
	};

	const classes = useStyles();

	return (
		<>
			<Grid container classes={{root: classes.root}} style={{width: '60%'}}>
				<CssBaseline />
				<Grid item xs={false} sm={4} md={7} className={classes.image}>
					<div className={classes.textBox}>
						<h3>
							Poodle is an online learning platform with many classes for
							curious people. On Poodle, thousands of members come together to
							find inspiration and take the next step in their professional
							journey.
						</h3>
					</div>
				</Grid>
				<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
					<div className={classes.paper}>
						<Typography component="h1" variant="h5">
							Sign Up
						</Typography>
						<form className={classes.form} onSubmit={onSubmit}>
							<Grid container spacing={2}>
								<TextField
									className={classes.textfield}
									variant="outlined"
									margin="normal"
									fullWidth
									id="email"
									label="Email Address"
									name="email"
									autoComplete="email"
									autoFocus
									onChange={(e) => onUpdate('email', e.target.value)}
								/>
								{invalidEmail && (
									<p style={{ color: 'red', margin: '0px' }}>{invalidEmail}</p>
								)}
								<TextField
									className={classes.textfield}
									variant="outlined"
									margin="normal"
									fullWidth
									name="password"
									label="Password"
									type="password"
									id="password"
									autoComplete="current-password"
									onChange={(e) => onUpdate('password', e.target.value)}
								/>
								{invalidPassword && (
									<p style={{ color: 'red', margin: '0px auto' }}>
										{invalidPassword}
									</p>
								)}
								<Grid
									item
									container
									sm={12}
									justifyContent="space-between"
									style={{ padding: 0 }}
								>
									<Grid item xs={6} sm={6}>
										<TextField
											style={{ paddingRight: '6px' }}
											variant="outlined"
											margin="normal"
											fullWidth
											label="First Name"
											onChange={(e) => onUpdate('firstName', e.target.value)}
										/>
									</Grid>
									<Grid item xs={6} sm={6}>
										<TextField
											style={{ paddingLeft: '6px' }}
											variant="outlined"
											fullWidth
											margin="normal"
											label="Last Name"
											onChange={(e) => onUpdate('lastName', e.target.value)}
										/>
									</Grid>
								</Grid>
								{invalidNames && (
									<p style={{ color: 'red', margin: '0px' }}>{invalidNames}</p>
								)}

								<Button
									type="submit"
									fullWidth
									variant="contained"
									color="primary"
									className={classes.submit}
								>
									Sign Up
								</Button>
								<Grid container>
									<Grid item>
										<Link to={'/login'} className={classes.link}>
											{'Already have an account? Sign In'}
										</Link>
									</Grid>
								</Grid>
							</Grid>
						</form>
					</div>
				</Grid>
			</Grid>
			{successfulRegister ? (
				<RegisterModal
					newUser={newUser}
					isOpenModal={true}
					goToLogin={goToLogin}
				/>
			) : null}
		</>
	);
};

export default Register;
