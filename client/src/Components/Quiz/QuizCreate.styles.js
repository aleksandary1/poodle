import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => ({
	header: {
		height: '100px',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		margin: '30px 0',
	},
	quizSummaryHeader: {
		display: 'flex',
		alignItems: 'center',
		fontSize: '20px',
	},
	accordionWrapper: {
		width: '100%',
		display: 'flex',
	},
	accordion: { width: '93%', marginTop: '10px', alignItems: 'center' },
	accordionQuestion: {
		fontSize: '20px',
		marginTop: '2px',
		display: 'inline-block',
	},
	accordionPoints: {
		fontStyle: 'italic',
		fontSize: '15px',
		marginTop: '2px',
		marginLeft: '10px',
		color: 'grey',
		display: 'inline-block',
	},
	whiteButton: {
		width: '200px',
		height: '50px',
		display: 'flex',
		float: 'right',
		fontSize: '22px',
		textTransform: 'none',
		background: '#fff',
		letterSpacing: 1,
		color: '#7e57c2',
		borderRadius: '8px',
		boxShadow: '0px 8px 12px -3px rgba(0,0,0,0.2)',
		'&:hover': {
			background: '#9575cd',
			color: '#fff',
		},
	},
	purpleBtn: {
		width: '200px',
		height: '100%',
		display: 'flex',
		float: 'right',
		marginTop: '15px',
		fontSize: '22px',
		letterSpacing: 1,
		textTransform: 'none',
		background: '#9575cd',
		color: '#fff',
		borderRadius: '10px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#7e57c2',
			color: '#fff',
		},
	},
}));
