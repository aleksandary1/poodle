import { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import AuthContext from '../../providers/auth.provider';
import {
	Box,
	Divider,
	Drawer,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
} from '@material-ui/core';
import Logo from '../../Media/SidebarLogo.png';
import options from './SidebarOptions';
import SidebarProfile from './SidebarProfile';
import { useStyles } from './sidebar.styles.js';

const Sidebar = ({ page }) => {
	const classes = useStyles();
	const history = useHistory();

	const { user } = useContext(AuthContext);

	let sidebarOptions = options;

	if (!user) {
		sidebarOptions = options.filter((item) => item.name !== 'My Courses');
	}

	const loggedUser = (
		<>
			<Box>
				<SidebarProfile />
			</Box>
			<Divider variant="middle" />
		</>
	);

	const notLoggedUser = (
		<>
			<List className={classes.notLogged}>
				<Link to={'/login'} style={{ textDecoration: 'none', color: 'black' }}>
					<ListItem button>
						<ListItemText primary="Login" style={{ marginLeft: 40 }} />
					</ListItem>
				</Link>

				<Link
					to={'register'}
					style={{ textDecoration: 'none', color: 'black' }}
				>
					<ListItem button>
						<ListItemText primary="Register" style={{ marginLeft: 35 }} />
					</ListItem>
				</Link>
			</List>
			<Divider variant="middle" />
		</>
	);

	return (
		<Box>
			<Drawer
				variant="permanent"
				classes={{
					paper: classes.drawerPaper,
				}}
				anchor="left"
			>
				<Link to={''} style={{ textDecoration: 'none', color: 'black' }}>
					<Box className={classes.logo}>
						<img src={Logo} alt="logo" width="130px" height="140px" />
					</Box>
				</Link>
				<Divider variant="middle" />
				{user ? loggedUser : notLoggedUser}
				{sidebarOptions.map((item) => (
					<ListItem
						button
						classes={{ selected: classes.selectedPage }}
						onClick={() => history.push(item.link)}
						selected={page === item.page}
						key={item.name}
					>
						<ListItemIcon className={classes.listIcon}>
							{item.icon}
						</ListItemIcon>
						<ListItemText primary={item.name} />
					</ListItem>
				))}
			</Drawer>
		</Box>
	);
};

export default Sidebar;
