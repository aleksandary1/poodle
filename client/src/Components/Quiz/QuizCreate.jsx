import { useState } from 'react';
import { useStyles } from './QuizCreate.styles';
import MainView from '../../Views/MainView';
import CreateEditQuestionModal from './CreateEditQuestionModal';
import {
	Box,
	Button,
	Accordion,
	AccordionSummary,
	AccordionDetails,
	Typography,
	IconButton,
} from '@material-ui/core';
import { ExpandMore, Check, Clear, Edit, Delete } from '@material-ui/icons';
import coursesRequests from '../../Requests/courses.requests';
import { useHistory } from 'react-router-dom';

const QuizCreate = (computedMatch) => {
	const { courseId } = computedMatch.computedMatch.params;

	const [questions, setQuestions] = useState([]);
	const [totalQuestionsCount, setTotalQuestionsCount] = useState(0);
	const [totalPoints, setTotalPoints] = useState(0);
	const [isOpenModal, setIsOpenModal] = useState(false);
	const [questionToEdit, setQuestionToEdit] = useState(null);
	const [counterId, setCounterId] = useState(questions.length);

	const history = useHistory();

	const toggleModal = (indexToEdit) => {
		setIsOpenModal(!isOpenModal);

		const questionToEditExists = questions.filter(
			(_, index) => index === indexToEdit
		);

		if (questionToEditExists.length) {
			setQuestionToEdit(questionToEditExists[0]);
		} else {
			setQuestionToEdit(null);
		}
	};

	const addQuestion = (newQuestion) => {
		setQuestions([...questions, newQuestion]);
		setTotalQuestionsCount(questions.length + 1);
		setTotalPoints(totalPoints + newQuestion.question.points);
		setCounterId(counterId + 1);
		setQuestionToEdit(null);
		toggleModal();
	};

	const updateQuestions = (newQuestion) => {
		const indexToUpdate = questions.findIndex(
			(x) => x.question.id === newQuestion.question.id
		);

		questions[indexToUpdate] = newQuestion;
		setQuestions([...questions]);
		setQuestionToEdit(null);
		toggleModal();
	};

	const removeQuestion = (questionToRemove) => {
		const filteredQuestions = questions.filter(
			(_, index) => index !== questionToRemove.question.id
		);

		setQuestions(filteredQuestions);
		setTotalPoints(totalPoints - questionToRemove.question.points);
		setTotalQuestionsCount(totalQuestionsCount - 1);
	};

	const createNewQuiz = (quiz) => {
		coursesRequests.createQuiz(courseId, quiz);
		history.push(`/courses/${courseId}/sections`);
	};

	const classes = useStyles();

	return (
		<MainView page="createQuiz">
			<Box className={classes.header}>
				<h1>QUIZ</h1>

				<Box className={classes.quizSummaryHeader}>
					<p style={{ marginRight: '20px' }}>
						Questions Count: {totalQuestionsCount}
					</p>
					<p>Max Points: {totalPoints}</p>
				</Box>

				<Button className={classes.whiteButton} onClick={toggleModal}>
					Add Question
				</Button>
			</Box>

			{isOpenModal ? (
				<CreateEditQuestionModal
					id={counterId}
					questionToEdit={questionToEdit}
					updateQuestionInQuizView={updateQuestions}
					isOpenModal={isOpenModal}
					toggleModal={toggleModal}
					questionNumber={questions.length + 1}
					addQuestion={addQuestion}
				/>
			) : null}

			{questions.length ? (
				<>
					{' '}
					{questions.map((q, index) => (
						<Box className={classes.accordionWrapper}>
							<Accordion key={index} className={classes.accordion}>
								<AccordionSummary
									expandIcon={<ExpandMore />}
									aria-controls="panel1a-content"
									id="panel1a-header"
								>
									<Typography className={classes.accordionQuestion}>
										{q.question.content}
										<Typography className={classes.accordionPoints}>
											( {q.question.points} points )
										</Typography>
									</Typography>
								</AccordionSummary>
								{q.answers.map((ans, index) => (
									<AccordionDetails>
										{ans.isCorrect ? (
											<Check style={{ color: 'grey' }} />
										) : (
											<Clear style={{ color: 'grey' }} />
										)}
										<Typography key={index} style={{ marginLeft: '10px' }}>
											{ans.answer}
										</Typography>
									</AccordionDetails>
								))}
							</Accordion>
							<Box
								style={{
									display: 'flex',
									width: '5%',
									marginTop: '10px',
								}}
							>
								<IconButton onClick={() => toggleModal(index)}>
									<Edit />
								</IconButton>
								<IconButton onClick={() => removeQuestion(q)}>
									<Delete />
								</IconButton>
							</Box>
						</Box>
					))}{' '}
				</>
			) : (
				<p>No Questions Yet</p>
			)}

			{questions.length ? (
				<Button
					className={classes.purpleBtn}
					onClick={() => createNewQuiz({ id: '', questions, totalPoints })}
				>
					Create QUIZ
				</Button>
			) : null}
		</MainView>
	);
};

export default QuizCreate;
