import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
  root: {
    marginBottom: '20px'
  },
  card: {
    width: '900px',
  },
  name: {
    fontSize: '20pt',
    margin: '10px 0 10px 10px'
  },
  showButton: {
    height: '50px',
    marginTop: '5px',
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: '5px 10px'
  },
  addButton: {
    backgroundColor: 'green',
    '&:hover': {
      backgroundColor: 'green'
    }
  },
  cancelButton: {
    backgroundColor: 'red',
    '&:hover': {
      backgroundColor: 'red'
    },
    marginRight: '5px'
  },
  editTitle: {
    height: '15px',
    width: '500px'
  }
}));