import { Box, Collapse, makeStyles } from '@material-ui/core'
import { ArrowDropDown, ArrowDropUp } from '@material-ui/icons';
import React, { useContext } from 'react'
import { useState } from 'react';
import AuthContext from '../../providers/auth.provider';
import ProfileOptions from './ProfileOptions';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    height: 70
  },
  nameRole: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '5px',
    fontSize: '10pt'
  },
  role: {
    alignSelf: 'flex-start'
  },
  name: {
    marginBottom: 0,
  }
}));

const SidebarProfile = () => {
  const classes = useStyles();
  const [showMenu, setShowMenu] = useState(false);
  const { user } = useContext(AuthContext);
  return (
    <Box>
    <Box className={classes.root} onClick={() => setShowMenu(!showMenu)}>
      <Box className={classes.nameRole}>
        <Box style={{display: 'flex'}}>
          <p className={classes.name}>{`${user.firstName} ${user.lastName}`}</p>
          {showMenu ? <ArrowDropUp style={{marginTop: '10px'}}/> : <ArrowDropDown style={{marginTop: '10px'}}/>}
        </Box>
        <Box className={classes.role}>{user.isTeacher ? 'Teacher' : 'Student'}</Box>
      </Box>
    </Box>
    <Collapse in={showMenu} timeout='auto' unmountOnExit>
      <ProfileOptions />
    </Collapse>
    </Box>
  )
}

export default SidebarProfile
