import { Link } from 'react-router-dom';
import { useStyles } from './registerModal.styles';
import { Modal, Backdrop, Fade } from '@material-ui/core';

const RegisterModal = ({ newUser, isOpenModal, goToLogin }) => {
	const { firstName } = newUser;

	const classes = useStyles();
	return (
		<Modal
			open={isOpenModal}
			aria-labelledby="transition-modal-title"
			aria-describedby="transition-modal-description"
			className={classes.modal}
			closeAfterTransition
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 500,
			}}
		>
			<Fade in={isOpenModal}>
				<div className={classes.paper}>
					<h2
						id="transition-modal-title"
						style={{
							textAlign: 'center',
						}}
					>
						Welcome to Poodle, {firstName}!
					</h2>
					<p
						id="transition-modal-description"
						style={{
							textAlign: 'center',
						}}
					>
						To continue, please&nbsp;
						<Link to={'/login'} className={classes.link}>
							login
						</Link>
					</p>
				</div>
			</Fade>
		</Modal>
	);
};

export default RegisterModal;
