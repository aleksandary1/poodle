/* eslint-disable import/no-anonymous-default-export */
import { BASE_URL } from '../common/constants';
import { createRequest } from './requests.helper';

const getOverview = () => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/courses/overview`, request).then((res) =>
		res.json()
	);
};

const getAll = (
	page,
	elementsPerPage,
	courseFilter = 'All',
	searchWord = ''
) => {
	const request = createRequest('GET');

	if (searchWord) {
		return fetch(
			`${BASE_URL}/courses?courseFilter=${courseFilter}&page=${page}&elementsPerPage=${elementsPerPage}&title=${searchWord}`,
			request
		).then((res) => res.json());
	}

	return fetch(
		`${BASE_URL}/courses?courseFilter=${courseFilter}&page=${page}&elementsPerPage=${elementsPerPage}`,
		request
	).then((res) => res.json());
};

const getAllTitles = () => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/courses/titles`, request).then((res) => res.json());
};

const getPublic = (page, elementsPerPage, searchWord = '', userId = '') => {
	const request = createRequest('GET');

	if (searchWord) {
		return fetch(
			`${BASE_URL}/courses/public?page=${page}&elementsPerPage=${elementsPerPage}&title=${searchWord}&userId=${userId}`,
			request
		).then((res) => res.json());
	}

	return fetch(
		`${BASE_URL}/courses/public?page=${page}&elementsPerPage=${elementsPerPage}&userId=${userId}`,
		request
	).then((res) => res.json());
};

const getMyCourses = (
	userId,
	page,
	elementsPerPage,
	courseFilter = 'All',
	searchWord = ''
) => {
	const request = createRequest('GET');

	return fetch(
		`${BASE_URL}/courses/myCourses?courseFilter=${courseFilter}&page=${page}&elementsPerPage=${elementsPerPage}&userId=${userId}&title=${searchWord}`,
		request
	).then((res) => res.json());
};

const getWithLimit = (limit) => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/courses?limit=${limit}`, request).then((res) =>
		res.json()
	);
};

const getPublicWithLimit = (limit) => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/courses/public?limit=${limit}`, request).then(
		(res) => res.json()
	);
};

const getById = (id) => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/courses/${id}`, request).then((res) => res.json());
};

const enrollStudent = (courseId, userId) => {
	const request = createRequest('GET');

	return fetch(
		`${BASE_URL}/courses/${courseId}/enroll?userId=${userId}`,
		request
	).then((res) => res.json());
};

const unenrollStudent = (courseId, userId) => {
	const request = createRequest('GET');

	return fetch(
		`${BASE_URL}/courses/${courseId}/unenroll?userId=${userId}`,
		request
	).then((res) => res.json());
};

const getEnrolled = (courseId) => {
	const request = createRequest('GET');

	return fetch(`${BASE_URL}/courses/${courseId}/enrolled`, request).then(
		(res) => res.json()
	);
};

const create = (course) => {
	const formData = new FormData();
	formData.append('courseImage', course.courseImage);
	formData.append('course', JSON.stringify(course.course));

	const headers = {};
	headers['Authorization'] = `Bearer ${localStorage.getItem('token')}`;

	const request = { method: 'POST', body: formData, headers };

	return fetch(`${BASE_URL}/courses`, request).then((res) => res.json());
};

const update = (course) => {
	const formData = new FormData();
	formData.append('courseImage', course.courseImage);
	formData.append('course', JSON.stringify(course.course));

	const headers = {};
	headers['Authorization'] = `Bearer ${localStorage.getItem('token')}`;

	const request = { method: 'PUT', body: formData, headers };

	return fetch(`${BASE_URL}/courses/${course.course.id}`, request).then((res) =>
		res.json()
	);
};

const uploadPicture = (file) => {
	const formData = new FormData();
	formData.append('courseImage', file.file);

	const request = createRequest('POST', { body: formData });

	return fetch(
		`${BASE_URL}/courses/uploadPicture/${file.courseId}`,
		request
	).then((res) => res.json());
};

const createQuiz = (courseId, quiz) => {
	const request = createRequest('POST', { body: JSON.stringify(quiz) });

	return fetch(`${BASE_URL}/courses/${courseId}/createQuiz`, request).then(
		(res) => res.json()
	);
};

const uploadUserPointsForQuiz = (quizResult, courseId) => {
	console.log(quizResult);
	const request = createRequest('POST', { body: JSON.stringify(quizResult) });

	return fetch(`${BASE_URL}/courses/${courseId}/takeQuiz`, request).then(
		(res) => res.json()
	);
};

export default {
	getOverview,
	getAll,
	getAllTitles,
	getPublic,
	getMyCourses,
	getWithLimit,
	getPublicWithLimit,
	getById,
	enrollStudent,
	unenrollStudent,
	getEnrolled,
	create,
	update,
	uploadPicture,
	createQuiz,
	uploadUserPointsForQuiz,
};
