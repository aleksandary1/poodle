import { useEffect, useState } from 'react';
import ProtectedRoute from './Components/ProtectedRoute';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import getUserFromToken from './utils/auth';
import AuthContext from './providers/auth.provider';
import Register from './Components/Register/Register';
import Login from './Components/Login/Login';
import Home from './Views/Home';
import CoursesList from './Views/Courses';
import DetailedCourse from './Views/DetailedCourse';
import Chat from './Views/Chat/Chat';
import { Box } from '@material-ui/core';
import MyCourses from './Views/MyCourses';
import SocketProvider from './providers/socket.provider';
import QuizCreate from './Components/Quiz/QuizCreate';
import TakeQuiz from './Components/Quiz/TakeQuiz';

function App() {
	const [user, setUser] = useState(
		getUserFromToken(localStorage.getItem('token'))
	);

	useEffect(() => {
		if (localStorage.getItem('token')) {
			const time = user?.exp * 1000 - new Date().getTime();
			setTimeout(() => {
				localStorage.removeItem('token');
				setUser(null);
				<Redirect to="/login" />;
			}, time);
		}

		return () => clearTimeout();
	});

	return (
		<Box>
			<BrowserRouter>
				<AuthContext.Provider value={{ user, setUser }}>
					<SocketProvider>
						<Switch>
							<Route path="/" exact>
								<Home />
							</Route>

							<Route path="/courses" exact>
								<CoursesList />
							</Route>

							<Route path="/myCourses" exact>
								<MyCourses />
							</Route>

							<ProtectedRoute
								path="/courses/:courseId/createQuiz"
								exact
								Component={QuizCreate}
							/>

							<ProtectedRoute
								path="/courses/:courseId/sections"
								exact
								Component={DetailedCourse}
							/>

							<ProtectedRoute
								path="/courses/:courseId/quiz"
								exact
								Component={TakeQuiz}
							/>

							<ProtectedRoute path="/chat" exact Component={Chat} />

							<Route path="/login" exact>
								<Login />
							</Route>

							<Route path="/register" exact>
								<Register />
							</Route>

							<Route path="*" exact>
								<h2>404 NOT FOUND!</h2>
							</Route>
						</Switch>
					</SocketProvider>
				</AuthContext.Provider>
			</BrowserRouter>
		</Box>
	);
}

export default App;
