const getConfigPropertyNames = () => {
	return { first_name: 'authorFirstName', last_name: 'authorLastName' };
};

export default { getConfigPropertyNames };
