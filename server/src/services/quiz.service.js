import { v4 as uuid } from 'uuid';
import quizData from '../data/quiz.data.js';
import { normalizeEntityProperties } from './entity-mapper.service.js';

const getByCourseId = async (courseId) => {
	const quiz = normalizeEntityProperties(
		await quizData.getByCourseId(courseId)
	);

	if (!quiz) return null;

	const questions = await quizData.getQuestionsByQuizId(quiz.id);

	const composedQuestions = await composeQuestionWithAnswersById(questions);

	return { ...quiz, questions: composedQuestions };
};

const create = async (courseId, quiz) => {
	quiz.id = uuid();

	// create quiz
	await quizData.create(courseId, quiz.id, quiz.totalPoints);

	// add id to questions and answers
	setQuestionsIds(quiz.questions);

	// add questions
	await quizData.addQuestions(quiz.id, quiz.questions);

	// add answers per question
	await Promise.all(
		quiz.questions.map(
			async (q) => await quizData.addAnswers(q.question.id, q.answers)
		)
	);
};

const userQuizResult = async (quizResult) => {
	return await quizData.userQuizResult(quizResult);
};

/* HELPER FUNCTIONS */

const setQuestionsIds = (questions = []) => {
	questions.forEach((x) => {
		x.question = { ...x.question, id: uuid() };
		setQuestionAnswersId(x.answers);
	});
};

const setQuestionAnswersId = (answers = []) => {
	answers.forEach((x) => (x.answer = { id: uuid(), ...x }));
};

const composeQuestionWithAnswersById = async (questions) => {
	const composedQuestionsWithAnswers = await Promise.all(
		questions.map(async (question) => {
			const answersRaw = await quizData.getAnswersByQuestionId(question.id);
			const answers = await answersRaw.map((answer) =>
				normalizeEntityProperties(answer)
			);

			return { question, answers };
		})
	);

	return composedQuestionsWithAnswers;
};

export default { getByCourseId, create, userQuizResult };
