import { Box } from '@material-ui/core'
import React from 'react'
import { useStyles } from './message.styles'
import ProfilePic from '../../Media/ProfilePicture.jpg'
import { format } from "timeago.js"

const Message = ({ message, own }) => {
  const classes = useStyles();
  return (
    <Box className={own ? `${classes.message} ${classes.own}` : classes.message}>
      <Box className={classes.messageTop}>
        <img className={classes.image} src={ProfilePic} alt='profile'/>
        <p className={classes.messageText}>{message.message}</p>
      </Box>
      <Box className={classes.messageTime}>
        {format(message.createdOn)}
      </Box>
    </Box>
  )
}

export default Message
