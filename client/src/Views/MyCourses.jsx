import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import MainView from './MainView';
import { Box, Button, Grid, MenuItem } from '@material-ui/core';
import coursesRequests from '../Requests/courses.requests';
import CourseCard from '../Components/Course/CourseCard/CourseCard';
import AuthContext from '../providers/auth.provider';
import Dropdown from '../Components/Dropdown/Dropdown';
import Search from '../Components/Search/Search';
import InvalidSearch from '../Components/Search/InvalidSearch';
import CreateUpdateCourseModal from '../Components/Course/AddCourseModal/CreateUpdateCourseModal';
import { btnStyles } from '../style/button.styles';
import { useStyles } from './Courses.styles';

const MyCourses = () => {
	const { user } = useContext(AuthContext);

	const [allCourses, setAllCourses] = useState([]);
	const [fetchCourses, setFetchCourses] = useState(true);

	const [searchWord, setSearchWord] = useState('');
	const [searchedCoursesCount, setSearchedCoursesCount] = useState(0);
	const [invalidSearch, setInvalidSearch] = useState(false);

	const elementsPerPageArr = [8, 16, 24, 40];

	const [page, setPage] = useState(1);
	const [totalPages, setTotalPages] = useState(1);
	const [elementsPerPage, setElementsPerPage] = useState(elementsPerPageArr[0]);

	const coursesTypesArr = ['All', 'Public', 'Private'];
	const [courseFilter, setCourseFilter] = useState(coursesTypesArr[0]);

	const [isOpenModal, setIsOpenModal] = useState(false);
	const [courseToEdit, setCourseToEdit] = useState(null);

	const toggleModal = (e, courseId = '') => {
		setIsOpenModal(!isOpenModal);
		setCourseToEdit(allCourses.filter((c) => c.id === courseId)[0]);
	};

	const updateElementsPerPage = (e) => {
		setElementsPerPage(e.target.value);
		setPage(1);
	};

	useEffect(() => {
		setTimeout(() => {
			coursesRequests
				.getMyCourses(user.id, page, elementsPerPage, courseFilter, searchWord)
				.then((data) => {
					updateStates(data);
				});
		}, 100);
	}, [user, page, elementsPerPage, searchWord, courseFilter, fetchCourses]);

	const updateStates = (data) => {
		setAllCourses(data.result.courses);
		setTotalPages(data.result.totalPages);

		if (searchWord) {
			if (!data.result.coursesCount) {
				setSearchedCoursesCount(0);
				setInvalidSearch(true);
			} else {
				setSearchedCoursesCount(data.result.coursesCount);
				setInvalidSearch(false);
			}
		}
	};

	const onSearch = async (e) => {
		if (e.key === 'Enter') {
			setSearchWord(e.target.value);
			setPage(1);
		}
	};

	const createNewCourse = (course) => {
		setAllCourses([...allCourses, course]);
	};

	const onUnenroll = (courseId) => {
		setAllCourses(allCourses.filter((c) => c.id !== courseId));
	};

	const onEdit = (course) => {
		const newCourses = allCourses.filter((c) => c.id !== course.id);
		setAllCourses([course, ...newCourses]);
	};

	const btnClasses = btnStyles();
	const classes = useStyles();

	return (
		<MainView page="myCourses">
			<div className={classes.header}>
				{searchWord ? (
					<h1>
						{searchedCoursesCount} results for "{searchWord}"{' '}
						{user && user.isTeacher && invalidSearch
							? `in ${courseFilter} courses`
							: ''}
					</h1>
				) : (
					<>
						{' '}
						{user.isTeacher ? (
							<h1>Courses</h1>
						) : (
							<h1>Courses you are enrolled in</h1>
						)}{' '}
					</>
				)}

				<div className={classes.filterOptions}>
					{user.isTeacher ? (
						<Button className={btnClasses.btn} onClick={toggleModal}>
							Create
						</Button>
					) : null}
					<Search onEnter={(e) => onSearch(e)} />

					{user && user.isTeacher ? (
						<Box style={{ marginRight: '15px' }}>
							<Dropdown
								size={90}
								value={courseFilter}
								style={{ marginRight: '30px' }}
								onChange={(e) => setCourseFilter(e.target.value)}
							>
								{coursesTypesArr.map((type) => (
									<MenuItem key={type} value={type}>
										{type}
									</MenuItem>
								))}
							</Dropdown>
						</Box>
					) : null}

					<Dropdown
						size={40}
						value={elementsPerPage}
						onChange={(e) => updateElementsPerPage(e)}
					>
						{elementsPerPageArr.map((num) => (
							<MenuItem key={num} value={num}>
								{num}
							</MenuItem>
						))}
					</Dropdown>
				</div>
			</div>

			{isOpenModal ? (
				<CreateUpdateCourseModal
					isOpenModal={isOpenModal}
					toggleModal={toggleModal}
					onCreate={createNewCourse}
					onEdit={onEdit}
					course={courseToEdit}
					fetchCourses={fetchCourses}
					setFetchCourses={setFetchCourses}
				/>
			) : null}

			{invalidSearch && searchWord ? (
				<InvalidSearch searchWord={searchWord} />
			) : (
				<>
					{allCourses ? (
						<>
							<Grid container spacing={4} style={{ marginTop: '40px' }}>
								{allCourses.map((course) => (
									<Grid item sm={3} key={course.id}>
										<Link
											key={course.id}
											to={`/courses/${course.id}/sections`}
											style={{ textDecoration: 'none' }}
										>
											<CourseCard
												course={
													user.isTeacher
														? {
																...course,
																isStudentEnrolled: true,
																authorFirstName: user.firstName,
																authorLastName: user.lastName,
														  }
														: { ...course, isStudentEnrolled: true }
												}
												onUnenroll={onUnenroll}
												toggleModal={toggleModal}
											/>
										</Link>
									</Grid>
								))}
							</Grid>

							<Pagination
								page={page}
								count={totalPages}
								onChange={(_, page) => setPage(page)}
								className={classes.pagination}
								size="large"
							/>
						</>
					) : (
						<h2>You have no courses yet</h2>
					)}
				</>
			)}
		</MainView>
	);
};

export default MyCourses;
