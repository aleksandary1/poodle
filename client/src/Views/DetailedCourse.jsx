import { useEffect, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Box, Button, IconButton } from '@material-ui/core';
import {
	Add,
	ArrowDownward,
	ArrowUpward,
	Delete,
	Edit,
} from '@material-ui/icons';
import SectionCard from '../Components/Sections/SectionCard/SectionCard';
import AuthContext from '../providers/auth.provider';
import coursesRequests from '../Requests/courses.requests';
import sectionsRequests from '../Requests/sections.requests';
import MainView from './MainView';
import { useStyles } from './detailedCourse.styles';
import AddSectionModal from '../Components/Modals/AddSectionModal/AddSectionModal';
import { btnStyles as useStylesBtn } from '../style/button.styles';

const DetailedCourse = ({ computedMatch }) => {
	const { courseId } = computedMatch.params;
	const classes = useStyles();
	const [course, setCourse] = useState({});
	const [sections, setSections] = useState([]);
	const [addModal, setAddModal] = useState(false);
	const [order, setOrder] = useState(1);
	const [enrolledStudents, setEnrolledStudents] = useState([]);
	const [userEnrolled, setUserEnrolled] = useState(false);
	const { user } = useContext(AuthContext);
	const canEdit = user.isTeacher && user.id === course.creatorId;

	const deleteSection = async (sectionId) => {
		await sectionsRequests.deleteById(courseId, sectionId);
		await sectionsRequests
			.getAllByCourseId(courseId)
			.then((data) => setSections(data.result));
	};

	const orderDown = (sectionId) => {
		sectionsRequests
			.order(courseId, sectionId, 'down')
			.then((data) => setSections(data.result));
	};

	const orderUp = (sectionId) => {
		sectionsRequests
			.order(courseId, sectionId, 'up')
			.then((data) => setSections(data.result));
	};

	useEffect(() => {
		coursesRequests.getEnrolled(courseId).then((data) => {
			const result = data.result.map((r) => ({
				value: r.id,
				label: `${r.firstName} ${r.lastName} (${r.email})`,
			}));
			setEnrolledStudents(result);
			const isEnrolled = result.some((el) => el.value === user.id);
			setUserEnrolled(isEnrolled);
		});
	}, [courseId, user]);

	useEffect(() => {
		coursesRequests.getById(courseId).then((data) => setCourse(data.result));

		sectionsRequests.getAllByCourseId(courseId).then((data) => {
			setSections(data.result);
			setOrder(data.order);
		});
	}, [courseId]);

	const removeEnroll = () => {
		coursesRequests.unenrollStudent(courseId, user.id).then(() => {
			setUserEnrolled(false);
			enrolledStudents.filter((student) => student.value !== user.id);
		});
	};

	const enroll = () => {
		coursesRequests.enrollStudent(courseId, user.id).then(() => {
			setUserEnrolled(true);
			enrolledStudents.push({
				value: user.id,
				label: `${user.firstName} ${user.lastName} (${user.email})`,
			});
		});
	};

	const buttonClass = useStylesBtn();

	const teacherButtons = (
		<Box style={{ position: 'absolute', right: '100px' }}>
			<IconButton
				style={{
					color: 'white',
					backgroundColor: '#9575cd',
					marginBottom: 10,
				}}
				onClick={() => setAddModal(true)}
			>
				<Add />
			</IconButton>

			{user && user.isTeacher && !course.quiz ? (
				<Link
					to={`/courses/${courseId}/createQuiz`}
					style={{
						textDecoration: 'none',
						color: 'blue',
					}}
				>
					<Button
						className={buttonClass.btn}
						style={{ width: '100%', display: 'flex' }}
					>
						Create QUIZ
					</Button>
				</Link>
			) : null}
		</Box>
	);

	const enrollButton = (
		<>
			<Button className={classes.enrollButton} onClick={enroll}>
				Enroll me
			</Button>
		</>
	);

	const unrollButton = (
		<>
			<Button className={classes.enrollButton} onClick={removeEnroll}>
				Unroll me
			</Button>
		</>
	);

	return (
		<MainView>
			<AddSectionModal
				addModal={addModal}
				setAddModal={setAddModal}
				order={order}
				courseId={courseId}
				setSections={setSections}
			/>
			<Box>
				<Box className={classes.titleWithButtons}>
					<p className={classes.name}>
						<b>{course.title}</b>
					</p>
					<Box className={classes.buttons}>
						{canEdit
							? teacherButtons
							: user.isTeacher
							? null
							: userEnrolled
							? unrollButton
							: enrollButton}
					</Box>
				</Box>
				<Box>
					<p className={classes.description}>{course.description}</p>
				</Box>
				<Box>
					{sections.length ? (
						sections.map((section, index) => (
							<Box className={classes.cardButtons} key={section.id}>
								<SectionCard
									sectionId={section.id}
									title={section.title}
									content={section.content}
									courseId={courseId}
									deleteSection={deleteSection}
									enrolledStudents={enrolledStudents}
									isRestricted={section.is_restricted}
									canEdit={canEdit}
									restrictedBy={section.restricted_by}
								/>
								{canEdit ? (
									<Box>
										{index === 0 ? null : (
											<IconButton onClick={() => orderUp(section.id)}>
												<ArrowUpward />
											</IconButton>
										)}
										{index === sections.length - 1 ? null : (
											<IconButton onClick={() => orderDown(section.id)}>
												<ArrowDownward />
											</IconButton>
										)}
									</Box>
								) : null}
							</Box>
						))
					) : (
						<p>There are no sections yet! :(</p>
					)}

					{course.quiz ? (
						<Link
							to={{
								pathname: `/courses/${courseId}/quiz`,
								state: { quiz: course.quiz },
							}}
							style={{
								textDecoration: 'none',
								color: 'blue',
							}}
							quiz={course.quiz}
						>
							<Button
								className={buttonClass.btn}
								style={{ width: '100%', display: 'flex' }}
							>
								Start QUIZ
							</Button>
						</Link>
					) : null}
				</Box>
			</Box>
		</MainView>
	);
};

export default DetailedCourse;
