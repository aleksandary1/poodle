import { useState, useContext, useEffect } from 'react';
import Select from 'react-select';
import AuthContext from '../../../../providers/auth.provider';
import coursesRequests from '../../../../Requests/courses.requests';
import usersRequests from '../../../../Requests/users.requests';
import { Box, TextField } from '@material-ui/core';
import Switch from '../../../Switch/Switch';
import {
	useStyles,
	customStylesSelect,
} from '..//CreateUpdateCourseModal.styles';

const CreateEditBasicInfoStep = ({
	basicInfo,
	isEditMode,
	onCreate,
	onEdit,
	updateState,
}) => {
	const { user } = useContext(AuthContext);

	const mapUsersForDropDownSelect = (data) => {
		return data?.map(mapUserForDropDownSelect);
	};

	const mapUserForDropDownSelect = (student) => {
		return {
			value: student,
			label: `${student.firstName} ${student.lastName} (${student.email})`,
		};
	};

	const [isPrivate, setIsPrivate] = useState(
		basicInfo ? basicInfo.isPrivate : false
	);
	const [students, setStudents] = useState([]);

	const [enrolledStudents, setEnrolledStudents] = useState(
		basicInfo ? mapUsersForDropDownSelect(basicInfo.enrolledStudents) : []
	);

	const [existingTitles, setExistingTitles] = useState([]);
	const [invalidTitle, setInvalidTitle] = useState(false);
	const [invalidDescription, setInvalidDescription] = useState(false);
	const [invalidEnrolledStudents, setInvalidEnrolledStudents] = useState(false);
	const [newCourse, setNewCourse] = useState({
		id: basicInfo ? basicInfo.id : '',
		title: basicInfo ? basicInfo.title : '',
		description: basicInfo ? basicInfo.description : '',
		isPrivate: isPrivate,
		creatorId: user.id,
		enrolledStudents: enrolledStudents,
	});

	useEffect(() => {
		usersRequests.getAllByRole('students').then((data) => {
			const selectedStudentIds = basicInfo
				? basicInfo.enrolledStudents.map((s) => s.id)
				: [];
			data = data.filter((student) => !selectedStudentIds.includes(student.id));
			setStudents(mapUsersForDropDownSelect(data));
		});

		coursesRequests
			.getAllTitles()
			.then((data) => setExistingTitles(data.result));
	}, []);

	useEffect(() => {
		return () => {
			updateState({
				...newCourse,
				enrolledStudents: newCourse.enrolledStudents.map((x) => x.value),
			});
		};
	}, [newCourse]);

	const updateNewCourse = (prop, value) => {
		newCourse[prop] = value;
		setNewCourse({ ...newCourse });
	};

	const togglePrivate = () => {
		setIsPrivate(!isPrivate);
		updateNewCourse('isPrivate', !isPrivate);
	};

	const handleRestrictionSelect = async (e) => {
		if (e.action === 'select-option') {
			setEnrolledStudents([...enrolledStudents, e.option]);
			const enrolled = [...newCourse.enrolledStudents, e.option];
			updateNewCourse('enrolledStudents', enrolled);
		} else if (e.action === 'remove-value') {
			const newEnrolled = enrolledStudents.filter((x) => {
				return x.value.id !== e.removedValue.value.id;
			});
			setEnrolledStudents(newEnrolled);
			updateNewCourse('enrolledStudents', newEnrolled);
		} else if (e.action === 'clear') {
			setEnrolledStudents([]);
			updateNewCourse('enrolledStudents', []);
		}
	};

	const validateTitle = () => {
		if (!newCourse.title) {
			return setInvalidTitle(`Provide title`);
		}

		if (existingTitles.filter((x) => x.title === newCourse.title).length > 0) {
			return setInvalidTitle(`Course with such title already exists`);
		}

		setInvalidTitle('');
	};

	const validateDescription = () => {
		if (!newCourse.description) {
			return setInvalidDescription(`Provide description`);
		}

		if (newCourse.description.length < 10) {
			return setInvalidDescription(
				`Description should be at least 10 characters long`
			);
		}

		setInvalidDescription('');
	};

	const validateEnrolledStudents = () => {
		if (isPrivate) {
			updateNewCourse('isPrivate', true);

			if (!newCourse.enrolledStudents.length) {
				return setInvalidEnrolledStudents(
					`Invite students to course or make it public`
				);
			} else {
				updateNewCourse('enrolledStudents', newCourse.enrolledStudents);
			}
		}

		setInvalidEnrolledStudents('');
	};

	const classes = useStyles();

	return (
		<Box>
			<h2 style={{ marginTop: '5px' }}>
				{isEditMode ? 'Edit Main Overview' : 'Create New Course'}
			</h2>
			<TextField
				className={classes.outlinedTextField}
				style={!invalidTitle ? { marginBottom: '20px' } : null}
				multiline={true}
				variant="outlined"
				label="Title"
				value={newCourse.title}
				onChange={(e) => updateNewCourse('title', e.target.value)}
				onBlur={validateTitle}
			/>
			{invalidTitle && (
				<p id="invalidTitle" className={classes.invalidText}>
					{invalidTitle}
				</p>
			)}

			<TextField
				className={classes.outlinedTextField}
				style={!invalidDescription ? { marginBottom: '20px' } : null}
				rows={5}
				multiline={true}
				variant="outlined"
				label="Description"
				value={newCourse.description}
				onChange={(e) => updateNewCourse('description', e.target.value)}
				onBlur={validateDescription}
			/>
			{invalidDescription && (
				<p id="invalidDescription" className={classes.invalidText}>
					{invalidDescription}
				</p>
			)}

			<Box style={{ display: 'flex' }}>
				<Switch
					isChecked={isPrivate}
					label={isPrivate ? 'Private' : 'Public'}
					handleChange={togglePrivate}
					onBlur={validateEnrolledStudents}
				/>
				<p
					style={{
						color: 'grey',
						margin: '3px',
						justifyContent: 'center',
					}}
				>
					(Course will be available for&nbsp;
					{isPrivate ? 'only for selected users' : 'all users'})
				</p>
			</Box>

			{isPrivate ? (
				<Box style={{ marginTop: 15, marginBottom: 10, display: 'flex' }}>
					<Select
						value={enrolledStudents}
						maxMenuHeight={150}
						menuShouldScrollIntoView={true}
						closeMenuOnSelect={false}
						isMulti
						options={students}
						menuPortalTarget={document.body}
						styles={customStylesSelect}
						onChange={(_, action) => handleRestrictionSelect(action)}
					/>
				</Box>
			) : null}

			<div>
				{invalidEnrolledStudents && (
					<p id="invalidEnrolledStudents" className={classes.invalidText}>
						{invalidEnrolledStudents}
					</p>
				)}
			</div>
		</Box>
	);
};

export default CreateEditBasicInfoStep;
