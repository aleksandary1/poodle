import { withStyles } from '@material-ui/core/styles';
import {
	Switch as SwitchMaterialUi,
	FormControlLabel,
} from '@material-ui/core';
import { purple } from '@material-ui/core/colors';

const PurpleSwitch = withStyles({
	switchBase: {
		color: purple[300],
		'&$checked': {
			color: purple[500],
		},
		'&$checked + $track': {
			backgroundColor: purple[500],
		},
	},
	checked: {},
	track: {},
})(SwitchMaterialUi);

const Switch = ({ isChecked, label, handleChange }) => {
	return (
		<FormControlLabel
			control={<PurpleSwitch checked={isChecked} onChange={handleChange} />}
			label={label}
		/>
	);
};

export default Switch;
