import { useEffect, useState } from 'react';
import { Box, CardMedia, TextField } from '@material-ui/core';
import coursePicture from '../../../../Media/Course.jpg';
import { useStyles } from './CourseEditImageStep.styles';

const CourseEditImageStep = ({ image, updateState }) => {
	const [currentImage, setCurrentImage] = useState(null);
	const [preview, setPreview] = useState(coursePicture);
	const [showCoursePicture, setShowCoursePicture] = useState(!!image);

	const loadImage = (imageName) => {
		import(`../../../../uploads/${imageName}`).then((image) =>
			setCurrentImage(image.default)
		);
	};

	const onUpload = (e) => {
		const reader = new FileReader();
		reader.onload = () => {
			if (reader.readyState === 2) {
				setShowCoursePicture(false);
				setPreview(reader.result);
			}
		};
		const insertedFile = e.target.files[0];
		reader.readAsDataURL(insertedFile);
		setCurrentImage(insertedFile);
	};

	useEffect(() => {
		if (image) {
			loadImage(image);
			setShowCoursePicture(true);
		}
	}, [image]);

	useEffect(() => {
		return () => {
			updateState(currentImage);
		};
	}, [currentImage]);

	const classes = useStyles();
	return (
		<>
			<Box style={{ display: 'flex' }}>
				<h2 style={{ margin: '8px 0' }}>Choose Landing Picture</h2>
				<p
					style={{
						margin: 'auto 15px',
						justifyContent: 'center',
						fontStyle: 'italic',
					}}
				>
					(Optional)
				</p>
			</Box>
			<Box
				style={{
					display: 'flex',
					justifyContent: 'center',
				}}
			>
				<TextField
					type="file"
					className={classes.outlinedTextField}
					variant="outlined"
					onChange={onUpload}
				/>
			</Box>
			{currentImage ? null : (
				<p className={classes.imageMessage}>
					* If you skip uploading picture, course will have this image as
					default
				</p>
			)}
			<Box style={{ display: 'flex', justifyContent: 'center' }}>
				<CardMedia
					className={classes.media}
					title="Course"
					image={showCoursePicture ? currentImage : preview}
				></CardMedia>
			</Box>
		</>
	);
};

export default CourseEditImageStep;
