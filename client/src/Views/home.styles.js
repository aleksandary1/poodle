import { makeStyles } from '@material-ui/core';
import { deepPurple } from '@material-ui/core/colors';

export const useStyles = makeStyles(() => ({
	welcome: {
		fontSize: '25pt',
	},
	progress: {
		display: 'flex',
		width: '70%',
	},
	squares: {
		marginRight: '30px',
	},
	explore: {
		fontSize: '25pt',
		fontWeight: 'bold',
		marginTop: '50px',
		marginBottom: 0,
	},
	select: {
		minWidth: 200,
		marginRight: '10px',
		background: 'white',
		color: deepPurple[500],
		fontWeight: 200,
		borderStyle: 'none',
		borderWidth: 2,
		borderRadius: 12,
		paddingLeft: 24,
		paddingTop: 14,
		paddingBottom: 15,
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.14)',
		'&:focus': {
			borderRadius: 12,
			background: 'white',
			borderColor: deepPurple[100],
		},
	},
	icon: {
		color: deepPurple[300],
		right: 12,
		position: 'absolute',
		userSelect: 'none',
		pointerEvents: 'none',
	},
	paper: {
		borderRadius: 12,
		marginTop: 8,
	},
	list: {
		paddingTop: 0,
		paddingBottom: 0,
		background: 'white',
		'& li': {
			fontWeight: 200,
			paddingTop: 12,
			paddingBottom: 12,
		},
		'& li:hover': {
			background: deepPurple[100],
		},
		'& li.Mui-selected': {
			color: 'white',
			background: deepPurple[400],
		},
		'& li.Mui-selected:hover': {
			background: deepPurple[500],
		},
	},
	sort: {
		display: 'flex',
		marginTop: '15px',
	},
	options: {
		display: 'flex',
		justifyContent: 'space-between',
		width: '66%',
	},
	filterButton: {
		border: '1px solid',
		borderColor: 'rgba(239,239,239,255)',
		borderRadius: '20px',
	},
	filterButtonText: {
		margin: 0,
	},
	filterIconText: {
		padding: '10px',
		display: 'flex',
	},
	showMoreDiv: {
		display: 'flex',
		justifyContent: 'center',
		width: '66%',
	},
	showMoreButton: {
		height: 50,
    width: 200,
    marginTop: 20,
		marginBottom: 20,
    fontSize: '22px',
		letterSpacing: 1,
		textTransform: 'none',
		background: '#9575cd',
		color: '#fff',
		borderRadius: '10px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#7e57c2',
			color: '#fff',
		},
	},
}));
