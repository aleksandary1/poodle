/* eslint-disable import/no-anonymous-default-export */
import { BASE_URL } from '../common/constants';
import { createRequest } from './requests.helper';

const getAllByCourseId = (id) => {
  const request = createRequest('GET');

  return fetch(`${BASE_URL}/courses/${id}/sections`, request)
  .then((res) => res.json());
}

const updateById = (courseId, sectionId, data) => {
  const request = createRequest('PUT', {
    body: JSON.stringify(data),
  });

  return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, request)
  .then((res) => res.json());
}

const deleteById = (courseId, sectionId) => {
  const request = createRequest('DELETE');

  return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, request)
  .then((res) => res.json());
};

const add = (courseId, data) => {
  const request = createRequest('POST', {
    body: JSON.stringify(data)
  })

  return fetch(`${BASE_URL}/courses/${courseId}/sections`, request)
  .then((res) => res.json());
}

const order = (courseId, sectionId, way) => {
  const request = createRequest('PUT');

  return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}?order=${way}`, request)
  .then((res) => res.json());
}

const getAllRestricted = (sectionId) => {
  const request = createRequest('GET');

  return fetch(`${BASE_URL}/restrictions/${sectionId}`, request)
  .then((res) => res.json());
}

const restrictUser = (sectionId, userId) => {
  const request = createRequest('POST');

  return fetch(`${BASE_URL}/restrictions/${sectionId}/${userId}`, request)
  .then((res) => res.json());
}

const removeRestriction = (sectionId, userId) => {
  const request = createRequest('DELETE');

  return fetch(`${BASE_URL}/restrictions/${sectionId}/${userId}`, request)
  .then((res) => res.json());
}

const clearAllRestrictions = (sectionId) => {
  const request = createRequest('DELETE');

  return fetch(`${BASE_URL}/restrictions/${sectionId}`, request)
  .then((res) => res.json());
}

const restrictSection = (sectionId, isRestricted) => {
  const request = createRequest('PUT', {
    body: JSON.stringify({isRestricted})
  });

  return fetch(`${BASE_URL}/restrictions/${sectionId}`, request)
  .then((res) => res.json());
};

const restrictBy = (sectionId, isRestricted) => {
  const request = createRequest('PUT', {
    body: JSON.stringify(isRestricted)
  })

  return fetch(`${BASE_URL}/restrictions/${sectionId}/restrictBy`, request)
  .then((res) => res.json());
};

export default {
  getAllByCourseId,
  updateById,
  deleteById,
  add,
  order,
  getAllRestricted,
  restrictUser,
  removeRestriction,
  clearAllRestrictions,
  restrictSection,
  restrictBy,
}