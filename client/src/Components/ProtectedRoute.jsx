import React from 'react';
import { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import AuthContext from '../providers/auth.provider';

const ProtectedRoute = ({ path, Component, adminOnly, location, ...props }) => {
	const { user } = useContext(AuthContext);

	if (adminOnly) {
		if (!user.isAdmin) return <Redirect to="/" />;
	}

	if (location.state !== undefined) {
		props = { ...props, quiz: location.state.quiz };
	}

	return (
		<Route path={path} exact>
			{user ? <Component {...props} /> : <Redirect to="/login" />}
		</Route>
	);
};

export default ProtectedRoute;
