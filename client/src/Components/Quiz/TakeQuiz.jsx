import { useState, useContext } from 'react';
import QuizQuestion from './QuizQuestion';
import MainView from '../../Views/MainView';
import { Box, Button, Modal } from '@material-ui/core';
import { useStyles } from './QuizCreate.styles';
import AuthContext from '../../providers/auth.provider';
import { useHistory } from 'react-router-dom';

const TakeQuiz = ({ quiz, computedMatch }) => {
	const [countPoints, setCountPoints] = useState(0);
	const [selectedAnswers, setSelectedAnswers] = useState([]);
	const [isModalOpen, setIsModalOpen] = useState(false);

	const { courseId } = computedMatch.params;

	const history = useHistory();

	const toggleModal = () => {
		setIsModalOpen(!isModalOpen);
	};

	const { user } = useContext(AuthContext);

	const questionsWithCorrectAnswers = quiz.questions.map((question) => ({
		answers: question.answers
			.filter((answer) => answer.isCorrect)
			.map((answer) => answer.id),
		questionId: question.question.id,
		questionTotalPoints: question.question.points,
	}));

	const onSubmit = () => {
		const matchedSelectedAnswers = selectedAnswers.reduce(
			(acc, questionWithAnswer) => {
				const foundQuestionIndex = acc.findIndex(
					(question) => question.questionId === questionWithAnswer.questionId
				);

				if (foundQuestionIndex !== -1) {
					acc[foundQuestionIndex].answers = acc[
						foundQuestionIndex
					].answers.concat(' ', questionWithAnswer.id);

					return acc;
				}

				acc.push({
					questionId: questionWithAnswer.questionId,
					answers: questionWithAnswer.id,
				});

				return acc;
			},
			[]
		);

		questionsWithCorrectAnswers.reduce((acc, question) => {
			const selectedQuestionAnswers = matchedSelectedAnswers.filter(
				(matchedQuestion) => matchedQuestion.questionId === question.questionId
			);

			acc = question.answers.join(' ') === selectedQuestionAnswers[0]?.answers;

			if (acc) {
				setCountPoints(
					(countPoints) => (countPoints += question.questionTotalPoints)
				);
			}

			return acc;
		}, false);

		toggleModal();
		setTimeout(() => history.push(`/courses/${courseId}/sections`), 4000);
	};

	const classes = useStyles();
	return (
		<MainView>
			<Modal open={isModalOpen} onClose={() => setIsModalOpen(false)}>
				<Box
					style={{
						position: 'absolute',
						width: 800,
						height: 200,
						backgroundColor: 'white',
						borderRadius: '20px',
						boxShadow: '5px',
						outline: 0,
						left: '38%',
						top: '30%',
					}}
				>
					<h2
						style={{
							display: 'flex',
							justifyContent: 'center',
							marginTop: '70px',
						}}
					>
						You've taken the quiz with {countPoints} points!
					</h2>
				</Box>
			</Modal>

			<Box style={{ marginTop: '50px' }}>
				{quiz.questions.map((question) => (
					<Box
						key={question.question.id}
						style={{
							width: '800px',
							margin: '0 auto',
						}}
					>
						<QuizQuestion
							question={question}
							setSelectedAnswers={setSelectedAnswers}
						></QuizQuestion>
					</Box>
				))}
			</Box>

			<Box
				style={{
					display: 'flex',
					justifyContent: 'center',
					marginBottom: '50px',
				}}
			>
				<Button className={classes.purpleBtn} onClick={onSubmit}>
					Finish Quiz
				</Button>
			</Box>
		</MainView>
	);
};

export default TakeQuiz;
