import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
	root: {
		padding: '12px',
		height: 'auto',
	},
	h1: {
		fontWeight: 'bold',
		fontSize: '25px',
		margin: '8px auto',
	},
	subtitle: {
		fontSize: '15px',
		color: 'grey',
	},
	btn: {
		width: '100%',
		margin: '15px 0 5px 0',
	},
	bulletWrapper: {
		display: 'flex',
		height: '100%',
		marginBottom: '0px',
		verticalAlign: 'top',
	},
	icon: {
		fontSize: '18px',
		justifyContent: 'center',
		marginTop: '10px',
	},
	p: {
		display: 'inline',
		fontSize: '17px',
		margin: '12px 0 0 12px',
		lineHeight: '18px',
	},
}));
