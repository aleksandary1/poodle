import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import ScrollToTopArrow from '../Components/ScrollToTopArrow/ScrollToTopArrow';
import MainView from './MainView';
import { Box, Grid, MenuItem } from '@material-ui/core';
import coursesRequests from '../Requests/courses.requests';
import CourseCard from '../Components/Course/CourseCard/CourseCard';
import AuthContext from '../providers/auth.provider';
import Dropdown from '../Components/Dropdown/Dropdown';
import Search from '../Components/Search/Search';
import InvalidSearch from '../Components/Search/InvalidSearch';
import { useStyles } from './Courses.styles';

const CoursesList = () => {
	const { user } = useContext(AuthContext);

	const [allCourses, setAllCourses] = useState([]);

	const [searchWord, setSearchWord] = useState('');
	const [searchedCoursesCount, setSearchedCoursesCount] = useState(0);
	let [invalidSearch, setInvalidSearch] = useState(false);

	const elementsPerPageArr = [8, 16, 24, 40];

	const [page, setPage] = useState(1);
	const [totalPages, setTotalPages] = useState(1);
	const [elementsPerPage, setElementsPerPage] = useState(elementsPerPageArr[0]);

	const coursesTypesArr = ['All', 'Public', 'Private'];
	const [courseFilter, setCourseFilter] = useState(coursesTypesArr[0]);

	const updateElementsPerPage = (e) => {
		setElementsPerPage(e.target.value);
		setPage(1);
	};

	useEffect(() => {
		const updateOnCourseTypeChange = (data) => {
			setAllCourses(data.result.courses);
			setTotalPages(data.result.totalPages);

			if (searchWord) {
				if (!data.result.coursesCount) {
					setSearchedCoursesCount(0);
					setInvalidSearch(true);
				} else {
					setSearchedCoursesCount(data.result.coursesCount);
					setInvalidSearch(false);
				}
			}
		};

		if (user && user.isTeacher) {
			coursesRequests
				.getAll(page, elementsPerPage, courseFilter, searchWord)
				.then((data) => {
					updateOnCourseTypeChange(data);
				});
		} else {
			coursesRequests
				.getPublic(page, elementsPerPage, searchWord, user ? user.id : null)
				.then((data) => {
					updateOnCourseTypeChange(data);
				});
		}
	}, [user, page, elementsPerPage, courseFilter, searchWord]);

	const onSearch = async (e) => {
		if (e.key === 'Enter') {
			setSearchWord(e.target.value);
			setPage(1);
		}
	};

	const classes = useStyles();

	return (
		<MainView page="courses">
			<div className={classes.header}>
				{searchWord ? (
					<h1>
						{searchedCoursesCount} results for "{searchWord}"
					</h1>
				) : (
					<>
						{user ? (
							<>
								{user.isTeacher ? (
									<h1>Courses</h1>
								) : (
									<h1>Let's start learning, {user.firstName}</h1>
								)}
							</>
						) : (
							<h1>Let's start learning</h1>
						)}
					</>
				)}

				<div className={classes.filterOptions}>
					<Search onEnter={(e) => onSearch(e)} />

					{user && user.isTeacher ? (
						<Box style={{ marginRight: '15px' }}>
							<Dropdown
								size={90}
								value={courseFilter}
								style={{ marginRight: '30px' }}
								onChange={(e) => setCourseFilter(e.target.value)}
							>
								{coursesTypesArr.map((type) => (
									<MenuItem key={type} value={type}>
										{type}
									</MenuItem>
								))}
							</Dropdown>
						</Box>
					) : null}

					<Box>
						<Dropdown
							size={40}
							value={elementsPerPage}
							onChange={(e) => updateElementsPerPage(e)}
						>
							{elementsPerPageArr.map((num) => (
								<MenuItem key={num} value={num}>
									{num}
								</MenuItem>
							))}
						</Dropdown>
					</Box>
				</div>
			</div>

			{invalidSearch && searchWord ? (
				<InvalidSearch searchWord={searchWord} />
			) : (
				<>
					<Grid container spacing={4} style={{ marginTop: '40px' }}>
						{allCourses.map((course) => (
							<Grid item sm={3} key={course.id}>
								{user ? (
									<Link
										key={course.id}
										to={`/courses/${course.id}/sections`}
										style={{ textDecoration: 'none' }}
									>
										<CourseCard course={course} />
									</Link>
								) : (
									<CourseCard course={course} />
								)}
							</Grid>
						))}
					</Grid>

					<Pagination
						page={page}
						count={totalPages}
						onChange={(_, page) => setPage(page)}
						className={classes.pagination}
						size="large"
					/>
				</>
			)}
		</MainView>
	);
};

export default CoursesList;
