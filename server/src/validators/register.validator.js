import { body, validationResult } from 'express-validator';

const registerValidator = [
	body('email').isEmail().withMessage('Enter a valid email'),
	body('password')
		.isStrongPassword({ minLength: 6, minSymbols: 0 })
		.withMessage(
			'Password length should be between 6 and 20 and must contain 1 uppercase character, 1 lowercase character and 1 number'
		),
	body('firstName')
		.isLength({ min: 3 })
		.withMessage('First name should be at least 3 characters'),
	body('lastName')
		.isLength({ min: 3 })
		.withMessage('Last name should be at least 3 characters'),

	(req, res, next) => {
		const errors = validationResult(req);
		if (!errors.isEmpty())
			return res.status(422).json({ errors: errors.array() });
		next();
	},
];

export default registerValidator;
