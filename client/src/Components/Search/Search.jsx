import { Search as SearchIcon } from '@material-ui/icons';
import InputBase from '@material-ui/core/InputBase';
import { useStyles } from './search.styles';

const Search = ({ onEnter }) => {
	const classes = useStyles();

	return (
		<div className={classes.search}>
			<div className={classes.searchIcon}>
				<SearchIcon />
			</div>
			<InputBase
				onKeyDown={onEnter}
				placeholder="Search…"
				classes={{
					root: classes.inputRoot,
					input: classes.inputInput,
				}}
				inputProps={{ 'aria-label': 'search' }}
			/>
		</div>
	);
};

export default Search;
