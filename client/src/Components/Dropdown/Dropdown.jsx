import { Select } from '@material-ui/core';
import ExpandMore from '@material-ui/icons/ExpandMore';
import React from 'react';
import { useStyles } from './dropdown.styles';
import { makeStyles } from '@material-ui/core';

const Dropdown = ({
	value,
	children,
	onChange,
	size = '200px',
	marginRight = '0px',
}) => {
	const classes = useStyles();

	const styles2 = makeStyles(() => ({
		width: { width: size },
		marginRight: { marginRight: marginRight },
	}));
	const classes2 = styles2();

	const iconComponent = (props) => {
		return <ExpandMore className={props.className + ' ' + classes.icon} />;
	};

	const menuProps = {
		classes: {
			paper: classes.paper,
			list: classes.list,
		},
		anchorOrigin: {
			vertical: 'bottom',
			horizontal: 'left',
		},
		transformOrigin: {
			vertical: 'top',
			horizontal: 'left',
		},
		getContentAnchorEl: null,
	};

	return (
		<Select
			disableUnderline
			classes={{ root: `${classes.select} ${classes2.width}` }}
			MenuProps={menuProps}
			IconComponent={iconComponent}
			value={value}
			onChange={(e) => onChange(e)}
		>
			{children}
		</Select>
	);
};

export default Dropdown;
