import { makeStyles } from '@material-ui/core';
import { deepPurple } from '@material-ui/core/colors';

export const useStyles = makeStyles(() => ({
	modal: {
		position: 'absolute',
		width: 900,
		height: '70%',
		left: '50%',
		top: '50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'white',
		borderRadius: '25px',
		boxShadow: '0px 30px 50px -3px rgba(0,0,0,0.3)',
		padding: '40px',
	},
	outlinedTextField: {
		marginBottom: '20px	',
		marginRight: '0px',
		'& span.MuiTypography-root.MuiFormControlLabel-label.MuiTypography-body1': {
			flexGrow: '1',
		},
		width: '100%',
		[`& fieldset`]: {
			borderRadius: '15px',
			borderColor: deepPurple[500],
		},
		'& label.Mui-focused': {
			color: deepPurple[500],
			borderColor: 'solid 2px',
		},
		'& .MuiOutlinedInput-root': {
			'& fieldset': {
				borderWidth: '1.5px',
				borderColor: deepPurple[500],
			},
			'&.Mui-focused fieldset': {
				borderColor: deepPurple[500],
			},
		},
	},
	label: {
		marginRight: 0,
		marginLeft: 0,
		position: 'relative',
		width: '100%',
		marginTop: '20px',
		'& span:nth-child(2)': {
			flexGrow: 1,
		},
	},
	btn: {
		position: 'absolute',
		bottom: '40px',
		right: '30px',
		width: '100px',
		marginRight: '15px',
		marginTop: '20px',
		fontSize: '22px',
		letterSpacing: 1,
		textTransform: 'none',
		background: '#9575cd',
		color: '#fff',
		borderRadius: '10px',
		boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#7e57c2',
			color: '#fff',
		},
	},
	answersConteiner: {
		maxHeight: '400px',
		overflowY: 'scroll',
	},
	clearBtn: {
		zIndex: 1,
		position: 'absolute',
		right: 0,
		minWidth: '10px',
		borderRadius: '15px',
		color: 'grey',
	},
	checkBox: {
		color: 'grey',
	},
}));
