import pool from '../db-pool.js';

export const getRandomValue = async (column, table, whereClause = '') => {
	const sql = `
    SELECT ${column} 
    FROM ${table}
    ${whereClause}
    ORDER BY RAND ()
    LIMIT 1  
  `;

	const result = await pool.query(sql);
	return result[0];
};
