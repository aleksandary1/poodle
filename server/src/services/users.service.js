import * as uuid from 'uuid';
import bcrypt from 'bcrypt';
import usersData from '../data/users.data.js';
import { normalizeEntityProperties } from './entity-mapper.service.js';

const getAll = async () => {
	const usersRaw = await usersData.getAll();

	return usersRaw.map((user) => normalizeEntityProperties(user));
};

const getAllByRole = async (role) => {
	const usersRaw = await usersData.getAllByRole(role);

	return usersRaw.map((user) => normalizeEntityProperties(user));
};

const getById = async (id) => {
	return normalizeEntityProperties(await usersData.getBy('id', id));
};

const getBy = async (column, value) => {
	return normalizeEntityProperties(await usersData.getBy(column, value));
};

const create = async (user) => {
	user.id = uuid.v4();
	user.password = await bcrypt.hash(user.password, 10);

	return normalizeEntityProperties(await usersData.create(user));
};

const insertTeacher = async (teacher) => {
	teacher.id = uuid.v4();
	teacher.password = await bcrypt.hash(teacher.password, 10);

	await usersData.insertTeacher(teacher);
};

const update = async (updatedData) => {
	return normalizeEntityProperties(await usersData.update(updatedData));
};

const remove = async (id) => {
	return normalizeEntityProperties(await usersData.remove(id));
};

const validateUser = async (email, password) => {
	const user = await usersData.getAllBy('email', email);

	if (!user) {
		throw `User with email ${email} does not exist`;
	}

	const match = await bcrypt.compare(password, user.password);

	if (!match) {
		throw 'Wrong Password';
	}

	return normalizeEntityProperties(user);
};

export default {
	getAll,
	getAllByRole,
	getById,
	create,
	insertTeacher,
	getBy,
	update,
	remove,
	validateUser,
};
