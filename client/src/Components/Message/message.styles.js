import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  message: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 20
  },
  messageTop: {
    display: 'flex'
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    marginRight: 10,
    marginTop: '15px'
  },
  messageText: {
    padding: 10,
    borderRadius: 20,
    backgroundColor: '#1877f2',
    color: 'white',
    maxWidth: 300
  },
  messageTime: {
    fontSize: 12,
    marginTop: 10
  },
  own: {
    alignItems: 'flex-end',
    color: 'black'
  }
}));