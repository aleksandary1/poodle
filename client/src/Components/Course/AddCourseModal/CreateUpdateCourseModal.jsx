import { useState } from 'react';
import { useStyles } from './CreateUpdateCourseModal.styles';
import { Box, Modal, Backdrop, Fade, Grid } from '@material-ui/core';
import LinearStepper from '../../LinearStepper/LinearStepper';
import CreateEditBasicInfoStep from './CreateEditBasicInfoStep/CreateEditBasicInfoStep';
import CourseEditImageStep from './CourseEditImageStep/CourseEditImageStep';
import CreateEditWhatToLearnStep from './CreateEditWhatToLearnStep/CreateEditWhatToLearnStep';
import coursesRequests from '../../../Requests/courses.requests';
import CreateEditPreviewStep from './CreateEditPreviewStep/CreateEditPreviewStep';

const CreateUpdateCourseModal = ({
	isOpenModal,
	toggleModal,
	onCreate,
	onEdit,
	course,
	fetchCourses,
	setFetchCourses,
}) => {
	const [isEditMode, setIsEditMode] = useState(!!course);
	const [currentStep, setCurrentStep] = useState(0);
	const [basicInfo, setBasicInfo] = useState(
		isEditMode
			? {
					id: course.id,
					title: course.title,
					description: course.description,
					isPrivate: course.isPrivate,
					enrolledStudents: course.enrolledStudents,
			  }
			: undefined
	);

	const [image, setImage] = useState(
		isEditMode && course.image ? { ...course.image } : null
	);

	const initBullets = () => {
		if (isEditMode && course.bullets && course.bullets.length) {
			return course.bullets.map((bullet) => {
				return {
					id: bullet.id ? bullet.id : '',
					bullet: bullet.bullet ? bullet.bullet : '',
				};
			});
		} else {
			const newBullets = [];

			for (let index = 0; index < 3; index++) {
				newBullets.push({
					id: '',
					bullet: '',
				});
			}

			return newBullets;
		}
	};

	const [bullets, setBullets] = useState(initBullets());

	const changeCurrentStep = (step) => {
		setCurrentStep(step);
	};

	const onComplete = () => {
		const promise = isEditMode ? update() : create();

		toggleModal();

		promise.finally(() => {
			setFetchCourses(!fetchCourses);
		});
	};

	const create = () => {
		const newCourse = {
			course: {
				...basicInfo,
				enrolledStudents: basicInfo.enrolledStudents.map((s) => s.id),
				bullets: [...bullets.map((b) => b.bullet)],
			},
			courseImage: image,
		};

		return coursesRequests.create(newCourse);
	};

	const update = async () => {
		const updatedCourse = {
			course: {
				...basicInfo,
				bullets: [...bullets],
			},
			courseImage: image,
		};

		return await coursesRequests.update(updatedCourse);
	};

	const classes = useStyles();

	return (
		<Modal
			open={isOpenModal}
			onClose={toggleModal}
			closeAfterTransition
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 500,
			}}
		>
			<Fade in={isOpenModal}>
				<Grid container direction="column">
					<Box className={classes.modal}>
						<Grid item xs={12} style={{ height: '75%' }}>
							{currentStep === 0 ? (
								<CreateEditBasicInfoStep
									isEditMode={isEditMode}
									basicInfo={basicInfo}
									onCreate={onCreate}
									onEdit={onEdit}
									modalStep={currentStep}
									updateState={(state) => {
										setBasicInfo(state);
									}}
								/>
							) : null}

							{currentStep === 1 ? (
								<CourseEditImageStep
									image={isEditMode ? course.image : undefined}
									updateState={(state) => {
										setImage(state);
									}}
								/>
							) : null}

							{currentStep === 2 ? (
								<CreateEditWhatToLearnStep
									updateState={(state) => {
										setBullets(state);
									}}
									bullets={bullets}
								/>
							) : null}

							{currentStep === 3 ? (
								<CreateEditPreviewStep
									basicInfo={basicInfo}
									image={image}
									bullets={bullets}
								/>
							) : null}
						</Grid>
						<Grid item xs={12} style={{ height: '25%' }}>
							<LinearStepper
								mode={isEditMode ? 'Edit' : 'Create'}
								changeStep={changeCurrentStep}
								modalStep={currentStep}
								complete={() => onComplete()}
							></LinearStepper>
						</Grid>
					</Box>
				</Grid>
			</Fade>
		</Modal>
	);
};

export default CreateUpdateCourseModal;
