import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
  root: {
    marginTop: '1%',
    height: '95vh'
  },
  leftSide: {
    padding: 10,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    borderRight: '0.5px solid gray'
  },
  searchInput: {
    width: '100%',
    height: 20,
    marginBottom: '30px',
  },
  chatBox: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    position: 'relative',
    padding: '10px',
    height: '100%',
  },
  top: {
    height: '83vh',
    overflowY: 'scroll',
    paddingRight: 10
  },
  bottom: {
    marginTop: 5,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  chatInput: {
    width: '80%',
    padding: 10
  },
  submitButton: {
    width: 70,
    height: 40,
    borderRadius: 5,
    backgroundColor: 'teal',
    color: 'white',
    marginRight: '20px'
  },
  noChat: {
    position: 'absolute',
    top: '40%',
    fontSize: 30,
    color: 'rgb(224, 220, 220)',
    cursor: 'default',
    left: '40%'
  }
}));