import { makeStyles } from "@material-ui/core";


export const useStyles = makeStyles(() => ({
	logo: {
		display: 'flex',
		justifyContent: 'center',
	},
	drawerPaper: {
		width: '300px',
		display: 'flex',
		border: 0,
		borderRadius: '0px 20px 20px 0px',
	},
	selectedPage: {
		borderLeft: '5px solid black',
	},
	listIcon: {
		marginLeft: '20%',
		justifyContent: 'flex-end',
		marginRight: '10px',
	},
  notLogged: {
    display: 'flex'
  }
}));