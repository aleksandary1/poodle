import pool from './db-pool.js';

const getAll = async () => {
	const sql = `
  SELECT *
  FROM sections
  `;

	return await pool.query(sql);
};

const getBy = async (column, value) => {
	const sql = `
  SELECT *
  FROM sections
  WHERE ${column} = ?
  `;

	const result = await pool.query(sql, value);
	return result[0];
};

const getAllBy = async (column, value) => {
	const sql = `
	SELECT *
	FROM sections
	WHERE ${column} = ?
	AND is_deleted = 0
	ORDER BY section_order asc
	`;

	return await pool.query(sql, value);
};

const getOrderBy = async (column, value) => {
	const sql = `
	SELECT COUNT(*) as count
	FROM sections
	WHERE ${column} = ?
	AND is_deleted = 0;
	`;

	const order = await pool.query(sql, value);
	return order[0];
};

const getCountByCourseId = async (courseId) => {
	const sql = `
	SELECT COUNT(*)
	FROM sections
	WHERE course_id = ? AND is_deleted = 0;
	`;

	const _ = await pool.query(sql, [courseId]);
	const sectionsCount = _[0]['COUNT(*)'];
	return sectionsCount;
};

const create = async (data, courseId) => {
	const { id, title, content, order, restriction_date } = data;

	const sql = `
  INSERT INTO sections (id, title, content, section_order, restriction_date, course_id)
  VALUES (?, ?, ?, ?, ?, ?);
  `;

	await pool.query(sql, [
		id,
		title,
		content,
		order,
		restriction_date || null,
		courseId,
	]);

	return await getBy('id', id);
};

const edit = async (id, editData, courseId) => {
	const { title, content, section_order, restriction_date } = editData;

	const sql = `
  UPDATE sections
  SET title = ?, content = ?, section_order = ?, restriction_date = ?, course_id = ?
  WHERE id = ?;
  `;

	await pool.query(sql, [
		title,
		content,
		section_order,
		restriction_date,
		courseId,
		id,
	]);

	return await getBy('id', id);
};

const remove = async (id) => {
	const sql = `
  UPDATE sections
  SET is_deleted = 1
  WHERE id = ?
  `;

	await pool.query(sql, id);

	return await getBy('id', id);
};

const moveUp = async (id) => {
	const sql = `
	UPDATE sections
	SET section_order = section_order-1
	WHERE id = ?
	`;

	return await pool.query(sql, id);
};

const moveDown = async (id) => {
	const sql = `
	UPDATE sections
	SET section_order = section_order+1
	WHERE id = ?
	`;

	return await pool.query(sql, id);
};

const removeAllByCourseId = async (courseId) => {
	const sql = `
	UPDATE sections
	SET is_deleted = 1
	WHERE course_id = ?;
	`;

	await pool.query(sql, [courseId]);
};

export default {
	getAll,
	getBy,
	getAllBy,
	getOrderBy,
	getCountByCourseId,
	create,
	edit,
	remove,
	moveUp,
	moveDown,
	removeAllByCourseId,
};
