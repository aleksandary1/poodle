/* eslint-disable import/no-anonymous-default-export */
import { BASE_URL } from '../common/constants';
import { createRequest } from './requests.helper';

const addMessage = (messageData) => {
  const request = createRequest('POST', {
    body: JSON.stringify(messageData)
  })

  return fetch(`${BASE_URL}/messages`, request)
  .then((res) => res.json());
}

const get = (conversationId) => {
  const request = createRequest('GET');

  return fetch(`${BASE_URL}/messages/${conversationId}`, request)
  .then((res) => res.json());
} 

export default {
  addMessage,
  get
}