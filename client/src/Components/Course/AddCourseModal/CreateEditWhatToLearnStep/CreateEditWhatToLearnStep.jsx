import { useEffect, useState } from 'react';
import { Box, TextField } from '@material-ui/core';
import { useStyles } from '..//CreateUpdateCourseModal.styles';

const CreateEditWhatToLearnStep = ({ updateState, bullets }) => {
	const [currentBullets, setCurrentBullets] = useState(bullets);

	const updateBullet = (index, value) => {
		currentBullets[index].bullet = value;
		setCurrentBullets([...currentBullets]);
	};

	useEffect(() => {
		return () => {
			updateState(currentBullets);
		};
	}, [currentBullets]);

	const classes = useStyles();

	return (
		<Box>
			<h2 style={{ marginTop: '5px' }}>Describe course's main advantages</h2>
			{currentBullets.map((_, index) => (
				<TextField
					key={index}
					className={classes.outlinedTextField}
					style={{ marginBottom: '20px' }}
					rows={3}
					multiline={true}
					variant="outlined"
					label={`Advantage №${index + 1} `}
					value={currentBullets[index].bullet}
					onChange={(e) => updateBullet(index, e.target.value)}
				/>
			))}
		</Box>
	);
};

export default CreateEditWhatToLearnStep;
