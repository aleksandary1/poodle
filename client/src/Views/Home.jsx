import { Box, Button, Grid } from '@material-ui/core';
import { useState, useEffect } from 'react';
import SquareProgress from '../Components/SquareProgress/SquareProgress';
import MainView from './MainView';
import { useContext } from 'react';
import AuthContext from '../providers/auth.provider';
import { useStyles } from './home.styles';
import coursesRequests from '../Requests/courses.requests';
import CourseCard from '../Components/Course/CourseCard/CourseCard';
import { Link, useHistory } from 'react-router-dom';
import AllInclusiveIcon from '@material-ui/icons/AllInclusive';

const Home = () => {
	const classes = useStyles();
	const [courses, setCourses] = useState([]);
	const [overview, setOverview] = useState({});

	const history = useHistory();

	const { user } = useContext(AuthContext);

	const options = (
		<>
			<SquareProgress
				number={Math.floor((overview.coursesCount || 10) / 10) * 10 + ' +'}
				text="Courses"
			/>
			<SquareProgress
				number={Math.floor((overview.sectionsCount || 10) / 10) * 10 + ' +'}
				text="Lessons"
			/>
			<SquareProgress
				number={Math.floor((overview.studentsCount || 10) / 10) * 10 + ' +'}
				text="Curious students"
			/>
			<SquareProgress
				number={
					<AllInclusiveIcon
						style={{ transform: 'scale(2.4)', marginLeft: '20px' }}
					/>
				}
				text="Career Paths"
			/>
		</>
	);

	useEffect(() => {
		if (user) {
			coursesRequests.getWithLimit(8).then((data) => setCourses(data.result));
		} else {
			coursesRequests
				.getPublic(1, 8)
				.then((data) => setCourses(data.result.courses));
		}

		coursesRequests.getOverview().then((data) => setOverview(data.result));
	}, [user]);

	return (
		<MainView page="home">
			<Box style={{ width: '100%' }}>
				<Box className={classes.welcome}>
					{user ? (
						<p>
							Hello, <b>{user.firstName}</b>
						</p>
					) : (
						<p>
							<b>Welcome</b>
						</p>
					)}
				</Box>
				<Box style={{ display: 'flex', marginRight: '90px' }}>
					<Box className={classes.progress}>{options}</Box>
					<h2
						style={{
							fontStyle: 'italic',
							letterSpacing: 1.5,
							width: 600,
							marginTop: '10px',
							marginBottom: '0px',
							marginLeft: '25px',
						}}
					>
						Poodle is an online learning platform with many classes for curious
						people. On Poodle, thousands of members come together to find
						inspiration and take the next step in their professional journey.
					</h2>
				</Box>
			</Box>
			<Box>
				<Box style={{ marginTop: '75px' }}>
					<Grid container spacing={1}>
						{courses.map((course) => (
							<Grid item xs={12} sm={3} key={course.id}>
								<Link
									to={`/courses/${course.id}/sections`}
									style={{ textDecoration: 'none' }}
								>
									<CourseCard course={course} />
								</Link>
							</Grid>
						))}
					</Grid>
				</Box>
			</Box>
		</MainView>
	);
};

export default Home;
