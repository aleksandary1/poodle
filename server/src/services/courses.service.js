import courseData from '../data/courses.data.js';
import { v4 as uuid } from 'uuid';
import { normalizeEntityProperties } from './entity-mapper.service.js';
import coursesMappingConfig from '../common/courses-mapping-config.js';
import {
	getSectionsCountByCourseId,
	removeAllSectionsByCourseId,
} from './sections.service.js';
import quizService from './quiz.service.js';

const nameConfig = coursesMappingConfig.getConfigPropertyNames();

/**
 * Function to take all existing courses
 * @returns Array of courses
 */
export const getCourses = async () => {
	const coursesRaw = await courseData.getAll();

	return await addBulletsToCourses(coursesRaw);
};

/**
 * Function to retrieve all existing titles
 * @returns Array of all titles
 */
export const getAllCoursesTitles = async () => {
	return await courseData.getAllTitles();
};

/**
 * Function to retrieve courses, sections, users and teachers count
 * @returns Object
 */
export const getOverview = async () => {
	return normalizeEntityProperties(await courseData.getOverview());
};

/**
 * Function to get all courses created by a certain teacher
 * @param {boolean} searchInAll
 * @param {boolean} isPrivateMode
 * @param {string} id
 * @param {number} page
 * @param {number} elementsPerPage
 * @param {string} searchWord
 * @returns Array of courses
 */
export const getCoursesByCreatorId = async (
	searchInAll,
	isPrivateMode,
	id,
	page,
	elementsPerPage,
	searchWord
) => {
	const skipElements = (page - 1) * elementsPerPage;

	const rawCourses = await courseData.getAllByCreatorId(
		searchInAll,
		isPrivateMode,
		id,
		skipElements,
		elementsPerPage,
		searchWord
	);

	const courses = await addBulletsToCourses(rawCourses);

	const coursesWithEnrolledStudent = await addEnrolledStudentsToCourses(
		courses
	);

	const coursesCount = await courseData.getCountByCreatorId(
		id,
		searchInAll,
		isPrivateMode,
		searchWord
	);
	const totalPages = Math.ceil(coursesCount / elementsPerPage);

	return await {
		courses: coursesWithEnrolledStudent,
		totalPages,
		coursesCount,
	};
};

/**
 * Function to retrieve public courses by page
 * @param {string} userId
 * @param {boolean} isPrivate
 * @param {number} page
 * @param {number} elementsPerPage
 * @returns
 */
export const getPublicCoursesByPage = async (
	userId,
	isPrivate,
	page,
	elementsPerPage
) => {
	const skipElements = (page - 1) * elementsPerPage;

	const rawCourses = await courseData.getPublicWithLimit(
		userId,
		isPrivate,
		skipElements,
		elementsPerPage
	);

	const courses = await addBulletsToCourses(rawCourses);

	const coursesCount = await courseData.getCountByCourseStatus(
		false,
		isPrivate
	);

	const totalPages = Math.ceil(coursesCount / elementsPerPage);

	return { courses, totalPages };
};

/**
 * Get course by certain column
 * @param {string} column
 * @param {any} value
 * @returns {object} Course with keys:
 * id, title, description, isPrivate, createdOn, lastModified, bullets
 */
export const getCourseBy = async (column, value) => {
	const course = await courseData.getBy(column, value);

	const quiz = await quizService.getByCourseId(course.id);

	if (quiz) {
		course['quiz'] = quiz;
	}

	const result = await addBulletsToCourses([course]);
	return result[0];
};

/**
 * Function to retrieve all courses by search criteria
 * @param {boolean} searchInAll
 * @param {boolean} isPrivate
 * @param {number} page
 * @param {number} elementsPerPage
 * @param {string} searchWord
 * @param {string} column
 * @param {string} userId
 * @returns {object} Courses by search, total pages and count of courses
 * that meet the searched criteria
 */
export const getCoursesBySearch = async (
	searchInAll,
	isPrivate,
	page,
	elementsPerPage,
	searchWord,
	column,
	userId
) => {
	const skipElements = (page - 1) * elementsPerPage;

	const rawCourses = await courseData.getBySearchWordWithLimit(
		searchInAll,
		isPrivate,
		skipElements,
		elementsPerPage,
		searchWord,
		column,
		userId
	);

	const coursesWithBullets = await addBulletsToCourses(rawCourses);
	const coursesWithEnrolledStudent = await addEnrolledStudentsToCourses(
		coursesWithBullets
	);

	const coursesCount = await courseData.getCountBySearchWord(
		searchInAll,
		isPrivate,
		column,
		searchWord
	);

	const totalPages = Math.ceil(coursesCount / elementsPerPage);

	return { courses: coursesWithEnrolledStudent, totalPages, coursesCount };
};

/**
 * Function to retrieve public courses by search criteria
 * @param {string} userId
 * @param {boolean} isPrivate
 * @param {number} page
 * @param {number} elementsPerPage
 * @param {string} searchWord
 * @param {string} column
 * @returns {object} Courses by search, total pages and count of courses
 * that meet the searched criteria
 */
export const getPublicCoursesBySearch = async (
	userId,
	isPrivate,
	page,
	elementsPerPage,
	searchWord,
	column
) => {
	const skipElements = (page - 1) * elementsPerPage;

	const rawCourses = await courseData.getPublicBySearchWordWithLimit(
		userId,
		isPrivate,
		skipElements,
		elementsPerPage,
		searchWord,
		column
	);

	const coursesWithBullets = await addBulletsToCourses(rawCourses);
	const coursesWithEnrolledStudent = await addEnrolledStudentsToCourses(
		coursesWithBullets
	);

	const coursesCount = await courseData.getCountBySearchWord(
		0,
		isPrivate,
		column,
		searchWord
	);

	const totalPages = Math.ceil(coursesCount / elementsPerPage);

	return { courses: coursesWithEnrolledStudent, totalPages, coursesCount };
};

/**
 * Function to create new course
 * @param {obj} course
 * @returns {obj} Created course with sections count and bullets
 */
export const addCourse = async (course) => {
	course.id = uuid();

	const newCourseCreated = await courseData.add(course);

	if (course.enrolledStudents.length > 0) {
		course.enrolledStudents.map(
			async (studentId) => await enrollUserInCourse(course.id, studentId)
		);
	}

	const bullets = await createBullets(course.id, course.bullets);

	const sectionsCount = await getSectionsCountByCourseId(course.id);

	return { ...newCourseCreated, sectionsCount, bullets };
};

/**
 * Function to add bullets to course description
 * @param {string} courseId
 * @param {array} bullets
 * @returns {array} of the created bullets
 */
export const createBullets = async (courseId, bullets) => {
	return await courseData.addBullets(courseId, bullets);
};

/**
 * Function to update existing bullets
 * @param {array} bullets
 * @returns {array} Updated bullets
 */
export const updateBullets = async (bullets) => {
	const updatedBullets = await Promise.all(
		bullets.map(async (b) => {
			if (!b.id) {
				b.id = uuid();
			}

			normalizeEntityProperties(await courseData.updateBullet(b));
		})
	);

	return updatedBullets;
};

/**
 * Function to update existing course
 * @param {string} id
 * @param {object} updateData
 * @returns {object} Updated course
 */

export const updateCourse = async (id, updateData) => {
	const courseToUpdate = await getCourseBy('id', id);

	if (!courseToUpdate) {
		return null;
	}

	if (courseToUpdate.isPrivate && !updateData.isPrivate) {
		await unenrollAllStudentsByCourseId(courseToUpdate.id);
	} else if (!courseToUpdate.isPrivate && updateData.isPrivate) {
		const studentsToEnroll = updateData.enrolledStudents;

		studentsToEnroll.map(
			async (x) => await enrollUserInCourse(courseToUpdate.id, x)
		);
	} else if (courseToUpdate.isPrivate && updateData.isPrivate) {
		await unenrollAllStudentsByCourseId(courseToUpdate.id);

		await courseData.enrollMultipleStudentInCourse(
			courseToUpdate.id,
			updateData.enrolledStudents
		);
	}

	const imageName = updateData.imageName
		? updateData.imageName
		: courseToUpdate.image;

	const updatedBullets = await updateBullets(updateData.bullets);
	const updatedCourse = await courseData.updateCourse(id, {
		...courseToUpdate,
		...updateData,
		imageName,
	});

	return { ...updatedCourse, bullets: updatedBullets };
};

/**
 * Function to delete course, it's sections, bullets and enrolled
 * students (if there are any)
 * @param {string} id
 */
export const deleteCourse = async (id) => {
	await removeAllSectionsByCourseId(id);
	await courseData.removeBulletsByCourseId(id);
	await courseData.unenrollAllStudentsByCourseId(id);

	await courseData.remove(id);
};

/**
 * Function to retrieve specific count of courses
 * @param {boolean} all
 * @param {boolean} isPrivate
 * @param {number} page
 * @param {number} limit
 * @param {string} userId
 * @returns {array}
 */
export const getCoursesWithLimit = async (
	all,
	isPrivate,
	page,
	limit,
	userId
) => {
	const rawCourses = await courseData.getWithLimit(
		all,
		isPrivate,
		page,
		limit,
		userId
	);

	return await addBulletsToCourses(rawCourses);
};

/**
 * Function to retrieve specific amount of public courses
 * @param {number} limit
 * @returns {array}
 */
export const getPublicCoursesWithLimit = async (limit) => {
	const publicCourses = await courseData.getPublicWithLimit(limit);

	return await addBulletsToCourses(publicCourses);
};

/**
 * Function to take all courses user is enrolled in with search criteria
 * @param {string} userId
 * @param {number} page
 * @param {number} elementsPerPage
 * @param {string} searchWord
 * @returns {array}
 */
export const getEnrolledCoursesByUserId = async (
	userId,
	page,
	elementsPerPage,
	searchWord
) => {
	const skipElements = (page - 1) * elementsPerPage;

	const rawCourses = await courseData.getEnrolledByUserId(
		userId,
		skipElements,
		elementsPerPage,
		searchWord
	);

	const courses = await addBulletsToCourses(rawCourses);
	let coursesCount = null;

	if (searchWord) {
		coursesCount = await courseData.getCountOfEnrolledCoursesByUserIdWithSearch(
			userId,
			'title',
			searchWord
		);
	} else {
		coursesCount = await courseData.getCountOfEnrolledCoursesByUserId(userId);
	}

	const totalPages = Math.ceil(coursesCount / elementsPerPage);

	return { courses, totalPages, coursesCount };
};

/**
 * Function to get all students that are enrolled in a certain course
 * @param {string} courseId
 * @returns {array}
 */
export const getEnrolledStudentsByCourseId = async (courseId) => {
	const users = await courseData.getEnrolledUsers(courseId);

	return users.map((u) => normalizeEntityProperties(u));
};

/**
 * Function to enroll student in a course
 * @author Kalina Georgieva
 * @param {string} courseId
 * @param {string} user
 * @returns {object} Student's id and course's Id
 */
export const enrollUserInCourse = async (courseId, user) => {
	const enroll = await courseData.enrollStudentInCourse(courseId, user.id);

	return normalizeEntityProperties(enroll);
};

/**
 * Function to unenroll student from a course
 * @param {string} courseId
 * @param {string} userId
 * @returns {array}
 */
export const unenrollUserInCourse = async (courseId, userId) => {
	await courseData.unenrollSingleStudentInCourse(courseId, userId);
};

export const unenrollAllStudentsByCourseId = async (courseId) => {
	await courseData.unenrollAllStudentsByCourseId(courseId);
};

/**
 * Function to take courses on specific page
 * @author Kalina Georgieva
 * @param {boolean} all
 * @param {boolean} isPrivate
 * @param {number} page
 * @param {number} elementsPerPage
 * @param {string} userId
 * @returns {array}
 */
export const getCoursesByPage = async (
	all,
	isPrivate,
	page,
	elementsPerPage,
	userId
) => {
	const skipElements = (page - 1) * elementsPerPage;

	const rawCourses = await courseData.getWithLimit(
		all,
		isPrivate,
		skipElements,
		elementsPerPage,
		userId
	);

	const courses = await addBulletsToCourses(rawCourses);

	const allCoursesCount = await courseData.getCountByCourseStatus(
		all,
		isPrivate
	);

	const totalPages = Math.ceil(allCoursesCount / elementsPerPage);

	return { courses, totalPages };
};

/* ---------- HELPER FUNCTIONS ---------- */

const addBulletsToCourses = async (courses) => {
	return Promise.all(
		courses.map(async (course) => {
			const bullets = await courseData.getBulletsByCourseId(course.id);
			return { ...normalizeEntityProperties(course, nameConfig), bullets };
		})
	);
};

const addEnrolledStudentsToCourses = async (courses) => {
	return await Promise.all(
		courses.map(async (c) => {
			const enrolledStudentsRaw = await courseData.getEnrolledUsers(c.id);

			const enrolledStudents = enrolledStudentsRaw.map((x) =>
				normalizeEntityProperties(x)
			);

			return { ...c, enrolledStudents };
		})
	);
};
