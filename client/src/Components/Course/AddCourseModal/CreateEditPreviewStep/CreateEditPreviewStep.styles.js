import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
	root: {
		/* width */
		'& ::-webkit-scrollbar': {
			width: '10px',
		},

		/* Track */
		'& ::-webkit-scrollbar-track': {
			boxShadow: 'inset 0 0 5px grey',
			borderRadius: '10px',
		},

		/* Handle */
		'& ::-webkit-scrollbar-thumb': {
			background: 'gray',
			borderRadius: '10px',
		},

		/* Handle on hover */
		'& ::-webkit-scrollbar-thumb:hover': {
			background: '#606164',
		},
	},
}));
