import { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import { Card, CardMedia, Typography, Tooltip } from '@material-ui/core';
import coursePicture from '../../../Media/Course.jpg';
import coursesRequests from '../../../Requests/courses.requests';
import CourseOverview from '../CourseOverview/CourseOverview';
import { useStyles } from './courseCard.styles';
import AuthContext from '../../../providers/auth.provider';

const useStylesLight = makeStyles((theme) => ({
	tooltip: {
		margin: '20px',
		zIndex: '100px',
		backgroundColor: theme.palette.common.white,
		color: 'rgba(0, 0, 0, 0.87)',
		boxShadow: theme.shadows[8],
		fontSize: 11,
	},
	arrow: {
		zIndex: '50px',
		fontSize: 20,
		color: theme.palette.common.white,
	},
}));

const CourseCard = ({ course, onUnenroll, toggleModal }) => {
	const [image, setImage] = useState(null);

	const loadImage = (imageName) => {
		if (imageName) {
			import(`../../../uploads/${imageName}`).then((image) =>
				setImage(image.default)
			);
		}
	};

	useEffect(() => {
		loadImage(course.image);
	}, [course.image]);

	const { user } = useContext(AuthContext);
	const history = useHistory();

	const [isUserEnrolled, setIsUserEnrolled] = useState(
		course.isStudentEnrolled
	);

	const enrollStudent = (e) => {
		e.preventDefault();

		coursesRequests
			.enrollStudent(course.id, user.id)
			.then(setIsUserEnrolled(true));

		history.push(`/courses/${course.id}/sections`);
	};

	const unenrollStudent = (e) => {
		e.preventDefault();

		coursesRequests
			.unenrollStudent(course.id, user.id)
			.then(setIsUserEnrolled(false));
	};

	const onEdit = () => {};

	const LightTooltip = (props) => {
		const classes = useStylesLight();

		return <Tooltip arrow classes={classes} {...props} />;
	};

	const classes = useStyles();

	return (
		<LightTooltip
			title={
				<CourseOverview
					course={course}
					enrollStudent={enrollStudent}
					isUserEnrolled={isUserEnrolled}
					unenrollStudent={unenrollStudent}
					onUnenroll={onUnenroll}
					toggleModal={toggleModal}
				/>
			}
			arrow
			interactive={true}
			placement={'right'}
		>
			<Card className={classes.root} elevation={0}>
				<CardMedia
					className={classes.media}
					title="Course"
					image={image ? image : coursePicture}
				></CardMedia>

				<Typography variant="h6" className={classes.title}>
					{course.title}
				</Typography>

				<Typography variant="subtitle1" className={classes.subtitle}>
					{`${course.authorFirstName} ${course.authorLastName}`}
				</Typography>
			</Card>
		</LightTooltip>
	);
};

export default CourseCard;
