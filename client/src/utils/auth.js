import decode from 'jwt-decode';

const getUserFromToken = (token) => {
	try {
		const user = decode(token);
		return user;
	} catch (err) {
		localStorage.removeItem('token');
	}

	return null;
};

export default getUserFromToken;
