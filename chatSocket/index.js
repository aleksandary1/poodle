import { createServer } from 'http';
import { Server } from 'socket.io';

const httpServer = createServer();
const options = {
	cors: {
		origin: '*',
	},
};
const io = new Server(httpServer, options);

let users = [];

const addUser = (userId, socketId) => {
	if (!users.some((user) => user.userId === userId)) {
		users.push({ userId, socketId });
	}
};

const removeUser = (socketId) => {
	users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
	return users.find((user) => user.userId === userId);
};

io.on('connection', (socket) => {
	console.log('user connected');

	socket.on('addUser', (userId) => {
		addUser(userId, socket.id);
		io.emit('getUsers', users);
	});

	socket.on('sendMessage', ({ conversationId, receiverId }) => {
		const user = getUser(receiverId);
		if (!user) return null;
		io.to(user.socketId).emit('getMessage', {
			conversationId,
		});
	});

	socket.on('createConversation', ({ conversationId, receiverId }) => {
		const user = getUser(receiverId);
		if (!user) return null;
		io.to(user.socketId).emit('newConversation', {
			conversationId,
			userId: receiverId,
		});
	});

	socket.on('disconnect', () => {
		console.log('user disconnected');
		removeUser(socket.id);
		io.emit('getUsers', users);
	});
});

httpServer.listen(3555, console.log('listening on port 3555'));
