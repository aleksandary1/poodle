const createPoodleDatabase = 'CREATE DATABASE poodle';

const createUsersTable = `
CREATE TABLE IF NOT EXISTS poodle.users (
  id VARCHAR(36) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(128) NOT NULL,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  is_teacher TINYINT(1) NOT NULL DEFAULT 0,
  created_on DATETIME NOT NULL,
  is_deleted TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (id),
  UNIQUE INDEX email_UNIQUE (email ASC),
  UNIQUE INDEX id_UNIQUE (id ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createConversationsTable = `
CREATE TABLE IF NOT EXISTS poodle.conversations (
  id VARCHAR(36) NOT NULL,
  admin_id VARCHAR(36) NOT NULL,
  is_deleted TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX fk_conversations_users1_idx (admin_id ASC),
  CONSTRAINT fk_conversations_users1
    FOREIGN KEY (admin_id)
    REFERENCES poodle.users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createConversationUsersTable = `
CREATE TABLE IF NOT EXISTS poodle.conversation_users (
  conversation_id VARCHAR(36) NOT NULL,
  user_id VARCHAR(36) NOT NULL,
  is_deleted TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (conversation_id, user_id),
  INDEX fk_conversation_users_conversations_idx (conversation_id ASC),
  INDEX fk_conversation_users_users1_idx (user_id ASC),
  CONSTRAINT fk_conversation_users_conversations
    FOREIGN KEY (conversation_id)
    REFERENCES poodle.conversations (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_conversation_users_users1
    FOREIGN KEY (user_id)
    REFERENCES poodle.users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createCoursesTable = `
CREATE TABLE IF NOT EXISTS poodle.courses (
  id VARCHAR(36) NOT NULL,
  title VARCHAR(45) NOT NULL,
  description TEXT NOT NULL,
  is_private TINYINT(1) NOT NULL,
  created_on DATETIME NOT NULL,
  last_modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  is_deleted TINYINT(1) NULL DEFAULT 0,
  image VARCHAR(45) NULL DEFAULT NULL,
  creator_id VARCHAR(36) NOT NULL,
  has_quiz TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (id),
  UNIQUE INDEX title_UNIQUE (title ASC),
  INDEX fk_courses_users1_idx (creator_id ASC),
  CONSTRAINT fk_courses_users1
    FOREIGN KEY (creator_id)
    REFERENCES poodle.users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createCourseSummaryBullets = `
CREATE TABLE IF NOT EXISTS poodle.course_summary_bullets (
  id VARCHAR(36) NOT NULL,
  bullet TEXT NOT NULL,
  course_id VARCHAR(36) NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_course_summary_bullets_courses_idx (course_id ASC),
  CONSTRAINT fk_course_summary_bullets_courses
    FOREIGN KEY (course_id)
    REFERENCES poodle.courses (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createEnrolledUsersTable = `
CREATE TABLE IF NOT EXISTS poodle.enrolled_users (
  user_id VARCHAR(36) NOT NULL,
  course_id VARCHAR(36) NOT NULL,
  PRIMARY KEY (user_id, course_id),
  INDEX fk_users_has_courses_courses1_idx (course_id ASC),
  INDEX fk_users_has_courses_users1_idx (user_id ASC),
  CONSTRAINT fk_users_has_courses_courses1
    FOREIGN KEY (course_id)
    REFERENCES poodle.courses (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_users_has_courses_users1
    FOREIGN KEY (user_id)
    REFERENCES poodle.users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createMessagesTable = `
CREATE TABLE IF NOT EXISTS poodle.messages (
  id VARCHAR(36) NOT NULL,
  conversation_id VARCHAR(36) NOT NULL,
  user_id VARCHAR(36) NOT NULL,
  message TEXT NOT NULL,
  created_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  is_deleted TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  INDEX fk_messages_conversation_users1_idx (conversation_id ASC, user_id ASC),
  CONSTRAINT fk_messages_conversation_users1
    FOREIGN KEY (conversation_id , user_id)
    REFERENCES poodle.conversation_users (conversation_id , user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;
`;

const createQuizesTable = `
CREATE TABLE IF NOT EXISTS poodle.quizes (
  id VARCHAR(36) NOT NULL,
  course_id VARCHAR(36) NOT NULL,
  total_points DECIMAL(5,2) NULL DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX fk_quizes_courses_idx (course_id ASC) VISIBLE,
  CONSTRAINT fk_quizes_courses
    FOREIGN KEY (course_id)
    REFERENCES poodle.courses (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;`;

const createQuizesQuestionsTable = `
CREATE TABLE IF NOT EXISTS poodle.quizes_questions (
  id VARCHAR(36) NOT NULL,
  content TEXT NOT NULL,
  points DECIMAL(5,2) NOT NULL,
  quiz_id VARCHAR(36) NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_quizes_questions_quizes1_idx (quiz_id ASC) VISIBLE,
  CONSTRAINT fk_quizes_questions_quizes1
    FOREIGN KEY (quiz_id)
    REFERENCES poodle.quizes (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;`;

const createQuizesAnswersTable = `
CREATE TABLE IF NOT EXISTS poodle.questions_answers (
  id VARCHAR(36) NOT NULL,
  answer TEXT NOT NULL,
  is_correct TINYINT(1) NULL,
  question_id VARCHAR(36) NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_questions_answers_quizes_questions1_idx (question_id ASC) VISIBLE,
  CONSTRAINT fk_questions_answers_quizes_questions1
    FOREIGN KEY (question_id)
    REFERENCES poodle.quizes_questions (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
`;

const createQuizesUsersTable = `
CREATE TABLE IF NOT EXISTS poodle.quizez_users (
  quizes_id VARCHAR(36) NOT NULL,
  users_id VARCHAR(36) NOT NULL,
  points DECIMAL(5,2) NULL,
  PRIMARY KEY (quizes_id, users_id),
  INDEX fk_quizez_users_users1_idx (users_id ASC) VISIBLE,
  CONSTRAINT fk_quizez_users_quizes1
    FOREIGN KEY (quizes_id)
    REFERENCES poodle.quizes (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_quizez_users_users1
    FOREIGN KEY (users_id)
    REFERENCES poodle.users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
`;

const createSectionsTable = `
CREATE TABLE IF NOT EXISTS poodle.sections (
  id VARCHAR(36) NOT NULL,
  title VARCHAR(128) NOT NULL,
  content TEXT NOT NULL,
  section_order TINYINT(4) NOT NULL,
  restriction_date DATETIME NULL DEFAULT NULL,
  is_deleted TINYINT(1) NOT NULL DEFAULT 0,
  is_restricted TINYINT(1) NOT NULL DEFAULT 0,
  course_id VARCHAR(36) NOT NULL,
  restricted_by VARCHAR(5) NOT NULL DEFAULT 'date',
  PRIMARY KEY (id),
  INDEX fk_sections_courses1_idx (course_id ASC),
  CONSTRAINT fk_sections_courses1
    FOREIGN KEY (course_id)
    REFERENCES poodle.courses (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
`;

const createRestrictedSectionsUsersTable = `
CREATE TABLE IF NOT EXISTS poodle.restricted_sections_users (
  user_id VARCHAR(36) NOT NULL,
  section_id VARCHAR(36) NOT NULL,
  PRIMARY KEY (user_id, section_id),
  INDEX fk_users_has_sections_sections1_idx (section_id ASC),
  INDEX fk_users_has_sections_users_idx (user_id ASC),
  CONSTRAINT fk_users_has_sections_sections1
    FOREIGN KEY (section_id)
    REFERENCES poodle.sections (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_users_has_sections_users
    FOREIGN KEY (user_id)
    REFERENCES poodle.users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;`;

export default {
	createPoodleDatabase,
	createUsersTable,
	createConversationsTable,
	createConversationUsersTable,
	createCoursesTable,
	createCourseSummaryBullets,
	createEnrolledUsersTable,
	createMessagesTable,
	createQuizesTable,
	createQuizesQuestionsTable,
	createQuizesAnswersTable,
	createQuizesUsersTable,
	createSectionsTable,
	createRestrictedSectionsUsersTable,
};
