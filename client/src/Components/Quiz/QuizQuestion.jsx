import {
	Card,
	CardContent,
	Checkbox,
	FormControlLabel,
	Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
	root: {
		minWidth: 275,
		marginBottom: '20px',
		padding: '10px',
	},
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)',
	},
	title: {
		fontSize: 14,
	},
	pos: {
		marginBottom: 12,
	},
	question: {
		fontSize: '25px',
		fontWeight: 'bold',
		marginBottom: '12px',
	},
	label: {
		width: '100%',
		fontSize: '20px',
		height: '50px',
	},
	checkBox: {
		color: 'grey',
	},
});

const QuizQuestion = ({ question, setSelectedAnswers }) => {
	const classes = useStyles();

	const updateSelectedAnswers = (e, answer, questionId) => {
		if (e.target.checked) {
			setSelectedAnswers((selectedAnswers) => [
				...selectedAnswers,
				{ ...answer, questionId },
			]);
		} else {
			setSelectedAnswers((selectedAnswers) =>
				selectedAnswers.filter((x) => x.id !== answer.id)
			);
		}
	};

	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography className={classes.question}>
					{question.question.content}
				</Typography>

				{question.answers.map((answer) => (
					<FormControlLabel
						key={answer.id}
						className={classes.label}
						control={<Checkbox color="default" className={classes.checkBox} />}
						label={<p className={classes.answers}>{answer.answer}</p>}
						onClick={(e) =>
							updateSelectedAnswers(e, answer, question.question.id)
						}
					></FormControlLabel>
				))}
			</CardContent>
		</Card>
	);
};

export default QuizQuestion;
