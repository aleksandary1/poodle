import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
	root: {
		fontFamily: 'Lato',
		height: '100vh',
		margin: 'auto',
		alignContent: 'center',
	},
	image: {
		height: '80vh',
		backgroundImage:
			'url(https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1052&q=80)',
		opacity: '90%',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
		boxShadow:
			'0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);',
	},
	textBox: {
		textTransform: 'uppercase',
		letterSpacing: '0.8px',
		color: 'black',
		position: 'relative',
		width: '87%',
		top: '10%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		backgroundAttachment: 'fixed',
	},
	paper: {
		margin: theme.spacing(9, 5),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(3),
	},
	textfield: {
		marginBottom: '5px',
	},
	link: {
		textDecoration: 'none',
		color: 'blue',
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));
