import { Box, CardMedia, TextField } from '@material-ui/core';
import { useEffect } from 'react';
import { useState } from 'react';
import coursePicture from '../../../../Media/Course.jpg';
import CheckIcon from '@material-ui/icons/Check';
import { useStyles } from './CreateEditPreviewStep.styles';

const CreateEditPreviewStep = ({ basicInfo, image, bullets }) => {
	const [preview, setPreview] = useState({});

	const classes = useStyles();

	
	useEffect(() => {
		const render = () => {
			const reader = new FileReader();
			reader.onload = () => {
				if (reader.readyState === 2) {
					setPreview(reader.result);
				}
			};
			reader.readAsDataURL(image);
		};

		if (typeof image === 'string' && image.includes('static')) {
			setPreview(image);
		} else {
			image ? render() : setPreview(coursePicture);
		}
	}, [image]);
	
	return (
		<Box className={classes.root}>
			<h2 style={{ marginTop: 0 }}>{basicInfo.title}</h2>
			<Box
				style={{
					display: 'flex',
					width: '100%',
					justifyContent: 'space-between',
				}}
			>
				<Box
					style={{
						display: 'flex',
						justifyContent: 'start',
						width: '400px',
						height: '225px',
					}}
				>
					<CardMedia
						style={{
							width: '100%',
							paddingTop: '38%', // 16:9
							borderRadius: '20px',
							boxShadow: '0px 5px 8px -3px rgba(0,0,0,0.99)',
						}}
						title="Course"
						image={preview}
					></CardMedia>
				</Box>
				<Box>
					<TextField
						style={{ width: '400px' }}
						rows={8}
						multiline={true}
						value={basicInfo.description}
						variant="outlined"
						label="Description"
					/>
				</Box>
			</Box>

			<Box
				style={{
					marginTop: '20px',
					display: 'flex',
					width: '100%',
					justifyContent: 'center',
				}}
			>
				<Box
					style={{
						width: '400px',
						overflowY: 'scroll',
						maxHeight: '200px',
						wordWrap: 'break-word',
					}}
				>
					{bullets.length > 0 ? (
						<>
							{bullets.map((bullet, index) => (
								<Box
									key={index}
									style={{
										display: 'flex',
									}}
								>
									<CheckIcon
										style={{
											fontSize: '20px',
											justifyContent: 'center',
											marginTop: '10px',
										}}
									/>
									<p
										style={{
											display: 'inline',
											fontSize: '17px',
											margin: '12px 0 0 12px',
											lineHeight: '18px',
										}}
									>
										{bullet.bullet}
									</p>
								</Box>
							))}
						</>
					) : null}
				</Box>
			</Box>
		</Box>
	);
};

export default CreateEditPreviewStep;
