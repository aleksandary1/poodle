import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
	root: {
		fontFamily: 'Lato',
		height: '100vh',
		width: '60%',
		margin: 'auto',
		alignContent: 'center',
	},
	image: {
		height: '80vh',
		backgroundImage:
			'url(https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80)',
		backgroundSize: 'cover',
		backgroundPosition: 'center',
		boxShadow:
			'0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);',
	},
	paper: {
		margin: theme.spacing(9, 5),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(3),
	},
	textfield: {
		marginBottom: '10px',
	},
	link: {
		textDecoration: 'none',
		color: 'blue',
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));
