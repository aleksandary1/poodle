import express from 'express';
import registerValidator from '../validators/register.validator.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import { createToken } from '../auth/create_token.js';
import usersService from '../services/users.service.js';
import loginValidator from '../validators/login.validator.js';

/**
 * Operations for user entity on /users route
 * @author Kalina Georgieva <georgieva.kalina27@gmail.com>
 */
const router = express.Router();

// GET all users
router.get('', authMiddleware, async (req, res) => {
	const { role } = req.query;

	try {
		if (role) {
			let isTeacher = null;
	
			if (role === 'students') {
				isTeacher = 0;
			}
	
			const users = await usersService.getAllByRole(isTeacher);
			return res.status(200).json(users);
		}
	
		const users = await usersService.getAll();
		return res.status(200).json({ status: 'success', result: users });
	} catch (err) {
		res.status(500).send(err);
	}
});

// GET user by id
router.get('/:id', authMiddleware, async (req, res) => {
	const { id } = req.params;

	try {
		const user = await usersService.getById(id);

		if (!user) {
			return res.status(404).json({
				error: `User with id ${id} does not exist!`,
			});
		}
	
		res.status(200).json({ status: 'success', result: user });
	} catch (err) {
		res.status(500).send(err);
	}
});

// POST create user
router.post('/register', registerValidator, async (req, res) => {
	const user = req.body;

	try {
		const emailExists = await usersService.getBy('email', user.email);

		if (emailExists) {
			return res
				.status(409)
				.json({ error: 'Email is already attached to an account' });
		}
	
		const createdUser = await usersService.create(user);
		res.status(200).json(createdUser);
	} catch (err) {
		res.status(500).send(err);
	}
});

// POST login user
router.post('/login', loginValidator, async (req, res) => {
	const { email, password } = req.body;

	try {
		const user = await usersService.validateUser(email, password);

		const payload = {
			id: user.id,
			email: user.email,
			isTeacher: user.isTeacher,
			firstName: user.firstName,
			lastName: user.lastName,
		};

		const token = createToken(payload);
		res.status(200).json({ token: token });
	} catch (error) {
		res.status(401).json({ error: error });
	}
});

// POST update user
router.post('/:id', authMiddleware, async (req, res) => {
	const { id } = req.params;
	const updatedUserData = req.body;

	try {
		const userExists = await usersService.getBy('id', id);

		if (!userExists) {
			return res.status(404).json({
				error: `User with id ${id} does not exist!`,
			});
		}
	
		const updatedUser = await usersService.update(updatedUserData);
		res.status(200).json(updatedUser);
	} catch (err) {
		res.status(500).send(err);
	}
});

// DELETE delete user
router.delete('/:id', authMiddleware, async (req, res) => {
	const { id } = req.params;

	try {
	const userExists = await usersService.getBy('id', id);

	if (!userExists) {
		return res.status(404).json({
			error: `User with id ${id} does not exist!`,
		});
	}

	const deletedUser = await usersService.remove(id);
	res.status(200).json(deletedUser);
	} catch (err) {
		res.status(500).send(err);
	}

});

export default router;
