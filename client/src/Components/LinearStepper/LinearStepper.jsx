import { useState } from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './linearStepper.styles';

// mode should be Edit or Create
const getSteps = (mode = 'Create') => {
	return [
		`${mode} main overview`,
		'Upload landing picture',
		'Describe what to learn',
	];
};

const LinearStepper = ({ mode, changeStep, modalStep, complete }) => {
	const [activeStep, setActiveStep] = useState(modalStep);
	const [skipped, setSkipped] = useState(new Set());
	const steps = getSteps(mode);

	const isStepOptional = (step) => {
		return step === 1;
	};

	const isStepSkipped = (step) => {
		return skipped.has(step);
	};

	const handleNext = () => {
		let newSkipped = skipped;

		if (isStepSkipped(activeStep)) {
			newSkipped = new Set(newSkipped.values());
			newSkipped.delete(activeStep);
		}

		setActiveStep((prevActiveStep) => prevActiveStep + 1);
		setSkipped(newSkipped);

		changeStep(activeStep + 1);
	};

	const handleBack = () => {
		setActiveStep((prevActiveStep) => prevActiveStep - 1);
		changeStep(activeStep - 1);
	};

	const handleSkip = () => {
		if (!isStepOptional(activeStep)) {
			throw new Error("You can't skip a step that isn't optional.");
		}

		setActiveStep((prevActiveStep) => prevActiveStep + 1);
		setSkipped((prevSkipped) => {
			const newSkipped = new Set(prevSkipped.values());
			newSkipped.add(activeStep);
			return newSkipped;
		});

		changeStep(activeStep + 1);
	};

	const handleReset = () => {
		complete();
		setActiveStep(0);
		changeStep(activeStep);
	};

	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Stepper activeStep={activeStep}>
				{steps.map((label, index) => {
					const stepProps = {};
					const labelProps = {};
					if (isStepOptional(index)) {
						labelProps.optional = (
							<Typography variant="caption">Optional</Typography>
						);
					}
					if (isStepSkipped(index)) {
						stepProps.completed = false;
					}
					return (
						<Step key={label} {...stepProps}>
							<StepLabel {...labelProps}>{label}</StepLabel>
						</Step>
					);
				})}
			</Stepper>
			<div>
				{activeStep === steps.length ? (
					<div style={{ display: 'flex', justifyContent: 'center' }}>
						<Button onClick={handleReset} className={classes.button}>
							Complete
						</Button>
					</div>
				) : (
					<div>
						<div style={{ display: 'flex', justifyContent: 'center' }}>
							<Button
								disabled={activeStep === 0}
								onClick={handleBack}
								className={classes.button}
							>
								Back
							</Button>
							{isStepOptional(activeStep) && (
								<Button
									variant="contained"
									onClick={handleSkip}
									className={classes.button}
									style={{ background: '#673ab7', color: '#fff' }}
								>
									Skip
								</Button>
							)}

							<Button
								variant="contained"
								onClick={handleNext}
								className={classes.button}
								style={{ background: '#673ab7', color: '#fff' }}
							>
								{activeStep === steps.length - 1 ? 'Finish' : 'Next'}
							</Button>
						</div>
					</div>
				)}
			</div>
		</div>
	);
};

export default LinearStepper;
