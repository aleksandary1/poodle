import { useContext, useState } from 'react';
import { useHistory, Link } from 'react-router-dom';
import AuthContext from '../../providers/auth.provider';
import { useStyles } from './login.style';
import usersRequest from '../../Requests/users.requests';
import getUserFromToken from '../../utils/auth';
import {
	Button,
	CssBaseline,
	TextField,
	Paper,
	Grid,
	Typography,
} from '@material-ui/core';

const LoginPage = () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [invalidEmail, setInvalidEmail] = useState('');
	const [invalidPassword, setInvalidPassword] = useState('');

	let testInvalidEmail = '';
	let testInvalidPassword = '';

	const { setUser } = useContext(AuthContext);
	const history = useHistory();

	const onSubmit = (e) => {
		e.preventDefault();
		testInvalidEmail = false;
		testInvalidPassword = false;

		const emailRegex = new RegExp(
			'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$'
		);

		if (!email) {
			testInvalidEmail = true;

			return setInvalidEmail('Provide email');
		}

		if (!emailRegex.test(email)) {
			testInvalidEmail = true;

			return setInvalidEmail('Invalid email');
		}

		if (!password) {
			testInvalidPassword = true;

			return setInvalidPassword('Provide password');
		}

		const passwordRegex = new RegExp('^(?=.*)(?=.*[a-z])(?=.*[A-Z]).{6,20}$');

		if (!passwordRegex.test(password)) {
			testInvalidPassword = true;

			return setInvalidPassword(
				'Password should be between 6 and 20 characters, should contain at least 1 uppercase character and 1 number'
			);
		}

		if (!testInvalidEmail && !testInvalidPassword) {
			usersRequest.login({ email, password }).then((data) => {
				if (data.error) {
					const errMsg = data.error;

					if (errMsg.includes('email')) {
						return setInvalidEmail(data.error);
					}

					setPassword('');
					return setInvalidPassword(data.error);
				}

				setUser(getUserFromToken(data.token));
				localStorage.setItem('token', data.token);
				history.push('/');
			});
		}
	};

	const classes = useStyles();

	return (
		<Grid container component="main" className={classes.root}>
			<CssBaseline />
			<Grid item xs={false} sm={4} md={7} className={classes.image} />
			<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
				<div className={classes.paper}>
					<Typography component="h1" variant="h5">
						Sign In
					</Typography>
					<form className={classes.form} onSubmit={onSubmit}>
						<Grid container spacing={2}>
							<TextField
								className={classes.textfield}
								variant="outlined"
								margin="normal"
								fullWidth
								id="email"
								label="Email Address"
								name="email"
								autoComplete="email"
								autoFocus
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							{invalidEmail && <p style={{ color: 'red' }}>{invalidEmail}</p>}
							<TextField
								className={classes.textfield}
								variant="outlined"
								margin="normal"
								fullWidth
								name="password"
								label="Password"
								type="password"
								id="password"
								autoComplete="current-password"
								value={password}
								onChange={(e) => setPassword(e.target.value)}
							/>
							{invalidPassword && (
								<p style={{ color: 'red' }}>{invalidPassword}</p>
							)}
							<Button
								type="submit"
								fullWidth
								variant="contained"
								color="primary"
								className={classes.submit}
							>
								Sign In
							</Button>
							<Grid container>
								<Grid item>
									<Link to={'/register'} className={classes.link}>
										{"Don't have an account? Sign Up"}
									</Link>
								</Grid>
							</Grid>
						</Grid>
					</form>
				</div>
			</Grid>
		</Grid>
	);
};

export default LoginPage;
