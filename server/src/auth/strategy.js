import passportJwt from 'passport-jwt';
import { SECRET_KEY } from '../common/config.js';

const options = {
	secretOrKey: SECRET_KEY,
	jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
	const user = {
		id: payload.id,
		email: payload.email,
		firstName: payload.firstName,
		lastName: payload.lastName,
		isTeacher: payload.isTeacher,
	};

	done(null, user);
});

export { jwtStrategy };
