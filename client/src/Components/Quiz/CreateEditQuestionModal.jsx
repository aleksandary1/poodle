import { useState } from 'react';
import {
	Box,
	Modal,
	Backdrop,
	Fade,
	Grid,
	TextField,
	FormControlLabel,
	Checkbox,
	Button,
	IconButton,
} from '@material-ui/core';
import { Add, Clear } from '@material-ui/icons';
import { useStyles } from './CreateEditQuestionModal.styles';

const CreateEditQuestionModal = ({
	id,
	questionToEdit,
	updateQuestionInQuizView,
	isOpenModal,
	toggleModal,
	questionNumber,
	addQuestion,
}) => {
	const [question, setQuestion] = useState({
		id: questionToEdit ? questionToEdit.question.id : id,
		content: questionToEdit ? questionToEdit.question.content : '',
		points: questionToEdit ? questionToEdit.question.points : '',
	});

	const [answers, setAnswers] = useState(
		questionToEdit
			? questionToEdit.answers
			: [
					{ answer: '', isCorrect: false },
					{ answer: '', isCorrect: false },
					{ answer: '', isCorrect: false },
			  ]
	);

	const updateQuestion = (prop, value) => {
		question[prop] = value;
		setQuestion({ ...question });
	};

	const handleCheckboxChange = (index, e) => {
		setAnswers(
			answers.map((obj, currentIndex) => {
				if (index === currentIndex) {
					return { ...obj, isCorrect: e.target.checked };
				}

				return obj;
			})
		);
	};

	const updateAnswerText = (index, answer) => {
		setAnswers(
			answers.map((obj, currentIndex) => {
				if (index === currentIndex) {
					return { ...obj, answer };
				}

				return obj;
			})
		);
	};

	const removeAnswer = (indexToRemove) => {
		const newAnswers = answers.filter((_, index) => index !== indexToRemove);
		setAnswers(newAnswers);
	};

	const classes = useStyles();

	return (
		<Modal
			open={isOpenModal}
			onClose={toggleModal}
			closeAfterTransition
			disableBackdropClick={true}
			disableEscapeKeyDown={true}
			BackdropComponent={Backdrop}
			BackdropProps={{
				timeout: 500,
			}}
			onKeyDown={(e) => {
				if (e.key === 'Escape') {
					toggleModal();
				}
			}}
		>
			<Fade in={isOpenModal}>
				<Box className={classes.modal}>
					<Box style={{ display: 'flex', justifyContent: 'space-between' }}>
						<h2 style={{ marginTop: '5px' }}>Create New Question</h2>
						<Button onClick={toggleModal}>
							<Clear />
						</Button>
					</Box>

					<Grid
						conainer
						spacing={4}
						style={{
							width: '100%',
							display: 'flex',
							justifyContent: 'space-between',
						}}
					>
						<Grid item style={{ width: '88%' }}>
							<TextField
								className={classes.outlinedTextField}
								multiline={true}
								rows={4}
								variant="outlined"
								label={`Question № ${questionNumber}`}
								value={question.content}
								onChange={(e) => updateQuestion('content', e.target.value)}
							/>
						</Grid>

						<Grid item style={{ width: '10%', marginTop: 'auto' }}>
							<TextField
								className={classes.outlinedTextField}
								multiline={true}
								rows={1}
								variant="outlined"
								label={`Points`}
								value={question.points}
								onChange={(e) =>
									updateQuestion('points', Number(e.target.value))
								}
							/>
						</Grid>
					</Grid>

					<Box className={answers.length > 3 ? classes.answersConteiner : null}>
						{answers.map((answer, index) => (
							<FormControlLabel
								key={index}
								className={classes.label}
								control={
									<Checkbox
										color={''}
										className={classes.checkBox}
										checked={answer.isCorrect}
										onChange={(e) => handleCheckboxChange(index, e)}
									/>
								}
								label={
									<>
										<TextField
											className={classes.outlinedTextField}
											multiline={true}
											rows={2}
											variant="outlined"
											label={`Answer №${index + 1}`}
											value={answer.answer}
											onChange={(e) => updateAnswerText(index, e.target.value)}
										></TextField>
										<Button
											className={classes.clearBtn}
											onClick={() => removeAnswer(index)}
										>
											<Clear />
										</Button>
									</>
								}
							/>
						))}
					</Box>

					<IconButton
						onClick={() =>
							setAnswers([...answers, { answer: '', isCorrect: false }])
						}
					>
						<Add />
					</IconButton>

					<Button
						className={classes.btn}
						onClick={() => {
							if (questionToEdit) {
								updateQuestionInQuizView({ question, answers });
							} else {
								addQuestion({
									question,
									answers: answers.filter((answer) => answer.answer),
								});
							}
						}}
					>
						{' '}
						Save{' '}
					</Button>
				</Box>
			</Fade>
		</Modal>
	);
};

export default CreateEditQuestionModal;
