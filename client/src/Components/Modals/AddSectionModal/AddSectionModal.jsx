import { Box, Button, Modal, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-html';
import 'ace-builds/src-noconflict/theme-monokai';
import { useStyles } from './addSectionModal.styles';
import sectionsRequests from '../../../Requests/sections.requests';

const AddSectionModal = ({
	addModal,
	setAddModal,
	order,
	courseId,
	setSections,
}) => {
	const classes = useStyles();
	const [sectionTitle, setSectionTitle] = useState('');
	const [sectionContent, setSectionContent] = useState('');
	const [sectionDate, setSectionDate] = useState('2021-09-01T10:30');

	const addSection = async () => {
		const sectionData = {
			title: sectionTitle,
			content: sectionContent,
			order: order + 1,
			restriction_date: sectionDate,
		};

		await sectionsRequests.add(courseId, sectionData);
		await sectionsRequests.getAllByCourseId(courseId).then((data) => {
			setAddModal(false);
			setSections(data.result);
			setSectionTitle('');
			setSectionContent('');
			setSectionDate('2021-09-01T10:30');
		});
	};

	return (
		<Modal open={addModal} onClose={() => setAddModal(false)}>
			<Box className={classes.addModal}>
				<Box style={{ marginLeft: '20px' }}>
					<h2>Create New Section</h2>
					<TextField
						label="Title"
						value={sectionTitle}
						onChange={(e) => setSectionTitle(e.target.value)}
					/>
					<p style={{ marginBottom: '2px', marginTop: '5px' }}>Content:</p>
					<AceEditor
						placeholder="Write your section HTML here!"
						mode="html"
						theme="tomorrow"
						fontSize={17}
						width="650px"
						height="400px"
						showGutter={true}
						onChange={setSectionContent}
						value={sectionContent}
						setOptions={{
							useWorker: false,
						}}
					/>
					<Box style={{display: 'flex', justifyContent: 'flex-end', marginRight: 10}}>
						<Button
							className={classes.cancelButton}
							onClick={() => setAddModal(false)}
						>
							Cancel
						</Button>
						<Button className={classes.addButton} onClick={() => addSection()}>
							Add
						</Button>
					</Box>
				</Box>
			</Box>
		</Modal>
	);
};

export default AddSectionModal;
