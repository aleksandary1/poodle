/* eslint-disable import/no-anonymous-default-export */
import { BASE_URL } from '../common/constants';
import { createRequest } from './requests.helper';

const getAllByUserId = (userId) => {
  const request = createRequest('GET');

  return fetch(`${BASE_URL}/conversations?userId=${userId}`, request)
  .then((res) => res.json());
}

const getFriend = (conversationId) => {
  const request = createRequest('GET');

  return fetch(`${BASE_URL}/conversations/${conversationId}`, request)
  .then((res) => res.json());
}

const checkConversation = (userOne, userTwo) => {
  const request = createRequest('POST', {
    body: JSON.stringify({userOne, userTwo})
  });

  return fetch(`${BASE_URL}/conversations/check`, request)
  .then((res) => res.json());
}

const create = (creatorId, recieverId) => {
  const request = createRequest('POST', {
    body: JSON.stringify({creatorId, recieverId})
  })

  return fetch(`${BASE_URL}/conversations`, request)
  .then((res) => res.json());
}

export default {
  getAllByUserId,
  getFriend,
  checkConversation,
  create
}