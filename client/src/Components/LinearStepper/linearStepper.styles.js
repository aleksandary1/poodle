import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		marginTop: '20px',
	},
	button: {
		marginRight: theme.spacing(1),
	},
	instructions: {
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(1),
	},
	step: {
		'& $completed': {
			color: 'lightgreen',
		},
		'& $active': {
			color: 'pink',
		},
		'& $disabled': {
			color: 'red',
		},
	},
	active: {}, //needed so that the &$active tag works
	completed: {},
	disabled: {},
}));
