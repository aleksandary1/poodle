import { useContext } from 'react';
import AuthContext from '../../../providers/auth.provider';
import { Typography, Button, Box } from '@material-ui/core';
import { useStyles } from './courseOverview.styles';
import { convertDate } from '../../../common/dateFormatter';
import CheckIcon from '@material-ui/icons/Check';

const CourseOverview = ({
	course,
	enrollStudent,
	isUserEnrolled,
	unenrollStudent,
	onUnenroll,
	toggleModal,
}) => {
	const { user } = useContext(AuthContext);

	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Typography variant="h1" className={classes.h1}>
				{course.title}
			</Typography>
			<div style={{ display: 'flex', justifyContent: ' space-between' }}>
				<Typography variant="subtitle1" className={classes.subtitle}>
					{`${course.authorFirstName} ${course.authorLastName}`}
				</Typography>
				<Typography variant="subtitle1" className={classes.subtitle}>
					{`${course.sectionsCount} lessons`}
				</Typography>
			</div>
			<Typography
				variant="subtitle1"
				className={classes.subtitle}
				style={{ color: '#1e6055', fontWeight: 'bold', marginBottom: '10px' }}
			>
				Updated {convertDate(course.lastModified)}
			</Typography>
			<Typography style={{ lineHeight: '22px' }}>
				{course.description}
			</Typography>
			{course.bullets.length > 0 ? (
				<>
					{course.bullets.map((bullet) => (
						<Box key={bullet.id} className={classes.bulletWrapper}>
							<CheckIcon className={classes.icon} />
							<p className={classes.p}>{bullet.bullet}</p>
						</Box>
					))}{' '}
				</>
			) : null}

			{user && user.isTeacher && window.location.pathname === '/myCourses' ? (
				<Button
					variant="contained"
					onClick={(e) => {
						e.preventDefault();
						toggleModal(e, course.id);
					}}
					className={classes.btn}
					color="secondary"
				>
					Edit
				</Button>
			) : null}

			{user && !user.isTeacher ? (
				<>
					{isUserEnrolled ? (
						<Button
							variant="contained"
							onClick={(e) => {
								unenrollStudent(e);
								onUnenroll(course.id);
							}}
							className={classes.btn}
							color="secondary"
						>
							Unenroll
						</Button>
					) : (
						<Button
							variant="contained"
							onClick={(e) => enrollStudent(e)}
							className={classes.btn}
							color="primary"
						>
							Enroll now
						</Button>
					)}
				</>
			) : null}
		</div>
	);
};

export default CourseOverview;
