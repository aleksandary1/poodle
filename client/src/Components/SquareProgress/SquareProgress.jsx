import { Box, makeStyles, Paper } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(() => ({
	root: {
		width: '230px',
		marginRight: '20px',
	},
	paper: {
		height: '200px',
		borderRadius: '30px',
		position: 'relative',
	},
	item: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		marginLeft: '30px',
	},
	number: {
		fontWeight: 'bold',
		fontSize: '40pt',
		marginBottom: 0,
	},
	text: {
		marginBottom: '30px',
		marginTop: 0,
	},
}));

const SquareProgress = ({ number, text }) => {
	const classes = useStyles();
	return (
		<Box className={classes.root}>
			<Paper className={classes.paper} elevation={5}>
				<Box className={classes.item}>
					<p className={classes.number}>{number}</p>
					<p className={classes.text}>{text}</p>
				</Box>
			</Paper>
		</Box>
	);
};

export default SquareProgress;
