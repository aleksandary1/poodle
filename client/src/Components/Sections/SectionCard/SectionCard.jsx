import {
	Box,
	Button,
	Card,
	Collapse,
	FormControlLabel,
	IconButton,
	Switch,
	TextField,
	Typography,
} from '@material-ui/core';
import { Edit, ExpandLess, Lock } from '@material-ui/icons';
import Select from 'react-select';
import ExpandMore from '@material-ui/icons/ExpandMore';
import React, { useState } from 'react';
import AceEditor from 'react-ace';
import { useStyles } from './sectionCard.style';
import 'ace-builds/src-noconflict/mode-html';
import 'ace-builds/src-noconflict/theme-tomorrow';
import sectionsRequests from '../../../Requests/sections.requests';
import { useEffect } from 'react';
import { useContext } from 'react';
import AuthContext from '../../../providers/auth.provider';

const SectionCard = ({
	sectionId,
	title,
	content,
	courseId,
	enrolledStudents,
	isRestricted,
	canEdit,
	restrictedBy
}) => {
	const classes = useStyles();
	const [showContent, setShowContent] = useState(false);
	const [editedTitle, setEditedTitle] = useState(title);
	const [editedContent, setEditedContent] = useState(content);
	const [restrictionDate, setRestrictionDate] = useState('2021-08-19T10:30');
	const [editMode, setEditMode] = useState(false);
	const [restrict, setRestrict] = useState(isRestricted ? true : false);
	const [selectedRestrictMode, setSelectedRestrictMode] = useState(restrictedBy);
	const [restrictedStudents, setRestrictedStudents] = useState([]);
	const { user } = useContext(AuthContext)

	useEffect(() => {
		sectionsRequests.getAllRestricted(sectionId).then((data) => {
			const result = data.result.map((r) => ({
				value: r.userId,
				label: `${r.firstName} ${r.lastName} (${r.email})`,
			}));
			setRestrictedStudents(result);
		});
	}, [sectionId]);

	const updateShowContent = (e, boolean) => {
		e.stopPropagation();
		setShowContent(boolean);
	};

	const updateEditMode = (e, boolean) => {
		e.stopPropagation();
		setEditMode(boolean);
	};

	const updateSection = async (e) => {
		e.preventDefault();

		const data = {
			title: editedTitle,
			content: editedContent,
			restriction_date: restrictionDate,
		};

		await sectionsRequests.updateById(courseId, sectionId, data).then(() => {
			setEditMode(false);
		});
	};

	const cancelEdit = (e) => {
		e.preventDefault();

		setEditedTitle(title);
		setEditedContent(content);
		setRestrictionDate('2021-08-19T10:30');
		setEditMode(false);
	};

	const updateRestrict = async (boolean) => {
		await sectionsRequests.restrictSection(sectionId, boolean ? 1 : 0)
		.then(() => setRestrict(boolean))
	}

	const restrictByOptions = [
		{ value: 'date', label: 'By Date' },
		{ value: 'users', label: 'By Users' },
	];

	const handleRestrictionSelect = async (e) => {
		if (e.action === 'select-option') {
			await sectionsRequests
				.restrictUser(sectionId, e.option.value)
				.then(() => setRestrictedStudents([...restrictedStudents, e.option]));
		} else if (e.action === 'remove-value') {
			await sectionsRequests
				.removeRestriction(sectionId, e.removedValue.value)
				.then(() =>
					setRestrictedStudents(
						restrictedStudents.filter((r) => r.value !== e.removedValue.value)
					)
				);
		} else if (e.action === 'clear') {
			await sectionsRequests
				.clearAllRestrictions(sectionId)
				.then(() => setRestrictedStudents([]));
		}
	};

	const RestrictModes = () => {
		if (selectedRestrictMode === 'date') {
			return (
				<Box style={{ marginTop: 15 }}>
					<TextField
						label="Restriction Date"
						type="datetime-local"
						defaultValue={restrictionDate}
						onChange={(e) => setRestrictionDate(e.target.value)}
						className={classes.textField}
						InputLabelProps={{
							shrink: true,
						}}
					/>
				</Box>
			);
		} else if (selectedRestrictMode === 'users') {
			return (
				<Box style={{ marginTop: 15, marginBottom: 10 }}>
					<Select
						isMulti
						defaultValue={restrictedStudents}
						options={enrolledStudents}
						menuPortalTarget={document.body}
						styles={{
							container: () => ({ width: '500px' }),
							menuPortal: (base) => ({ ...base, zIndex: 9999 }),
						}}
						onChange={(value, action) => handleRestrictionSelect(action)}
					/>
				</Box>
			);
		}
	};

	const updateRestrictMode = (e) => {
		sectionsRequests.restrictBy(sectionId, {restrictBy: e.value})
		.then(() => {
			setSelectedRestrictMode(e.value);
		})
	}

	const restrictions = (
		<Box style={{ marginLeft: 15 }}>
			<Select
				defaultValue={restrictedBy === 'date' ? restrictByOptions[0] : restrictByOptions[1]}
				options={restrictByOptions}
				onChange={(e) => {
					updateRestrictMode(e)}}
				menuPortalTarget={document.body}
				styles={{
					container: () => ({ width: '200px' }),
					menuPortal: (base) => ({ ...base, zIndex: 9999 }),
				}}
			/>
			<RestrictModes />
		</Box>
	);

	const editor = (
		<Box>
			<AceEditor
				placeholder="Place your section HTML here!"
				mode="html"
				theme="tomorrow"
				fontSize={17}
				width="900px"
				showGutter={true}
				onChange={setEditedContent}
				value={editedContent}
				setOptions={{
					useWorker: false,
				}}
			/>
			<Box>
				<FormControlLabel
					control={<Switch checked={restrict} onChange={() => updateRestrict(!restrict)}/>}
					label="Restrict?"
					labelPlacement="start"
				/>
				<Collapse in={restrict} timeout="auto" unmountOnExit>
					{restrictions}
				</Collapse>
			</Box>
			<Box className={classes.buttonWrapper}>
				<Button className={classes.cancelButton} onClick={(e) => cancelEdit(e)}>
					Cancel
				</Button>
				<Button className={classes.addButton} onClick={(e) => updateSection(e)}>
					Save
				</Button>
			</Box>
		</Box>
	);

	return (
		<Box className={classes.root}>
			<Card className={classes.card}>
				<Box
					style={{ display: 'flex', justifyContent: 'space-between' }}
					onClick={(e) => updateShowContent(e, !showContent)}
				>
					<Box style={{display: 'flex'}}>
					{editMode ? (
						<TextField
							label="Title"
							value={editedTitle}
							fullWidth
							className={classes.editTitle}
							onChange={(e) => setEditedTitle(e.target.value)}
						/>
					) : restrict ? (
						<>
						<Typography className={classes.name}>{editedTitle}</Typography>
						{user.isTeacher ? <Lock style={{marginTop: 17, marginLeft: 10}}/>
						: null}
						</>
					) : <Typography className={classes.name}>{editedTitle}</Typography>}
					</Box>

					<Box>
					{editMode ? null 
					: canEdit ? (
						<IconButton
							className={classes.showButton}
							onClick={(e) => updateEditMode(e, !editMode)}
						>
							<Edit />
						</IconButton>
					) : null}
						<IconButton className={classes.showButton}>
							{showContent ? <ExpandLess /> : <ExpandMore />}
						</IconButton>
					</Box>
				</Box>
				<Box>
						{/* {isRestricted ? <p style={{margin: '0 0 0 10px', fontSize: 16}}><b>This section is restricted!</b></p> : null} */}
				</Box>
				<Collapse in={showContent} timeout="auto" unmountOnExit>
					{editMode ? (
						editor
					) : (
						<div style={{margin: 10}} dangerouslySetInnerHTML={{ __html: editedContent }} />
					)}
				</Collapse>
			</Card>
		</Box>
	);
};

export default SectionCard;
