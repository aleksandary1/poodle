import dotenv from 'dotenv';

dotenv.config();

const PORT = process.env.PORT;
const DB_HOST = process.env.DB_HOST;
const DB_PORT = process.env.DB_PORT;
const DB_USER = process.env.DB_USER;
const DB_PASS = process.env.DB_PASS;
const DATABASE = process.env.DATABASE;
const SECRET_KEY = process.env.SECRET_KEY;
const TOKEN_LIFETIME = process.env.TOKEN_LIFETIME;

export {
	PORT,
	DB_HOST,
	DB_PORT,
	DB_USER,
	DB_PASS,
	DATABASE,
	SECRET_KEY,
	TOKEN_LIFETIME,
};
