import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { PORT } from './common/config.js';
import passport from 'passport';
import { jwtStrategy } from './auth/strategy.js';
import usersRoute from './controllers/users.controller.js';
import coursesRouter from './controllers/courses.controller.js';
import conversationRouter from './controllers/conversation.controller.js';
import messagesRouter from './controllers/message.controller.js';
import restrictionRouter from './controllers/restriction.controller.js';

const app = express();

passport.use(jwtStrategy);

app.use(cors(), helmet(), express.json());
app.use(passport.initialize());
app.use(express.static('uploads'));
app.get('/', (req, res) => {
	res.status(200).send('Hello World!');
});

// Users Router
app.use('/users', usersRoute);
// Courses Router
app.use('/courses', coursesRouter);
// Conversations Router
app.use('/conversations', conversationRouter);
// Messages Router
app.use('/messages', messagesRouter);
// Restrictions Router
app.use('/restrictions', restrictionRouter);

app.listen(PORT, () => {
	console.log(`Listening on port ${PORT}`);
});
