import { v4 as uuid } from 'uuid';
import conversationsData from '../data/conversations.data.js';
import { normalizeEntityProperties } from './entity-mapper.service.js';

export const getConversationsBy = async (column, value) => {
  const conversationData = await conversationsData.getBy(column, value);

  return conversationData.map(c => normalizeEntityProperties(c));
};

export const getOtherUser = async (converastionId, userId) => {
  const conversationData = await conversationsData.getOther('conversation_id', converastionId, userId);

  return conversationData.map(c => normalizeEntityProperties(c));
};

export const checkConversation = async (userOne, userTwo) => {
  const result = await conversationsData.check(userOne, userTwo);

  return normalizeEntityProperties(result);
};

export const createConversation = async (creatorId, recieverId) => {
  const id = uuid();
  const members = [creatorId, recieverId].map(userId => `('${id}', '${userId}')`);

  await conversationsData.create(id, creatorId);
  await conversationsData.insertMembers(members.join(', '));

  const result = await getConversationsBy('conversation_id', id);

  return result[0];
};
