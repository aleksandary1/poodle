import { makeStyles } from '@material-ui/core';
import { deepPurple } from '@material-ui/core/colors';

export const useStyles = makeStyles(() => ({
	modal: {
		position: 'absolute',
		width: 900,
		height: '70%',
		left: '50%',
		top: '50%',
		transform: 'translate(-50%, -50%)',
		backgroundColor: 'white',
		borderRadius: '25px',
		boxShadow: '0px 30px 50px -3px rgba(0,0,0,0.3)',
		padding: '40px',
	},
	button: {
		display: 'flex',
		float: 'right',
		background: '#fff',
		color: '#7e57c2',
		borderRadius: '8px',
		boxShadow: '0px 8px 12px -3px rgba(0,0,0,0.5)',
		'&:hover': {
			background: '#9575cd',
			color: '#fff',
		},
	},
	outlinedTextField: {
		width: '100%',
		[`& fieldset`]: {
			borderRadius: '15px',
			borderColor: deepPurple[500],
		},
		'& label.Mui-focused': {
			color: deepPurple[500],
			borderColor: 'solid 2px',
		},
		'& .MuiOutlinedInput-root': {
			'& fieldset': {
				borderWidth: '1.5px',
				borderColor: deepPurple[500],
			},
			'&.Mui-focused fieldset': {
				borderColor: deepPurple[500],
			},
		},
	},
	invalidText: {
		color: 'red',
		margin: '10px 0',
	},
	root: {
		'& #invalidTitle': {
			position: 'absolute',
		},
	},
}));

export const customStylesSelect = {
	container: (provided) => ({
		...provided,
		width: '100%',
		display: 'inline-block',
		'&:focus': {
			border: '1px solid black',
		},
	}),
	valueContainer: (base) => ({
		...base,
		maxHeight: '120px',
		overflowY: 'scroll',
		overflowX: 'hidden',
	}),
	menuPortal: (base) => ({
		...base,
		zIndex: 9999,
		minHeight: '2px',
	}),
	control: (styles, state) => ({
		...styles,
		borderRadius: '12px',
		boxShadow: state.isFocused ? '0 0 0 0.3px #673ab7' : 0,
		borderColor: state.isFocused ? '#9575cd' : '#673ab7',
		'&:hover': {
			borderColor: state.isFocused ? '#9575cd' : '#673ab7',
		},
	}),
	input: (provided) => ({
		...provided,
		height: '40px',
	}),
	option: (styles, state) => ({
		...styles,
		backgroundColor: state.isFocused ? '#d1c4e9' : null,
	}),
};
