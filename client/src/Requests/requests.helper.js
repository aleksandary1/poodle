export const createRequest = (verb, request) => {
	return addInitHeaders({ ...request, method: verb });
};

const addInitHeaders = (req) => {
	const initHeaders = {};
	initHeaders['Content-Type'] = 'application/json';
	initHeaders['Authorization'] = `Bearer ${localStorage.getItem('token')}`;

	const headers = { ...initHeaders, ...(req.headers ? req.headers : {}) };

	return { ...req, headers };
};
