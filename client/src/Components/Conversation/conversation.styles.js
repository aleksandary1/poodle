import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    padding: 10,
    cursor: 'pointer',
    marginTop: 20,
    borderRadius: 20,
    '&:hover': {
      backgroundColor: 'rgb(245, 243, 243)'
    }
  },
  selected: {
    backgroundColor: 'rgb(245, 243, 243)',
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    marginRight: 20 
  }
}));