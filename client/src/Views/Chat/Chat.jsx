import { Box, Button, Grid, TextField } from '@material-ui/core';
import React, { useRef, useContext, useEffect, useState } from 'react'
import Select from 'react-select';
import Conversation from '../../Components/Conversation/Conversation';
import Message from '../../Components/Message/Message';
import AuthContext from '../../providers/auth.provider';
import { useSocket } from '../../providers/socket.provider';
import conversationsRequests from '../../Requests/conversations.requests';
import messagesRequest from '../../Requests/messages.request';
import usersRequests from '../../Requests/users.requests';
import MainView from '../MainView'
import { useStyles } from './chat.styles';

const Chat = () => {
  const [users, setUsers] = useState([]);
  const [conversations, setConversations] = useState([]);
  const [currentChat, setCurrentChat] = useState(null);
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState("");
  const [arrivalMessage, setArrivalMessage] = useState(null);
  const socket = useSocket()
  const { user } = useContext(AuthContext);
  const scrollRef = useRef();
  const classes = useStyles();

  useEffect(() => {
    usersRequests.getAll()
    .then(data => {
      const result = data.result.map(r => (
        { value: r.id, label: `${r.firstName} ${r.lastName} (${r.email})`}
        ));
      setUsers(result);
    })
  }, [])

  useEffect(() => {
    if (socket == null) return;

    socket.on("getMessage", (data) => {
      setArrivalMessage({
        conversationId: data.conversationId,
      });
    });
    socket.on("newConversation", (data) => {
      setConversations([data, ...conversations])
    })
  }, [socket, user, conversations])

  useEffect(() => {
    if (arrivalMessage 
        && currentChat.conversationId === arrivalMessage.conversationId) {
          messagesRequest.get(currentChat?.conversationId)
          .then(data => setMessages(data))
    }
  }, [arrivalMessage, currentChat])

  useEffect(() => {
    conversationsRequests.getAllByUserId(user.id)
    .then(data => {
      setConversations(data.result);
    });
  }, [user.id]);

  useEffect(() => {
    messagesRequest.get(currentChat?.conversationId)
    .then(data => setMessages(data))
  }, [currentChat])

  const sendMessage = async (e) => {
    e.preventDefault();
    const message = {
      userId: user.id,
      text: newMessage,
      conversationId: currentChat.conversationId
    }

    const friendId = await conversationsRequests.getFriend(currentChat.conversationId)
    .then(data => {
      return data.result[0].userId
    });

    socket.emit("sendMessage", {
      conversationId: currentChat.conversationId,
      receiverId: friendId
    });

    try {
      await messagesRequest.addMessage(message)
      .then(data => {
        setMessages([...messages, ...data]);
        setNewMessage('');
      });

    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: 'smooth'});
  }, [messages]);

  const chatOpen = (
    <>
    <Box className={classes.top}>
      {messages.map(m => (
        <Box key={m.id} ref={scrollRef}>
          <Message message={m} own={m.userId === user.id} />
        </Box>
      ))}
    </Box>
    <Box className={classes.bottom}>
      <TextField
        className={classes.chatInput}
        placeholder='your message...'
        value={newMessage}
        onChange={(e) => setNewMessage(e.target.value)}
      />
      <Button
        className={classes.submitButton}
        onClick={sendMessage}
        >
          Send
        </Button>
    </Box>
    </>
  );

  const noChat = (
    <>
    <span className={classes.noChat}>
      Open a conversation to start a chat.
    </span>
    </>
  );

  const openChat = (userId) => {
    conversationsRequests.checkConversation(user.id, userId)
    .then(data => {
      if (data.result) {
        const conversation = conversations.find( ({conversationId}) => conversationId === data.result.conversationId );
        setCurrentChat(conversation);
      } else {
        conversationsRequests.create(user.id, userId)
        .then(data => {

          setConversations([data.result, ...conversations]);
          setCurrentChat(data.result);
          socket.emit('createConversation', {
            conversationId: data.result.conversationId,
            receiverId: userId,
          });
        })
      }
    })
  }

  return (
    <MainView hideSearch={true}>
      <Grid container className={classes.root}>
        <Grid md={3} container item className={classes.leftSide}>
          <Select
            options={users}
            onChange={(e) => openChat(e.value)}
          />
          {conversations.map(c => (
            <Box key={c.conversationId} onClick={() => setCurrentChat(c)}>
              <Conversation
              conversationId={c.conversationId}
              currentUser={user}
              selected={c?.conversationId === currentChat?.conversationId}
              />
            </Box>
          ))}
        </Grid>
        <Grid md={9} container item className={classes.chatBox}>
          {currentChat ? chatOpen : noChat}
        </Grid>
      </Grid>
    </MainView>
  )
}

export default Chat
