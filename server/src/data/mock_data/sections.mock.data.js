export default [
	{
		title: 'Arrays',
		content:
			'<iframe width="560" height="315" src="https://www.youtube.com/embed/oigfaZ5ApsM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
		order: 1,
	},
	{
		title: 'Objects',
		content:
			'<iframe width="560" height="315" src="https://www.youtube.com/embed/X0ipw1k7ygU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
		order: 2,
	},
	{
		title: 'Functions',
		content:
			'<iframe width="560" height="315" src="https://www.youtube.com/embed/xUI5Tsl2JpY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
		order: 3,
	},
	{
		title: 'Array Iterations',
		content:
			'<iframe width="560" height="315" src="https://www.youtube.com/embed/Urwzk6ILvPQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
		order: 4,
	},
	{
		title: '10 interview questions every JavaScript developer should know',
		content:
			'<iframe width="560" height="315" src="https://www.youtube.com/embed/oxoFVqetl1E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
		order: 5,
	},
];