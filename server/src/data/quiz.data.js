/* eslint-disable no-unused-vars */
import quizService from '../services/quiz.service.js';
import pool from './db-pool.js';

const getByCourseId = async (courseId) => {
	const sql = `
		SELECT *
		FROM quizes 
		WHERE course_id = ?;
	`;

	const result = await pool.query(sql, [courseId]);
	return result[0];
};

const getQuestionsByQuizId = async (quizId) => {
	const sql = `
		SELECT id, content, points
		FROM quizes_questions
		WHERE quiz_id = ?;
	`;

	return await pool.query(sql, [quizId]);
};

const getAnswersByQuestionId = async (questionId) => {
	const sql = `
		SELECT id, answer, is_correct
		FROM questions_answers
		WHERE question_id = ?;
	`;

	return await pool.query(sql, [questionId]);
};

const create = async (courseId, quizId, totalPoints) => {
	const sql = `
      INSERT INTO quizes (id, course_id, total_points) 
      VALUES (? , ?, ?);
      `;

	await pool.query(sql, [quizId, courseId, totalPoints]);

	const returnCreatedQuizSql = `
      SELECT *
      FROM quizes
      WHERE id = ? ;
    `;

	return await pool.query(returnCreatedQuizSql, [quizId]);
};

const addQuestions = async (quizId, questions = []) => {
	const sql = `
	INSERT INTO quizes_questions (id, content, points, quiz_id)
	VALUES ${valuesForMultipleInsertionsQuestions(quizId, questions)}`;

	await pool.query(sql);
};

const addAnswers = async (questionId, answers = []) => {
	const sql = `
  INSERT INTO questions_answers (id, answer, is_correct, question_id)
  VALUES ${valuesForMultipleInsertionsAnswers(questionId, answers)}`;

	await pool.query(sql);
};

const userQuizResult = async ({ quizId, userId, points }) => {
	const sql = `
	INSERT INTO quizez_users (quizes_id, users_id, points)
	VALUES (?, ?, ?);
	`;

	await pool.query(sql, [quizId, userId, points]);

	const takeInsertedValuesSql = `
	SELECT * 
	FROM quizez_users
	WHERE quizes_id = ? AND users_id = ?;
	`;

	const result = await pool.query(takeInsertedValuesSql, [quizId, userId]);
	return result[0];
};

/* HELPER FUNCTIONS */

const valuesForMultipleInsertionsQuestions = (quizId, questionsArr) => {
	const sql = questionsArr.map(
		(x) =>
			`("${x.question.id}", "${x.question.content}", "${x.question.points}","${quizId}")`
	);

	return sql.join(',');
};

const valuesForMultipleInsertionsAnswers = (questionId, answersArr) => {
	const sql = answersArr.map(
		(x) =>
			`("${x.answer.id}", "${x.answer.answer}", "${
				x.answer.isCorrect ? 1 : 0
			}","${questionId}")`
	);

	return sql.join(',');
};

export default {
	getByCourseId,
	getQuestionsByQuizId,
	getAnswersByQuestionId,
	create,
	addQuestions,
	addAnswers,
	userQuizResult,
};
