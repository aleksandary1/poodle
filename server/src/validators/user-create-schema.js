/**
 * Validate User when register
 * @author Kalina Georgieva <georgieva.kalina27@gmail.com>
 */

const emailRegex = new RegExp(
	'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$'
);

// Validate password: to have a number, to have a capital letter, to be between 6 and 20 characters
const passwordRegex = new RegExp('^(?=.*)(?=.*[a-z])(?=.*[A-Z]).{6,20}$');

export default {
	email: (value) =>
		value !== undefined &&
		typeof value === 'string' &&
		value.length > 0 &&
		emailRegex.test(value),
	password: (value) =>
		value !== undefined &&
		typeof value === 'string' &&
		value.match(passwordRegex),
	firstName: (value) =>
		value !== undefined && typeof value === 'string' && value.length > 0,
	lastName: (value) =>
		value !== undefined && typeof value === 'string' && value.length > 0,
};
