// import { v4 as uuid } from 'uuid';
import restrictionsData from '../data/restrictions.data.js';
import { normalizeEntityProperties } from './entity-mapper.service.js';

export const getRestriction = async (sectionId, userId) => {
  const restriction = await restrictionsData.get(sectionId, userId);

  return normalizeEntityProperties(restriction);
};

export const getRestrictedUsers = async (sectionId) => {
  const restricted = await restrictionsData.getUsers(sectionId);

  return restricted.map(r => normalizeEntityProperties(r));
};

export const restrictUser = async (sectionId, userId) => {
  return await restrictionsData.restrict(sectionId, userId);
};

export const removeRestriction = async (sectionId, userId) => {
  return await restrictionsData.remove(sectionId, userId);
};

export const clearRestrictions = async (sectionId) => {
  return await restrictionsData.clear(sectionId);
};

export const changeRestriction = async (sectionId, isRestricted) => {
  return await restrictionsData.change(sectionId, isRestricted);
};

export const changeRestrictionBy = async (sectionId, restriction) => {
  return await restrictionsData.changeBy(sectionId, restriction);
};